\newpage
\section{Affine Algebraic Geometry}

Motivation: A finite group $G$ is a subgroup of some permutation group $S_n$.
Generalization: Replace $S_n$ by $\GL_n$, and finite groups by Zariski-compact subgroups of $\GL_n$.

Fact:  Let $K \subseteq \GL_n(\R)$ be a compact subgroup.
Then there exist $f_1,\dotsc,f_s$ in $\pol{\R}{X}{1,1}{n,n}$ such that $K = \setdef{\forall 1\leq i\leq s\colon A \in \GL_n(\R)}{f_i(A) = 0}$

Example: $K = O_n(\R) = \setdef{A \in \GL_n}{A^T A = I_n = AA^T}$

Warning:  The converse is not true, for example
$\SL_2(\R) = \setdef{A \in \GL_2(\R)}{ \det A -1 = 0}$.

From now on:  Fix $k = \bar k$ an algebraically closed field.
We also sometimes omit $k$ where it is clear, for example $\GL_n = \GL_n(k)$.

\begin{definition}[linear algebraic group]
  A \textbf{linear algebraic group} (over $k$) is a subgroup of $\GL_n(k)$ which is the common zero set of a set $M$ of polynomials in $\pol{k}{X}{1,1}{n,n}$.
\end{definition}
\begin{example}~ 
  \begin{enumerate}
  \item
    $\GL_n$ the zero set of the zero polynomial.
  \item
    $\SL_n$ as the zero set of $\det - 1$.
  \item
    Finite subgroups of $\GL_n (k)$ (Exercise).
  \item
    Diagonal matrices in $\GL_n$ as the zero set of the polynomials $X_{i,j}$ for $i \neq j$.
  \item
    Upper triangular matrices.
    More generally:  Standard parabolic subgroups (image, or google it lelz), (a zero set of some $X_{i,j}$, $i\neq j$, $i > j$ ? similar to the diagonal matrices ?)
  \item
    The orthogonal group $\operatorname{O}_n$.
  \item
    $S_{kn}(k) = \setdef{A \in \GL_{2n}(k)}{A^T J A = J}$, where $J =
    \begin{pmatrix}
      0 & 1_n\\ 1_n & 0
    \end{pmatrix}$.
  \item
    The intersection of linear algebraic groups in $\GL_n (k)$ is again a linear algebraic group.
  \end{enumerate}
\end{example}
We want that the linear algebraic group $\GL_1 = k^\times$ be isomorphic to the subgroup $H = \setdef{\begin{pmatrix} a & 0 \\ 0 &1\end{pmatrix}}{a \in k^\times} \subseteq \GL_2$.
\begin{definition}[affine algebraic group]
  An \textbf{affine algebraic group} (over $k$) is an affine algebraic variety (over $k$) $(G, k[G])$ with a group structure on $G$ such that the multiplication and inversion maps of the group $G$ are morphisms of algrebraic sets.
\end{definition}
\begin{definition}
  An \textbf{affine algebraic variety} is a pair $(X,k[X])$, where
  \begin{itemize}
  \item $X$ is a set
  \item $k[X]$ is a finitely generated subalgebra of $\operatorname{Maps}(X,k)$ such that
    \begin{align*}
      \Phi \colon X &\longrightarrow \Hom_{\mathrm{alg}}(k[X],k)  \\
      x & \longmapsto \operatorname{ev}_x
    \end{align*}
    is bijective.
  \end{itemize}
\end{definition}
\begin{example}~ 
  \begin{enumerate}
  \item $X = k^n$, $k[X] = \pol{k}{X}{1}{n}$, identified with the subalgebra $\mathcal P_k(k^n) \subseteq \operatorname{Maps}(k^n,k)$.
    We claim that $(X,k[X])$ is an affine algebraic variety, the \textbf{affine space} of dimension $n$.
    It is obvious that $k[X]$ us a finitely generated algebra.
    We also know via Hilbert's Nullstellensatz that $\Phi \colon X \to \Hom_{\mathrm{alg}}(\pol{k}{X}{1}{n},k)$, $y \mapsto \operatorname{ev}_y$ is a bijection via Hilbert's Nullstellensatz.
  \item
    $X = \set{\mathrm{pt}}$, $k[X] = \operatorname{Maps}(X,k) = \operatorname{Maps}(\set{\mathrm{pt}},k) = k$.
    $k$ is a finitely generated subalgebra of $k = \operatorname{Maps}(\set{\mathrm{pt}},k)$.
    $\operatorname{Hom}_{\mathrm{alg}} (k[X],k) = \operatorname{Hom}_{\mathrm{alg}} (k,k) = \set{id}$.
    As $\Phi$ is bijective, we see that this is an affine variety.
  \item
    $X$ finite set, $k[X] = \operatorname{Maps}(X,k)$ (Exercise)
  \item
    Let $(X,k[X])$ an affine algebraic variety.
    Consider a subset $M \subseteq k[X]$ and define $$V(M) = \setdef{x \in X}{\forall f \in M\colon f(x) = 0} \subseteq X \, .$$
    Consider also $$k[V(M)] := \res{k[X]}{V(M)} \cong k[X]/(M) \, .$$
    We claim that $(V(M),k[V(M)])$ is an affine variety.

    $k[X] \to k[V(M)] = \res{k[X]}{V(M)} \cong k[X]/(M)$ is asurjective algebra homomorphism (via restriction, or modding out).
    By assumption $k[X]$ is finitely generated, therefore also $k[V(M)]$.
    To show is that $\tilde \Phi \colon V(M) \to \Hom_{\mathrm{alg}}(k[V(M)], k)$, $x \mapsto \operatorname{ev}_x$ is bijective.
    If $f \colon k[V(M)] \to k$ is an algebra homomorphism, then      $$\begin{tikzcd}
        k[X] \arrow{r}{\operatorname{res}} \arrow[dashed]{d}{\tilde{f}} & k[V(M)] \arrow{dl}{f} \\ k
    \end{tikzcd}$$
    defines an algebra homomorphism $\tilde f = f \circ \operatorname{res}$.
    In particular, $\tilde f = \operatorname{ev}_x$ for some $x \in X$, because $(X,k[X])$ is an affine algebraic variety.

    \underline{$\tilde \Phi$ is injective:}
    Let $x,y \in V(M)$ such that $\operatorname{ev}_x = \operatorname{ev}_y \colon k[V(M)] \to k$.
    Then $\tilde{\operatorname{ev}_x} = \operatorname{ev}_y$.
    But $\tilde{\operatorname{ev}_x}$ must be $\operatorname{ev}_x \colon k[X] \to k$, and $\tilde{\operatorname{ev}_y}$ must be $\operatorname{ev}_y \colon k[X] \to k$.
    Therefore $\operatorname{ev}_x = \operatorname{ev}_y \colon k[X] \to k$.
    But then $x = y$ (as elements in $X$), as $\Phi$ is bijective, as $(X,k[X])$ is an affine variety.

    \underline{$\tilde \Phi$ is surjective:}
    Let $h \in \operatorname{Hom}_{\mathrm{alg}}(k[V(M)],k)$.
    Define $\tilde h := h \circ \operatorname{res}$.
    We have $\tilde h = \operatorname{ev}_x$ for some (unique) $x \in X$.
    
    Case 1: $x \in V(M)$. For $f \in k[V(M)]$, pick $f^\prime \in k[X]$ such that $\res{f^\prime}{V(M)} = f$.
    Then $\operatorname{ev}_x(f) = f(x)$ and $h(f) = h(\operatorname{res}(f^\prime)) = \tilde h(f) = \operatorname{ev}_x(f) = f(x)$ for all $f \in k[V(M)]$.
    Therefore $\operatorname{ev}_x = h$.

    Case 2: $x \notin V(M)$.
    Then there exists a $g \in M$ such that $g(x) \neq 0$ but $\res{g}{V(M)} = 0$.
    Now
    \begin{equation*}
      \begin{tikzcd}
        k[X] \arrow{r}{\operatorname{res}} \arrow{dr} {\operatorname{ev}_x} & k[V(M)] \arrow{d}{h} \\ & k
      \end{tikzcd}
      \begin{tikzcd}
        g \arrow[mapsto]{r} \arrow[d,mapsto] & \operatorname{res}(g) = 0 \arrow[mapsto]{d} \\
        \operatorname{ev}_x(g) = g(x) \neq 0 & h(0) = 0
      \end{tikzcd}
    \end{equation*}
    This is a contradiction.
    Therefore any $h \in \Hom_{\mathrm{alg}}(k,k[V(M)],k)$ has a preimage $x \in V(M)$.
  \item
    Special case of the above: $X = k^n$, $k[X] = \mathcal P(k^n)$ is an affine algebraic variety by the first example.
    Then for $M \subseteq k[X]$, $(V(M),k[V(M)])$ is an affine algebraic variety.
  \item
    Let $(X,k[X])$ be an affine algebraic variety, and $f \in k[X]$.
    If $X_f := \setdef{x \in X}{f(x) \neq 0}$ and $k[X_f] := \res{k[X]}{X_f}[1/f]$, then $(X_f,k[X_f])$ is an affine algebraic variety (Exercise).
  \end{enumerate}
  A consequence of these examples is that any linear algebraic group is an affine algebraic group.
\end{example}
\begin{proposition}
  Given a linear algebraic group $G$, we can set $X := G$ and find some $k[X]$ such that $(X,k[X])$ is an affine algebraic variety.
\end{proposition}
\begin{proof}
  Let $G \subseteq \GL_n(k)$ be a linear algebraic group.
  Consider $Y = k^{n^2} = \operatorname{M}_{n\times n}(k)$.
  Now by the first example, $(Y,k[Y] = \pol{k}{X}{1,1}{n,n})$ is an affine algebraic variety.
  By the last example, we know that $\GL_n \subseteq Y$ ``is'' an affine algebraic variety via $X = \operatorname{M}_{n\times n} (k)$, $f = \det$ and $X_f = \GL_n$.
  So $(\GL_n = X_{\det}, k[X_{\det}])$ is an affine algebraic variety.
  Now we use the fourth example to get that $(V(M) = G, k[V(M)] = k[G])$ is an affine algebraic variety, where $M\subseteq \pol{k}{X}{1,1}{n,n} \subseteq k[\det]$, such that $G = \operatorname{A \in \GL_n}{\forall f \in M\colon f(A) = 0}$.
\end{proof}

\underline{Morphisms between algebraic varieties}

\begin{definition}
  Let $(X,k[X])$ and $(Y,k[Y])$ be affine algebraic varieties.
  A \textbf{morphism of affine algebraic varieties} from $(X,k[X])$ to $(Y,k[Y])$ is a map $f \colon X \to Y$ such that $f^\ast \colon k[Y] \to k[X]$.
\end{definition}
\begin{remark}
  If $\operatorname{im}f^\ast \subseteq k[X]$, we also write $f^\natural$ instead of $f^\ast$.
  Hence a morphism is a pair $(f,f^\natural)$ such that $G = \setdef{A \in \GL_n}{\forall f \in M\colon f(A) = 0}$
\end{remark}
\begin{definition}[isomorphism]
  A morphism $f \colon X \to Y$ is an \textbf{isomorphism} if there exists a morphism $g \colon Y \to X$ such that $g \circ f = \id_X$ and $f \circ g=\id_Y$
\end{definition}
Warning:
Consider $\F_p$ and $k = \bar{\F_p}$ with the Frobenius morphism $\operatorname{Fr}$.
$\operatorname{Fr}$ is bijective but not an automorphism for $(k,k[k])$.
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

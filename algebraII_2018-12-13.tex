\underline{Recall:}
An affine algebraic group is an affine algebraic variety $(G,k[G])$, where $G$ is a group, and the maps of multiplication $\mu$ and inversion $\inv$ are morphisms.
We call $\Delta \colon k[G] \xrightarrow{\mu^\ast}k[G\times G]\xrightarrow[\sim]{\beta^{-1}} k[G]\otimes k[G]$ the \textbf{comultiplication} and $i\colon k[G] \xrightarrow{\inv^\ast} k[G]$ the \textbf{antipode}.
Note that $i(f)(g) = f(g^{-1})$.

\begin{lemma}\label{lemviii.2}
  Let $(G,k[G])$ be an affine algebraic group.
  Then
  \begin{align*}
    &\Delta (f) = \sum_i f_i \otimes g_i \\
    \iff& \forall x,y \in G \colon f(x\cdot y) = \sum_i f_i(x)g_i(y) \; .
  \end{align*}
\end{lemma}
\begin{proof}
    \begin{align*}
    &\Delta (f) = \sum_i f_i \otimes g_i \\
      \iff&\forall x,y \in G \colon f(x\cdot y) = \mu^\ast (f)((x,y)) = (\beta \circ \Delta (f)) ((x,y)) %= \sum_i f_i(x)g_i(y) f(x\cdot y)  \\ &
      = \sum_i f_i(x)g_i(y) \; .
  \end{align*}
\end{proof}
\begin{example}~ 
  \begin{enumerate}
  \item
    $G = \GL_n(k)$, $k[G] = \pol{k}{X}{1,1}{n,n} [\det^{-1}]$.
    Then we know $(G,k[G])$ is an affine algebraic variety.
    $G$ is a group.
    
    \underline{Claim:
      $\Delta (X_{i,j}) = \sum_{k=1}^n X_{i,k} \otimes X_{k,j}$, $\Delta(\det^{-1}) = \det^{-1}\otimes\det^{-1}$.}
  
    Indeed, $X_{i,j}(A\otimes B) = \sum_{k=1}^n a_{i,k}b_{k,j} = \sum_{k=1}^n X_{i,k}(A)X_{k,j}(B)$.
    Therefore via \Cref{lemviii.2} we have $\Delta(X_{i,j}) = \sum_{k=1}^n X_{i,k}X_{k,j}$ for all $1\leq i,j \leq n$.
    We also have $$\det^{-1}(A\cdot B) = \det^{-1}(A)\det^{-1}(B) \,,$$ and therefore via \Cref{lemviii.2} we have $\Delta(\det^{-1}) = \det^{-1} \otimes \det^{-1}$.
    If $\Delta$ is an algebra homomorphism, then it is determined by these formulas.

    So to show is that $\Delta$ is an algebra homomorphism.
    $$(X_{i,j} + X_{k,l})(A\cdot B) = \sum_{r=1}^n a_{i,r}b_{r,j} + \sum_{s=1}^n a_{k,s}b_{s,l} = \sum_{r=1}^n X_{i,r}(A)X_{r,j}(B) + \sum_{s=1}^n X_{k,s}(A)X_{s,l}(B)$$
    Therefore $\Delta(X_{i,j} + X_{k,l}) = \sum_{r=1}^n X_{i,k}\otimes X_{r,j} + \sum_{s=1}^n X_{k,s} + X_{s,l} = \Delta (X_{i,j}) + \Delta(X_{k,l})$.
    It is easy to see that $\Delta(\lambda X_{i,j}) = \lambda \Delta (X_{i,j})$ for all $\lambda \in k$.
    Now \begin{align*}(X_{i,j}X_{k,l})(A\cdot B) & = (\sum_{r=1}^n a_{i,r}b_{r,j})(\sum_{s=1}^n a_{k,s}b_{s,l}) = (\sum_{r=1}^n X_{i,r}(A)X_{r,j}(B))(\sum_{s=1}^n X_{k,s}(A)X_{s,l}(B))  \\& = \sum_{r=1}^n \sum_{s=1}^n X_{i,r}(A)X_{k,s}(A) X_{r,j}(B)X_{s,l}(B) \, . \end{align*}
    Therefore$$\Delta(X_{i,j}X_{k,l}) = \sum_{r,s=1}^n X_{i,r}X_{k,s} \otimes X_{r,j}X_{s,l} = (\sum_{r=1}^n X_{i,r}\otimes X_{r,j}) (\sum_{s=1}^n X_{k,s} \otimes X_{s,l}) = \Delta(X_{i,j})\Delta(X_{k,l}) \, .$$
    Therefore $\Delta$ is a morphism, and $\mu$ is a morphism.
    
    Special case: $n=1$.
    $\GL_1(k) = k^\times$, $k[\GL_1(k)] = k[T,T^{-1}]$.
    $\Delta(T) = T\otimes T$, $\Delta (T^{-1}) =T^{-1}\otimes T^{-1}$.
    We call this group also $\mathbb{G}_m$, the \textbf{multiplicative group}.
  \item
    $G = (k,+)$, $k[G] = k[T]$, $\Delta(T) = T \otimes 1 + 1 \otimes T$.
  \end{enumerate}
\end{example}
\begin{lemma}\label{lemviii.3}
  Let $(X,k[X])$ be an affine algebraic variety, $Y \subseteq Z$ a closed subset, and $\varphi \colon X \to Z$ a morphism with $\im \varphi \subseteq Y$.
  Then $\varphi \colon X \to Y$ is a morphism.
\end{lemma}
\begin{proof}
  \begin{equation}
    \begin{tikzcd}
      X \arrow[r, "\varphi"] \arrow[dr,"\varphi"] & Z \\
      & Y \arrow[u,hookrightarrow,"\operatorname{incl}"]
    \end{tikzcd}
    \begin{tikzcd}
      k[X] & k[Z] \arrow[l,"\varphi^\ast"] \arrow[d,"\operatorname{incl}^\ast"] \\
      & k[Y] \arrow[ul,"\varphi^\ast"]
    \end{tikzcd}
  \end{equation}
  But we have $k[Y] = \set{\res{f}{Y} \text{ for } f \in k[Z]}$, therefore the lower $\varphi^\ast$ has image in $k[X]$, since upper $\varphi$ has by assumption.
\end{proof}
\begin{proposition}\label{propviii.4}
  Let $(G,k[G])$ be an affine algebraic group, and let $H\leq G$ be a closed subgroup.
  Then $(X,k[H] := \setdef{\res{f}{H}}{f \in k[G]})$ is an affine algebraic group.
\end{proposition}
\begin{theorem}\label{thmviii.5}
  Linear algebraic groups are affine algebraic groups
\end{theorem}
\begin{proof}
  $(\GL_n(k),k[\GL_n(k)])$ is an affine algebraic group for all $n$.
  If $H$ is an affine linear algebraic group, then by definition $H \leq \GL_n$ is a closed subgroup for some $n$.
  By \cref{lemviii.5}, $(H,k[H])$ is an affine algebraic group.
\end{proof}
An affine algebraic group $G$ is an affine algebraic variety together with morphisms $\mu $ of the multiplication, $\inv $ of the inversion and $e \colon \set{\mathrm{pt}} \to G$, $\mathrm{pt} \mapsto e$, such that
\begin{equation*}
  \begin{tikzcd}
    G\times G \times G \arrow[r,"\mu \times \id"] \arrow[d, "\id \times \mu"]& G\times G \arrow[d, "\mu"] \\
    G\times G \arrow[r,"\mu"] & G
  \end{tikzcd}
  \qquad
  \begin{tikzcd}
    \set{\mathrm{pt}} \times G \arrow[d,"e \times \id"]\arrow[r,equal] &G \arrow[d,equal] \arrow[r,equal] &G \times \set{\mathrm{pt}} \arrow[d,"\id \times e"]\\
    G \times G \arrow[r,"\mu"] &G & G\times G \arrow[l,"\mu"']
  \end{tikzcd}
\end{equation*}
\begin{equation*}
  \begin{tikzcd}
    &G\times G \arrow[dr,"\mu"] & \\
    G \arrow[ur,"\id \times \inv"] \arrow[r, "\set{\mathrm{pt}}"] \arrow[dr, "\inv \times \id"'] & \set{\mathrm{pt}} \arrow[r,"e"] & G \\
    & G\times G \arrow[ur,"\mu"'] &
  \end{tikzcd}
\end{equation*}
\begin{proposition}\label{propviii.6}
  An affine algebraic group is an affine algebraic variety $(G,k[G]) = (G,A)$ together with algebra homomorphisms $\Delta \colon A \to A \otimes A$, the \textbf{comultiplication}, $\epsilon \colon A \to k$, the \textbf{counit} and $i \colon A \to A$, the \textbf{antipode}, satisfying
  \begin{itemize}
  \item[(G1)]\begin{equation*}
    \begin{tikzcd}
      A \otimes A \otimes A & A \otimes A \arrow[l,"\Delta \otimes \id"]  \\
      A \otimes A \arrow[u,"\id \otimes \Delta"] & A \arrow[l,"\Delta"]\arrow[u,"\Delta"]
    \end{tikzcd}
  \end{equation*}
\item[(G2)]
  \begin{equation*}
    \begin{tikzcd}
      k \otimes A  \arrow[r,equal] & A \arrow[d,equal] \arrow[r,equal] & A \otimes k \\
      A \otimes A \arrow[u,"\epsilon \otimes \id"] & A \arrow[l,"\Delta"'] \arrow[r,"\Delta"] & A \otimes A \arrow[u,"\id \otimes \epsilon"]
    \end{tikzcd}
  \end{equation*}
\item[(G3)]
  \begin{equation*}
    \begin{tikzcd}
     &A\otimes A \arrow[dl,"m"] & A \otimes A \arrow[l,"\id \otimes i"] & \\
      A &  & k \arrow[ll,"\mathrm{unit map}"] & A \arrow[ul,"\Delta"] \arrow[l,"\epsilon"] \arrow[dl,"\Delta"]  \\
     &A \otimes A \arrow[ul,"m"] & A \otimes A \arrow[l,"i \otimes \id"] &
    \end{tikzcd}
  \end{equation*}
\end{itemize}
commute.
\end{proposition}
\begin{definition}
  A \textbf{Hopf algebra} (over $k$) is a $k$-vectorspace $A$ together with $k$-linear maps
  \begin{align*}
    m \colon A \otimes A & \to A &\text{multiplication}  \\
    \eta \colon k & \to A & \text{unit} \\
    \Delta \colon A & \to A \otimes A & \text{comultiplication} \\
    \epsilon \colon A & \to k & \text{counit} \\
    i \colon A & \to A & \text{antipode}
  \end{align*}
  satisfying
  \begin{itemize}
  \item[(H1)] $(A,m,\eta)$ is an associative algebra with $1 = \eta(1)$.
  \item[(H2)] $(A,\Delta,\eta)$ is a coassociative coalgebra with counit $\epsilon$, that means coassociativity (G1) holds and counit (G2) holds.
  \item[(H3)] compatibility between $m$ and $\epsilon$
    \begin{equation*}
      \begin{tikzcd}
        A \otimes A \arrow[r,"m"] \arrow[dr,"\epsilon \otimes \epsilon"]  & A \arrow[d,"\epsilon"] \\
        & k \otimes k = k
      \end{tikzcd}
    \end{equation*}
  \item[(H4)] compatibility of $\Delta$ and $\eta$.
    \begin{equation*}
      \begin{tikzcd}
        A \otimes A & A \arrow[l,"\Delta"]  \\
         & k \otimes k = k \arrow[ul,"\eta \otimes \eta"] \arrow[u,"\eta"]
      \end{tikzcd}
    \end{equation*}
  \item[(H5)] compatibility of $m$ and $\Delta$
    \begin{equation*}
      \begin{tikzcd}
        A \otimes A \arrow[d,"\Delta \otimes \Delta"] \arrow[r,"m"] & A \arrow[r, "\Delta"] & A \otimes A  \\
        (A \otimes A) \otimes (A \otimes A) \arrow[rr,"\id \otimes \tau \otimes \id"] && (A \otimes A) \otimes (A\otimes A) \arrow[u,"m \otimes m"]
      \end{tikzcd}
    \end{equation*}
  \item[(H6)] compatibility of $\eta$ and $\epsilon$
    \begin{equation*}
      \begin{tikzcd}
        k \arrow[r,"\eta"] \arrow[rr, bend left, "\id"] & A \arrow[r,"\epsilon"] &k
      \end{tikzcd}
    \end{equation*}
  \item[(H7)] antipode condition (G3).
  \end{itemize}
\end{definition}
\begin{example}~ 
  \begin{enumerate}
  \item $(k,m,\eta,\Delta,\epsilon,\id)$ with $m$ the multiplication in $k$ and $\epsilon$ and $\eta$ the identity is a Hopf algebra
  \item Let $G$ be a group.
    Then with $kG$ as the group algebra of $G$ and $m$ the multiplication in $kG$ and $\eta = \lambda \mapsto \lambda e$, $(kG,m,\eta,\Delta,\epsilon,i)$ is a Hopf algebra, if $\Delta = g \mapsto g \otimes g$ and $\epsilon (g) = 0$ if $g \neq e$ and $\epsilon(g) = 0$ otherwise, and $i = g \mapsto g^{-1}$ (each extended linearly).
  \item If $G$ is an affine algebraic variety, then $(K[G],m,\eta,\Delta,\epsilon,\iota)$ is a Hopf algebra with $m$ the algebra multiplication of $k[G]$, $\eta$ the counit as the inclusion $k \hookrightarrow k[G]$, $\Delta = m^\ast \colon k[G] \to k[G]\otimes k[G]$ the comultiplication, $\epsilon = \ev_{e} \colon k[G] \to k$ the counit as the evaluation at $e \in G$ and $\iota = \operatorname{inv}^\ast$ the antipode as the pullback of the inversion on $G$.
    The first example the special case of this with $G = \set{e}$.
  \end{enumerate}
\end{example}
\begin{remark}
  $(A,m,\eta)$ is a finite dimensional $k$-algebra with $1$ if and only if $(A^\ast,\Delta = m^\ast,\epsilon = \eta^\ast)$ is a finite dimensional coalgebra with counit $\epsilon$.
\end{remark}
\begin{remark}~ 
\begin{itemize}
  \item $(A,m,\eta,\Delta,\epsilon)$ with (H1)-(H6) is called a \textbf{bialgebra}.

  \item A Hopf algebra $(A,m,\eta,\Delta,\epsilon, i)$ is called \textbf{commutative} (respectively finitely generated) if $(A,m,\eta)$ is commutative (respectively finitely generated).

  \item Note that if $A,B$ are $k$-algebras, then $A\otimes B$ is a $k$-algebra with multiplication
  $m_{A\otimes B} \colon (A\otimes B) \otimes (A\otimes B) \to A\otimes B$, $(a_1 \otimes b_1) \otimes (a_2 \otimes b_2) \mapsto a_1a_2 \otimes b_1b_2$, $m_{A \otimes B} = m_A \otimes m_B \circ \id \otimes \tau \otimes \id$.
  
  \item Let $(A, \Delta_A,\epsilon,A)$ and $(B,\Delta_B,\epsilon_B)$ be coalgebras.
  Then $A\otimes B$ is a coalgebra via $\Delta_{A\otimes B} = \id \otimes \tau \otimes \id \circ \Delta_A \otimes \Delta_B$, $\epsilon_{A \otimes B} = \epsilon_A \otimes \epsilon_B$.
\end{itemize}
\end{remark}
\begin{lemma}\label{lemviii.7}~ 
  \begin{itemize}
  \item (H3) and (H8) $\iff$ $\epsilon$ is an algebra homomorphism sending unit to unit
  \item (H4) and (H5) $\iff$ $\Delta$ is an algebra homomorphism
  \item (H4) and (H6) $\iff$ $\eta$ is a coalgebra homomorphism
  \item (H3) and (H5) $\iff$ $m$ is a coalgebra homomorphism
\end{itemize}
\end{lemma}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

\begin{definition}
  Let $W$ be a $k$-vector space, $S \subseteq \End_k(W)$ a subset.
  Then
  \begin{equation*}
    S^\prime := \setdef{ \varphi \in \End_k (W)}{\forall s \in S\colon \varphi \circ s = s \circ \varphi}
  \end{equation*}
  is the \textbf{commutator of $S$} (in $\End_k(W)$).
  We can also write $S^\prime = \End_S (W)$.
  We abbreviate $(S^\prime)^\prime = S^{\prime \prime}$.
\end{definition}
\begin{note}
  \underline{Facts:}
  \begin{enumerate}
  \item
    $S^\prime \subseteq \End_k(W)$ is a subalgebra
  \item
    If $T\subseteq S \subseteq \End_k(W)$, then $S^\prime \subseteq T^\prime$.
  \item
    If $S \subseteq T^\prime$, then $T \subseteq S^\prime$.

    Explanation:
    \begin{equation*}
      S\subseteq T^\prime \iff \forall s \in S \colon \forall t \in T \colon st = ts \iff \forall t \in T \colon \forall s \in S \colon st = ts \iff T \subseteq S^\prime \; .
    \end{equation*}
  \item
    $ S \subseteq S^{\prime\prime}$
  \item
    $ S^\prime = S^{\prime\prime\prime}$

    Explanation:
    $S^\prime \subseteq (S^\prime)^{\prime\prime} = S^{\prime\prime\prime}$.
    By (4) we have $S \subseteq S^{\prime\prime}$, hence $(S^{\prime\prime})^\prime \subseteq S^\prime$ by (3).
  \item
    $S = T^\prime \iff T = S^\prime$ (as $S = T^\prime \implies S^{\prime\prime} = (T^\prime)^{\prime\prime} = T^\prime \implies T \subseteq S^\prime$, converse analogous)
  \end{enumerate}
\end{note}
\begin{remark}
  Let $V$ is an $A$-module ($A$ a $k$-algebra) and also a $B$-module ($B$ a $k$-algebra).
  If the action of $A$ commutes with the one of $B$, then $V$ is an $A \otimes B$ module given by $(a \otimes b ). v = abv = bav$.
\end{remark}
\begin{lemma}\label{lemvi.1}
  Let $A$ be a $k$-algebra and $M$ an $A$-module.
  Let $B$ be a $k$-algebra and $N$ a $B$-module.
  Then $M \otimes N$ is an $A \otimes B$-module via $(a \otimes b).(m \otimes n) = am \otimes bn$.
\end{lemma}
\begin{proof}
  Explicit calculation.
  Better: $M$ is an $A$-module means the choice of an algebra homomorphism $\varphi \colon A \to \End_k (M)$, $a \mapsto (m \mapsto am)$.
  Similarly, $N$ is a $B$-module means a choice of an algebra homomorphism $\psi \colon B \to \End_k(N)$.
  Then
  \begin{equation*}
    \begin{tikzcd}
      \varphi \otimes \psi \colon A \otimes B \arrow[r] \arrow[dr, dotted]& \End_k (M) \otimes \End_k (N) \arrow[d] \\ %dotted arrow
      & \End_k(M \otimes N)
    \end{tikzcd}
  \end{equation*}
\end{proof}
\begin{example}
  Let $G,H$ be groups and $M$ a representation of $G$, $N$ a representation of $H$ over a field $k$.
  Note: $k[G] \otimes k[H] \cong k[G \times H]$.
  We have that $M \otimes N$ is a $k[G]\otimes k[H]$-module and hence a representation of $G \times H$.
\end{example}
\begin{theorem}[Double Centralizer]
  Let $k$ be a field and $W$ a finite dimensional $k$-vectorspace.
  Let $A \subseteq \End_k(W)$ be a semisimple subalgebra.
  Then for
  \begin{equation*}
    A^\prime = \setdef{b \in \End_k(W)}{\forall a \in A \colon b \circ a = a \circ b} = \End_A(W)
  \end{equation*}
  the following holds true
  \begin{enumerate}
  \item
    $A^\prime$ is a semisimple algebra (subalgebra of $\End_k(W)$)
  \item
    $A^{\prime\prime} = A$, that is $\End_A (W) = A^\prime$ and $\End_{A^\prime}(W) = A$.
  \end{enumerate}
  If $k = \bar{k}$:
  \begin{enumerate}
    \setcounter{enumi}{2}
  \item
    There is a decomposition of $(A \otimes A^\prime)$-modules $\Phi^{-1} \colon W \xrightarrow{\cong} \bigoplus_{i=1}^r L_i \otimes L_i^\prime$, such that $L_1,\dotsc,L_r$ are irreducible $A$-modules and $L_1^\prime,\dotsc,L_r^\prime$ are irreducible $A^\prime$-modules pairwise nonisomorphic.
  \item
    The $L_1\dotsc,L_r$ are precisely the irreducible $A$-modules and the $L_1^\prime, \dotsc,L_r^\prime$ are precisely the irreducible $A^\prime$-modules up to isomorphism.
    Hence, in particular $$\card{\Irr(A)} = \card{\Irr(A^\prime)} \,.$$
  \end{enumerate}
\end{theorem}
\begin{remark}
  More generally, if $k$ is an arbitrary field, repleace (3) by $$\Phi^{-1} \colon W \xrightarrow{\cong} \bigoplus_{i=1}^r L_i \otimes_{D_i^{\mathrm{op}}} L_i^\prime \, .$$ where $D_i = \End_A (L_i)$.
  Moreover, this isomorphism is the isotypic decomposition for $W$ as $A$-modules, but also as $A^\prime$-modules.
\end{remark}
\begin{remark}
  Given a $k$-algebra $A$, an $A$-module $M$ and a $k$-vectorspace $N$, we then have an $A$-module structure onm $M \otimes N$ ($A$-modules are $A \otimes k$-modules).
  Note that $M \otimes N \cong M\oplus \dotsb \oplus M$ ($\dim N$ times) as an $A$-module if $N$ is finite dimensional.
\end{remark}
\begin{proof}[Proof of the double centralizer theorem]\renewcommand{\qedsymbol}{}
  If $A$ is a semisimple algebra, then every $A$-module is semisimple.
  Therefore $W \cong \bigoplus_{i=1}^s L_i^{\oplus n_i}$, where the $L_i$ are pairwise non-isomorphic.

  \begin{itemize}
  \item \underline{$s =\card{ \Irr(A)}$:}

    Assume $A \cong \prod_{i=1}^m R_i$ for some $m \in \N$ and $R_i \cong M_{m_i \times m_i}(C_i)$ where $C_i$ is a division algebra (exists by Artin-Wedderburn), and $m$, $m_i$ and $C_i$ are unique up to reordering and isomorphism of algebras.
    Then $\card{\Irr (A)} = m$ and so $s \leq m$ (by definition of s).
    Since $A \subseteq \End_k (W)$ is a subalgebra, we have that $1_i W \neq 0$ for any $1 \leq i \leq s$.
    Therefore $1_iW$ contains an irreducible $R_i$-module.
    Then for any irreducible $R_i$-module $U_i$, $1 \leq i \leq s$ there is an $R$-submodule in $W$ which is isomorphic to $U_i$ as an $R_i$-module.
    Therefore $s = m$, and $\card{\Irr(A)} = s$.
    Wlog we number them such that $\End_A(L_i)^{\mathrm{op}} \cong C_i$.

  \item \underline{$A^\prime$ is a semisimple algebra and $\card{\Irr(A^\prime)} = \card{\Irr(A)}$:}

    We have an isomorphism of algebras (Artin-Wedderburn)
    \begin{align*}
      A^\prime & = \setdef{b \in \End_k(W)}{\forall a \in A \colon ba =ab} = \End_A(W)  \\
       & \stackrel{\text{Schur}}{\cong} \prod_{i=1}^s M_{n_i \times n_i} (\End_A(L_i))  \\
       &=  \prod_{i=1}^s M_{n_i \times n_i} (D_i) \; ,
    \end{align*}
    where $D_i = \End_A (L_i)$.
    Therefore $A^\prime$ is semisimple and $\card{\Irr (A^\prime)} = s = \card{\Irr(A)}$.
    Note that we have $C_i^{\mathrm{op}} = D_i$.
    Via uniqueness of the $D_i$ we also have $D_i = \End_{A^\prime}(L_i^\prime)^{\mathrm{op}}$, for $L_i^\prime$ the irreducible $A$-modules.
    
    \item \uline{If $ U \subseteq W$ is an irreducible $A$-module, then $\Hom_k(U,W)$ is an irreducible $A^\prime$-module with the action given by $(\beta . f)(u) = \beta(f(u))$:}

    The action is well-defined:
    $(\beta f)(au) = \beta(f(au)) = \beta(af(u)) = a(\beta(f(u)))$.
    It is irreducible:
    It is enough to show that for $f_1,f_2 \in \Hom_A(U,W)$ there exists $\beta \in A^\prime$, such that $\beta f_1 = f_2$.
    Fix $ u \in U$.
    Set $v_1 = f_1 (u)$, $v_2 = f_2 (u)$.
    Since $W$ is a semisimple $A$-module, $W = Av_1 \oplus C$ for some $A$-module $C$.
    Now define $\beta \colon W \to W$ $k$-linear, $av_1 \mapsto av_2$ for $a \in A$, $c \mapsto c$ for $c \in C$.
    Obviously this is $A$-linear, hence $\beta \in A^\prime$.
    Now $(\beta f_1)(u) = \beta (f_1 (u)) = v_2 = f_2(u)$.
    Therefore $\beta f_1 (au) - f_2 (au)=0$ for all $a \in A$, because $\beta,f_1$ are $A$-linear.
    As $U$ is an irreducile $A$-module, we get $\beta f_1 = f_2$.
    This shows our claim that $U$ is irreducible.
  \end{itemize}
  Now, $L_i^\prime := \Hom_A (L_i,W)$ is an irreducible $A^\prime$-module, and these are all $A^\prime$-modules, by what we have just shown (and we number them as we did earlier).
  Since $L_i$ is a left $D_i = \End_A (L_i)$-module, $L_i^\prime$ is a right $D_i$-module, hence a left $D_i^{\mathrm{op}}$-module.
  Since $L_i$ is a left $\End_A(L_i)$-module, it is a right $\End_A (L_i)^{\mathrm{op}} = D_i^{\mathrm{op}}$-module.
  Therefore $L_i \otimes_{D_i^{\mathrm{op}}} L_i^\prime$ makes sense.

  $L_i \otimes_{D_i^{\mathrm{op}}} L_i^\prime$ is an $A \otimes A^\prime$-module via $(a\otimes b)(m \otimes n) = am \otimes bn$.
  In the case $k = \bar k$, $D_i = k = D_i^{\mathrm{op}}$ is clear by \cref{lemvi.2}.
  In the general case, to check is that the action is well-defined.
  Let $\varphi \in D_i^{\mathrm{op}}$.
  Let $a \in A$, $b \in A^\prime$.
  Then $$(a \otimes b)(x\varphi \otimes f) = (a\otimes b)(\varphi (x) \otimes f) = a \varphi (x) \otimes b f \,.$$
  On the other hand,
  \begin{align*}
    (a\otimes b)(x\otimes \varphi f) &= (a \otimes b)(x \otimes f \circ \varphi)  \\
                                     &= (ax \otimes bf \circ \varphi)  \\
                                     &= ax \otimes (bf)(\varphi) \\
                                     &= ax \otimes \varphi (bf)  \\
                                     &= ax \varphi \otimes bf  \\
                                     &= \varphi (ax) \otimes bf \\
                                     &= a\varphi \otimes bf
  \end{align*}

  Consider $\Phi_i \colon L_i \otimes_{D_i^{\mathrm{op}}} L_i^\prime \to W$, $x \otimes f \mapsto f(x)$.
  This is an $A$-module homomorphism (since $\Phi_i (a . x\otimes f) = \Phi_i ((ax)\otimes f) = f(ax) = af(x) = a\Phi_i (x\otimes f)$).
  By Schur's Lemma, $\im \Phi_i \subseteq \Iso_{L_i} (W)$.
  \begin{itemize}
  \item \underline{$\im \Phi_i = \Iso_{L_i} (W)$:}
    
    Let $f_1,\dotsc,f_l$ be a basis of $L_i^\prime = \Hom_A (L_i,W) = \Hom_A (L_i,\bigoplus_{i=1}^s L_i^{\oplus n_i})$, compatible with $\oplus$.
    Let
    \begin{equation*}
      f_i (x)=
      \begin{cases}
        x_i & \text{if } f_i \text{ is contained in the $i$-th copy of } L_i^\prime = \bigoplus_{i=1}^{n_i} \Hom_A (L_i,L_i)  \\
        0 & else
      \end{cases}\; ,
    \end{equation*}
    where $x_i = (0,\dotsc,x,\dotsc,0) \in L_i^{\oplus n_i}$ in the $i$-th component.
    This shows the claim.
    Therefore $\im \Phi_i \supseteq \Iso_{L_i} (W)$.

  \item \uline{$\Phi_i$ is injective and and we get an isomorphism $\Phi = \bigoplus_{i=1}^s \Phi_i \colon \bigoplus_{i=1}^s L_i \otimes_{D_i^{\mathrm{op}}} L_i^\prime \to W$, and moreover $\Phi_i$ and $\Phi$ are $A \otimes A^\prime$-module homomorphisms:}
    
    Let $k = \bar{k}$.
    We know that then $\Phi_i$ is surjective and
    \begin{equation*}
      \dim (L_i \otimes L_i^\prime) = \dim L_i \dim L_i^\prime = \dim L_i n_i = \dim L_i^{\oplus n_i} = \Iso_{L_i}(W) \; .
    \end{equation*}
    Therefore $\Phi_i$ is bijective onto $\Iso_{L_i}(W)$, which means that $\Phi$ is bijective.
    For general $k$:
    We know $L_i^\prime$ is an irreducible $A^\prime$-module on which $M_{n_i \times n_i}(D_i)$ with $D_i = \End_A(L_i)$ acts non-trivially.
    Therefore $L_i^\prime \cong D_i^{n_i}$ as $M_{n_i \times n_i}$-modules.
    In particular as $D_i$-module it is $n_i$ copies of $D_i$.
    Therefore $L_i \otimes_{D_i^{\mathrm{op}}} D_i^{n_i} \cong L_i^{\oplus n_i}$, which implies $\dim L_i \otimes_{D_i^{\mathrm{op}}} D_i^{n_i} = n_i \dim L_i = \dim \Iso_{L_i} (W)$. \qedhere
  \end{itemize}
\end{proof}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

\begin{definition}
  Let $(G,k[G])$ be an affine algebraic group, and $(X,k[X])$ be an affine algebraic variety.
  An \textbf{action} of $(G,k[G])$ on $(X,k[X])$ (or short: $G$ acts on $X$ as affine varieties) is a morphism of affine algebraic varieties
  \begin{align*}
    \alpha \colon G \times X &\longrightarrow X\\
    (g,x) &\longmapsto \alpha((g,x)) = g.x \; ,
  \end{align*}
  such that $\alpha$ defines an action of the group $G$ on the set $X$.
  We then write $G \curvearrowright X$ or $G \stackrel{\alpha}{\curvearrowright} X$.
  We also call this action \textbf{algebraic}.
\end{definition}
\begin{remark}
  One can define orbits, fixed points, transitive action etc. as usual.
\end{remark}
\begin{definition}
  Let $G \curvearrowright X$ and $Y,Z \subseteq X$.
  \begin{enumerate}
  \item $\Trans_G(Y,Z) = \setdef{g \in G}{\forall y \in Y \colon g.y \in Z}$, the \textbf{transporter} from $Y$ to $Z$.
  \item $C_G(Y) = \bigcap_{y \in Y} G_y$, where $G_y = \setdef{g \in G}{g.y=y}$, the \textbf{stabilizer} of $Y$ resp. of $y \in Y$.
  \end{enumerate}
\end{definition}
\begin{lemma}\label{lemix.1}
  Let $(X,k[X])$, $(X^\prime,k[X^\prime])$, $(Y,k[Y])$, $(Y^\prime,k[Y^\prime])$ be affine algebraic varieties.
  \begin{enumerate}[label=(\alph*)]
  \item For any $y \in Y$ the maps
    $X \to X \times Y$, $x \mapsto (x,y)$ and $Y \to X \times Y$, $y \mapsto (x,y)$ are morphisms
  \item If $\varphi_1 \colon X \to X^\prime$ and $\varphi_2 \colon Y \to Y^\prime$ are morphisms, then $\varphi_1 \times \varphi_2 \colon X \times Y \to X^\prime \times Y^\prime$, $(x,y) \mapsto (\varphi_1 (x),\varphi_2 (y))$ is a morphism.
  \end{enumerate}
\end{lemma}
\begin{proof}
  Exercise sheet.
\end{proof}
\begin{proposition}\label{propix.2}
  Let $G \stackrel{\alpha}{\curvearrowright} X$ and let $Y,Z \subseteq X$ be subsets, $Z$ closed.
  \begin{enumerate}[label=(\alph*)]
  \item $\Trans_G(Y,Z) \subseteq G$ is closed
  \item $C_G (Y) \subseteq G$ is closed, $G_y \subseteq G$ is closed for all $y \in Y$
  \item $X^G \subseteq X$ is closed    
  \end{enumerate}
\end{proposition}
\begin{proof}~
  \begin{enumerate}[label=(\alph*)]
  \item Let $y \in Y$.
    The \textbf{orbit map} for $y$
    \begin{align*}
      \alpha_y \colon G & \longrightarrow X \\
      g &\longmapsto g.y
    \end{align*}
    is a morphism, because it is the composition of the morphisms $G \to G\times X \xrightarrow{\alpha} X$, $g \mapsto (g,y) \mapsto g.y$ (\Cref{lemix.1}).
    On the other hand, $Z$ being closed implies that $\alpha^{-1}_y (Z)$ is closed (as morphisms are continuous).
    Now $\alpha_y^{-1} (Z) = \setdef{g \in G}{g.y\in Z}$, and so $\Trans_G(Y,Z) = \bigcap_{y \in Y} \alpha_y^{-1} (Z)$ is closed, which implies (a).
  \item $G_y = \Trans_G(\set{y},\set{y})$ is closed by (a), since points are closed in $X$ and then also $C_G (Y) = \bigcap_{y \in Y} G_y$ is closed.
  \item Let $g \in G$ and $\varphi \colon X \to X\times X$, $x \mapsto (x,g.x)$ (which is a morphism by \cref{lemix.1}).
    Then $X^G = \setdef{x \in X}{g.x = x} = \varphi^{-1} \left( \set{(x,x) \in X\times X} \right)$, the ``diagonal'' in $X \times X$, is closed, since it is a zero set of some polynomial.
    Furthermore, $\varphi$ is continuous, so $\varphi^{-1}(\mathrm{diagonal})$ is closed.
    Then $X^G = \bigcap_{g \in G} X^g$ is also closed. \qedhere
  \end{enumerate}
\end{proof}
\begin{corollary}\label{corix.3}
  Let $(G,k[G])$ be an affine algebraic group.
  Let $H \subseteq G$ be a closed subgroup and let $x \in G$.
  $N_G(H) = \setdef{g \in G}{gHg^{-1} \subseteq H}$, the \textbf{normalizer} of $H$, and $C_G(x) = \setdef{g \in G}{gxg^{-1} = x}$, the \textbf{centralizer} of $x$ are closed subgroups. 
\end{corollary}
\begin{proof}
  $C_G(x) = G_x$, where we act on $G$ by conjugation (this is algebraic, check this).
  $N_G(H) = \Trans_G(H,H)$ is closed by \Cref{propix.2}.
\end{proof}
\underline{Warining:}  Orbits are not closed in general!
Example: $G = G_m = \GL_1(\C) \curvearrowright \C$ by multiplication (this is algebraic).
The orbit of $0$ is $\set{0}$, which is closed, but the orbit of $1$ is $\C \setminus \set{0}$, which is not closed.

Assume $G \stackrel{\alpha}{\curvearrowright} X$.
For $g \in G$, consider
\begin{align*}
  \beta_g \colon X &\longrightarrow X \\
  x &\longmapsto g^{-1}.x \; ,
\end{align*}
which is a morphism since it is the composition of $X \to G\times X \xrightarrow{(\inv,\id)} G \times X \xrightarrow{\alpha} X$, $x \mapsto (g,x) \mapsto (g^{-1},x) \mapsto g^{-1}.x$, by \Cref{lemix.1}.
Hence we get a comorphism $\beta^\ast_g \colon k[X] \to k[X]$, $f \mapsto f \circ \beta_g$.

Note: $\forall x \in X \colon \beta_g^\ast (f)(x) = f(g^{-1}.x)$.
Moreover $\forall g,h \in G \colon \beta^\ast_{gh} = \beta^\ast_g \circ \beta^\ast_h$.

this means that if $(X,k[X])$ is an affine algebraic group, then we can also consider for $g \in G$ the map $\gamma \colon X \to X$, $x \mapsto xg$ and we get a comorphism $\gamma^\ast \colon k[G] \to k[G]$, $\gamma^\ast_g (f) = f \circ \gamma_g$.

Note: $ \forall g \in G, x \in X \colon \gamma_g^\ast(f)(x) = f(xg)$.
Moreover $\gamma^\ast_{gh} = \gamma_g^\ast \circ \gamma_h^\ast$.

\begin{definition}[left/right translation on functions]
  For any affine algebraic group $(G,k[G])$ we obtain a representation of the (ordinary) group $G$ on $k[G]$ via
  \begin{align*}
    \lambda \colon G &\longrightarrow \GL(k[G]) \\
    g &\longmapsto \lambda_g := \beta^\ast_g \; ,
  \end{align*}
  called the \textbf{left translation on functions}, and a representation
  \begin{align*}
    \rho \colon G &\longrightarrow \GL(k[G]) \\
    g &\longmapsto \rho_g := \gamma^\ast_g \; ,
  \end{align*}
  called the \textbf{right translation of functions}.
\end{definition}

We shall now characterize elements in closed subgroups.

\begin{lemma}\label{lemix.5}
  Let $(G,k[G])$ be an affine algebraic group and let $H \subseteq G$ be a closed subgroup.
  Let $I \trianglelefteq k[G]$ be the vanishing ideal for $H$, that is $I = \setdef{f \in k[G]}{\forall h \in H\colon f(h) =0}$.
  Then $H = \setdef{g \in G}{ \rho(I) \subseteq I}$.
\end{lemma}
\begin{proof}
  \underline{``$\subseteq$'':} Let $g \in H$, $f \in I$.
  Then $\rho_g (f)(h) = f(hg) = 0$, as $hg \in H$.

  \underline{``$\supseteq$'':} Let $\rho_g(I) \subseteq I$.
  Then for all $f \in I$ we have $f(g) = f(eg) = \rho_g (f)(e)=0$.
  Therefore $f(g) = 0$ for all $f \in I$, which implies $g \in H$.
  (Indeed: $H$ closed implies $H = V(J)$ for some ideal $J$ in $k[G]$.
  Moreover we saw already that $I(V(J)) = \sqrt{J} = J$.)
  We have $g \in V(I) = V(I(V(J))) = V(J) = H$.
\end{proof}
\begin{proposition}\label{propix.6}
  Let $G \stackrel{\alpha}{\curvearrowright} X$ and let $F \subseteq k[X]$ be a finite dimensional subspace.
  Then
  \begin{enumerate}[label=(\alph*)]
  \item There exists a finite dimensional subspace $E \subseteq k[X]$ such that $F \subseteq E$ and $E$ is stable under all left translations of functions, that is $\lambda_g(E) \subseteq E$ for all $g \in G$.
  \item $F$ is stable under all left translations if and only if $$\alpha^\ast (F) \subseteq k[G] \otimes F \subseteq k[G]\otimes k[X] \cong k[G\times X] \,.$$
  \end{enumerate}
\end{proposition}
\begin{proof}~ 
  \begin{enumerate}[label=(\alph*)]
  \item First assume that $F = \spann\set{f}$.
    Let $\alpha^\ast(f) = \sum_{i=1}^n r_i\otimes h_i \in k[G] \otimes k[X] \cong k[G\times X]$ with $n$ minimal.
    Then $\lambda_g(f) (x) = f(g^{-1}.x) = \sum_{i=1}^n r_i(g^{-1}) h_i(x)$ for all $x \in X$.
    Then $\lambda_g (f) = \sum_{i=1}^n r_i (g^{-1})h_i \in k[X]$.
    The $h_i$, $1\leq i \leq n$, span a finite dimensional subspace of $k[X]$ which contains all $\lambda_g (f)$, $g \in G$.
    Therefore $$E := E_f := \spann\setdef{\lambda_g (f)}{g \in G}$$ is finite dimensional and stable under all left transformations because $$\lambda_{g_1}(\lambda_{g_2}(f)) = \lambda_{g_1 g_2} (f) \in E \,.$$
    This implies (a) for the case $F = \spann\set{f}$.

    For a genral finite dimensional $F$, choose a basis $B$ and tale $E := \sum_{f \in B} E_f$, which concludes (a).
  \item Let $\setdef{f_i}{i \in I}$ be a basis of $F$, and extend this to a basis $\setdef{f_i}{i \in J}$ of $k[X]$.
    Now for $f \in F$ we have $\alpha^\ast (f) = \sum_{i \in I} r_i \otimes f_i + \sum_{i \in J\setminus I} r_i \otimes f_i \in k[G] \otimes k[X] \cong [G\times X]$ for some $r_i \in k[G]$, almost all zero.
    Now for $g \in G$, we obtain
    $$\lambda_g(f) = \sum_{i \in I} r_i (g^{-1}) f_i + \sum_{i \in J\setminus I} r_i (g^{-1}) f_i \in k[X] \,.$$
    Hence $\lambda_g(f) \in F$ if and only if $r_i(g^{-1}) = 0$ for all $i \in J\setminus I$.
    Hence
    \begin{align*}
      \forall g \in G \colon \lambda_g(f) \in F & \iff \forall i \in J\setminus I, \, \forall g \in G \colon r_i (g^{-1}) = 0 \\
                                                & \iff \forall i \in J\setminus I, \, \forall g \in G \colon r_i(g) = 0  \\
                                                & \iff \forall i \in J\setminus I \colon r_i = 0 \\
                                                & \iff \alpha^\ast (f) \in k[G] \otimes F
    \end{align*}
  \end{enumerate}
\end{proof}
\begin{corollary}\label{corix.7}
  Let $(G,k[G])$ be an affine algebraic group.
  Then every affine finite dimensional subspace $F \subseteq k[G]$ is contained in a finite dimensional subspace $E \subseteq k[G]$ which is stable under both all left and all right translations.
\end{corollary}
\begin{proof}
  Exercise
\end{proof}

We know that linear algebraic groups are affube algebraic groups.
\begin{theorem}\label{thmix.8}
  Let $(G,k[G])$ be an affine algebraic group.
  Then it is isomorphic to linear algebrac group.
  ``\textit{Affine algebraic groups are linear}''.
\end{theorem}
\begin{proof}
  $k[G]$ is finitely generated as an algebra.
  We can pick generators $f_1,\dotsc,f_n$ such that they are a basis of a finite dimensional subspace $F \subseteq k[G]$, and stable under right translations (\Cref{propix.6}, \Cref{corix.7}).
  Let $\alpha \colon G\times G \to G$, $(g,x) \mapsto xg$.
  Then $\alpha^\ast (F) \subseteq k[G] \otimes F\subseteq k[G] \otimes k[G] \subseteq k[G\times G]$ (use \cref{propix.6}).
  Thus $\alpha^\ast(f_i) = \sum_{j=1}^n a_{j,i} \otimes f_j \in k[G] \otimes F$ for some $a_{j,i} \in k[G]$.
  We have $ \rho_g(f_i)(x) = f_i (xg) = \sum_{j=1}^n a_{j,i}(g) f_j(x)$ for all $x \in G$.
  Therefore $$\rho_g (f_i) = \sum_{j=1}^n a_{j,i} (g) f_j \in k[G] \,.$$
  We define $A_g := (a_{i,j}(g))_{i,j \in [n]} \in M_{n \times n} (k)$.
  Note that $\res{\rho_g}{F}$ is described in the basis $f_1,\dotsc,f_n$ by the matrix $A_g$.
  Note that $A_{g^{-1}}$ is the inverse of $A_g$.
  This means that we obtain a map $\beta \colon G \to \GL_n (k)$, $g \mapsto A_g$.

  \underline{Claim 1:} $\beta$ is injective.

  \underline{Claim 2:} $H := \im \beta $ is closed.
\end{proof}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

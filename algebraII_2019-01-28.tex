\begin{example}
	Let $V,W$ be finite dimensional rational representation of $(G,k[G])$. Then $V \otimes W$ is a finite dimensional rational representation via $g.(v \otimes w) = g.v \otimes g.w$ for all $v \in V, W \in W, g \in G$. 
	
	Consider the corresponding map
	\begin{align*}
		V & \longrightarrow k[G] \otimes V \\
		v_i & \longmapsto \sum a_{ij} \otimes v_j 
	\end{align*}
	where $\{v_i\}$ is a basis of $V$. 
	Similarly, 
	\begin{align*}
		W & \longrightarrow k[G] \otimes W \\
		w_i & \longmapsto \sum a_{ij} \otimes w_j 
	\end{align*}
	with $\{w_i\}$ a basis of $W$. 
	
	Then define 
	\begin{align*}
		V \otimes W & \longrightarrow k[G] \otimes V \otimes W \\v_{i_1} \otimes w_{i2} & \longmapsto \sum_{j,k} a_{i_1j_1} b_{i_2j_2} v_{j_1} \otimes w_{j_2}
	\end{align*}
	This corresponds to the action given above. 
\end{example}
\begin{lemma} \label{lemXIV.4}
	Any finite dimensional representation $U$ of an affine algebraic group $(G,k[G])$ is a subrepresentation of $k[G]^{\oplus m}$ ($m = \dim U$) with the action of $G$ on $k[G]$ by left translation an on $k[G]^{\oplus m}$ by $g.(f_1, \dots , f_m) \coloneqq (gf_1, gf_2, \dots , gf_m)$ for $g \in G, f_i \in k[G]$ with $m=\dim U$. 
\end{lemma}
\begin{proof}
	Let $u_1, \dots u_m$ be a basis of $U$ and $u^1, \dots , u^m$ be the dual basis of $U^\ast$. Define \begin{align*}
                                                                                                              \Phi \colon U &\longrightarrow k[G]^{\oplus m} \\
                                                                                                              u & \longmapsto (\psi_1^u, \dots , \psi_m^u)
	\end{align*}
	where $\psi_i^u(g) = u^i(g^{-1}.u)$. 
	
	\textbf{Claim.} $\psi_i^u \in k[G]$ for all $1 \le i \le m$ and $u \in U$.
	
	\textit{Proof of Claim.} Consider $\gamma \colon G \to G \times U \xrightarrow{\operatorname{inv} \times \id} G \times U \to U$ given by \[g \mapsto (g,u) \mapsto (g^{-1},u) \mapsto g^{-1}.u \,.\]This induces $\gamma^\ast k[U] \to k[G]$. Now $k[U]=\{\text{poly. fcts. } U \to k \} \ni u^i$. So $\gamma^\ast (u_i) \in k[G]$ for all $1 \le i \le m$ and $\gamma^\ast(u^i)(g)=u^i (g^{-1}.u)$. 
	All in all, $\psi_i^u \in k[G]$. 
	
	We proved that $\Phi$ is well-defined. 
	
	Now \begin{align*}\Phi(g.u) = (\psi_i^{g.u})_{1 \le i \le m} && \text{with} && \psi_i^{g.u}(x) = u^i(x^{-1}g.u) \\ g.\Phi(u) = (g.\psi_i^u)_{1 \le i \le m} && \text{with} && g.\psi_i^u(x)= \psi_i^u(g^{-1}x) = u^i (x^{-1}g.u) \,. \end{align*} All in all, $\Phi$ is $G$-equivariant and obviously linear.
	
	\textbf{Claim.} $\Phi$ is injective. 
	
	\textit{Proof of Claim.} Clear since \[\psi_i^u(e)=u^i(e^{-1}.u)=u^i(u) \] completely determine $u$.
	
	All in all, $U$ is a subrepresentation of $k[G]^{\oplus m}$. 
\end{proof}
\begin{theorem} \label{theXIV.5}
	Let $V=k^n$ and $G \subset \GL(V)$ be a linear algebraic group. Let $\Det$ be the $1$-dimensional representation of $G$ ($\Det =k$ and $g.v = \det(g)v$).
	
	Then any finite dimensional representation $U$ of $(G,k[G])$ appears after tensoring with some $\underbrace{\Det \otimes \cdots \otimes \Det}_{r \text{ factors}}$ as a quotient of some subrepresentation of a finite direct sum of some $V^{\otimes d}$'s (where $G$ actons on $V$ in the obvious way). 
\end{theorem}
\begin{remark}
	If $r$ can be chosen to be $0$, then $U$ is called a \textbf{polynomial representation}.
\end{remark}
\begin{remark}
	If $V^{\otimes d}$ is semisimple as representation of $G$, then it suffices to understand finite dimensional representations of $G$ to understand representations of $V^{\otimes d}$. 
\end{remark}
\begin{proof}
	Consider 
	\begin{align*}
		\End_k(V) & \cong V^\ast \otimes V \\
		f_v & \mapsfrom f \otimes v \\
		f & \mapsto \sum_{i=1}^n v^i \otimes f(v_i)
	\end{align*}
	where $f_v(w)=f(w)v$ and $\{v_i\}$ is a (standard) basis of $V$ and $v^i$ is the dual basis of $V^\ast$.
	Identify $V^\ast \otimes V \cong M_{n \times n}(k)$ by $v^i \otimes v_j \to E_{ji}$. 
	
	Now multiplication in $G \subseteq \End_k(V)$ is given by \[(a_{ij})_{ij} v ^r \otimes v_s = (a_{ij})_{ij}E_{sr} = \sum a_{is}E_{ir} = \sum a_{is} v^r \otimes v_i \] 
	Therefore, $\End_k(V) \cong V^{\oplus m}$ as a representation of $G$ via left multiplication. 
	
	Let now $A \coloneqq k[\End_k(V)] \cong k[X_{11}, \dots , X_{nn}]$ and $B \coloneqq k[\GL(V)] \cong [X_{11}, \dots , X_{nn}][\det^{-1}]$ and consider \[\psi \colon A \hookrightarrow B \xrightarrow{\psi^\ast} k[G] \,.\] Now $B=\bigcup_{i \ge 0} \det^{-i} A$ where $\det^{-i}(A) = \setdef{f \in B}{\det^i f \in A}$. Now by \Cref{lemXIV.4}, there exists $\Phi \colon U \to k[G]^{\oplus m}$ with $m=\dim U$ such that $U$ becomes a subrepresentation of $k[G]^{\oplus m}$. 
	
	There exists $r \in \N$ such that the image of $\det^r U \coloneqq \psi^\ast (\det^r)\Phi(U)$ is contained in the image of $A^m$ under $\psi^m = \underbrace{\psi \times \psi \times \dotsb \times \psi}_{m \text{ copies}}$. Now $A=k[V^{\oplus m}]$ with $(g.f)(x)=f(g^{-1}.x)$ and $\psi_j, \Phi$ are $G$-equivariant. 
	
	Now $k[V^{\oplus m}] = S(V^{\oplus m}) \twoheadleftarrow T(V^{\oplus m})$ where $S(V^{\oplus m})$ is the symmetric algebra and  $T(V^{\oplus m})$ is the tensor algebra defined by \[T(V^{\oplus m}) = k \oplus (V^{\oplus m}) \oplus (V^{\oplus m})^{\otimes 2} \oplus \cdots \cong k \oplus (V^{\oplus m}) \oplus (V^{\otimes 2})^{\oplus m}  \,. \]
	
	There exists a linear map $F$ from some finite direct sum of $V^{\otimes d}$'s to $k[G]^{\oplus m}$ such that the image of $F$ contains $\det^r U$. 
	But $S$ is a rational representation and all maps are $G$-equivariant. Let $\setdef{u_i}{i \in I}$ be a basis of $\det^r U$ (finite!). Pick preimages of $u_i$ under $F$.Since $S$ is union of finite dimensional representations. These preimages lie in a finite-dimensional subrepresentation spanned by $\setdef{g.u_i}{i \in I, g \in G}=N$. All in all, $F(N) = \det^r U$. 
\end{proof}
\begin{definition}[Redefinition - right module, correction] Let $(C, \Delta_C,\varepsilon)$ be a coalgebra. A \textbf{right comodule for $C$} is a vector space $M$ together with a linear map $\Delta_M \colon M \to M \otimes C$ such that \begin{equation} \tag{C1} \label{C1}\begin{tikzcd}
		M \arrow{r}{\Delta_M} \arrow{d}{\Delta_M} & M \otimes C \arrow{d}{\Delta_M \otimes \id} \\
		M \otimes C \arrow{r}{\id \otimes \Delta_C} & M \otimes C \otimes C
	\end{tikzcd} \end{equation}
	and \begin{equation} \tag{C2} \label{C2}\begin{tikzcd}
		M \arrow{r}{\Delta_M} \arrow{dr}{\id} & M \otimes C \arrow{d}{\id \otimes \varepsilon} \\
		& M \otimes k
	\end{tikzcd} \end{equation}
\end{definition}
\begin{example}
	If $G$ is is a linear algebraic group acting on a finite-dimensional vector space $V$, then 
	\begin{align*}
		\Delta_V \colon V & \longrightarrow V \otimes k[G] \\
		v_j & \longmapsto \sum_i v_i \otimes a_{ij}
	\end{align*}
	where $a_{ij}(g)$ is the $(i,j)$th entry of $g \in GL_n(k)$. We check that \[\begin{tikzcd}
		v_j \arrow[mapsto]{r} \arrow[mapsto]{d} & \sum_k v_k \otimes a_{kj} \\
		\sum_i v_i \otimes a_{ij} \arrow[mapsto]{r} & \sum_i v_i \otimes a_{ik} \otimes a_{kj}
	\end{tikzcd} \]
	and the other condition. 
	
	Note that $\sum_i a_{ij}(g)v_i = g. v_j$
\end{example}
\begin{remark}
	If $(G,k[G])$ is an affine algebraic group, $V$ a rational representation. Then $V$ is a right $k[G]$-module via \begin{align*}
		\Delta_V\colon V & \longrightarrow V \otimes k[G] \\
		v & \longmapsto \sum v_i \otimes f_i
	\end{align*}
	such that $\sum_i f_i(g)v_i=g.v$ where $v_i$ is a basis of $V$.
	
	We check \eqref{C1}. On one hand we have $v \mapsto \sum_i v_i \otimes f_i \mapsto \sum_{i,j} v_{i_j} \otimes f_{ij} \otimes f_i$ such that \[\sum_{i,j} f_{ij}(g)f_i(h)v_{ij}=\sum_{i,j} f_i(h) f_{ij}(g)v_{i_j} = \sum_i f_i(h) \sum_j f_{ij}(g) v_j = g.\left( \sum_i f_i(h)v_i\right) = g.h.v \,. \]   On the other hand we have $v \mapsto v_i \otimes f_i \mapsto \sum_{i,i_1,i_2} v_i \otimes f_{i_1}\otimes f_{i_2}$ where \[\sum_{i,i_1,i_2} f_{i_1}(g)f_{i_2}(h)v_i=\sum_i f_i(gh)v_i = (gh).v \] We omit the proof of \eqref{C2}.
\end{remark}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

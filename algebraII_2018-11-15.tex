\begin{remark}
	$R$ simple $\implies$ $R$ is semisimple. 
\end{remark}
\begin{example}~ \vspace{-\topsep}
	\begin{enumerate}
		\item Let $R=k$ be a field. This is a simple ring.
		\item Let $G$ be a finite group and $k$ be a field of $\cha k \nmid |G|$. By \Cref{maschke} $kG$ is a semisimple ring. 
		\item Let $R=M_{n \times n}(D)$ ($n \in \Z_{>0}$) where $D$ is a division algebra. Then $R$ is a simple ring/$k$-algebra. 
		
		For $1 \le i \le n$. Let $$C_i=\setdef{A \in M_{n \times n}(D)}{\text{nonzero entries only in $i$-th column}} \, .$$
		Then, $R=M_{n \times n}(D)=\bigoplus_{i = 1}^n C_i$ as $R$-modules (because $E_{ab}E_{ji}=\delta_{jb} E_{ai}$) and $C_i \cong D^n$ as $R$-modules via $(E_{i,j})_i \mapsto e_j$. 
		
		Now, $D^n$ is an irreducible $R$-module since $R$ acts transitively on $D^n$ (because then any nonzero submodule is already $D^n$). Therefore, $R \cong \bigoplus_{i=1}^n L$ with $L \cong D^n$ with is irreducible. Therefore, $R$ is simple.  
	\end{enumerate}
\end{example}
\begin{lemma} \label{lemV.5}
	Let $R$ be simple ring. Then $|\Irr(R)|=1$
	\begin{proof}
          Let $R$ simple. By Definition $R=\Iso_L(R)$ for some irreducible $R$-module $L$. Assume $L^\prime$ is an irreducible $R$-module.
          Then pick $0 \neq m \in L^\prime$ and obtain a surjective $R$-module homomorphism
		\begin{align*}
			R & \longrightarrow L' \\
			r & \longmapsto rm
		\end{align*}
		Hence we get a nonzero $R$-module homomorphism $f \colon \Iso_L(R) \to L^\prime$. Therefore, there exists a nonzero $R$-module homomorphism $L \to L^\prime$ which via Schur's Lemma implies $L \cong L^\prime$. 
	\end{proof}
\end{lemma}
\begin{corollary} \label{corV.6}
	$D^n$ is the unique up to isomorphism irreducible $R$-module for $R=M_{n \times n}(D)$.
\end{corollary}
\begin{proposition} \label{propV.7}
	Let $R$ be a semisimple ring and $M$ be an $R$-module. Then, $M$ is semisimple (as an $R$-module).
\end{proposition}
\begin{proof}
	Let $\{m_i\}_{i \in I}$ be a set of generators of the $R$-module $M$. 
	There exists a surjective $R$-Module homomorphism 
	\begin{align*}
		\bigoplus_{i \in I} R & \longrightarrow M \\
		(0,\dots , 0, 1, 0 , \dots) &\longmapsto m_j %j-th component
	\end{align*} 
	Now $R$ is a semisimple ring and therefore $R$ is semimsimple as a left $R$-module. 
	Therefore, $\bigoplus_{i \in I} R$ is a semisimple $R$-module.
	Finally, the quotient $M$ is also semisimple. 
\end{proof}
\begin{proposition} \label{propV.8}
	Let $R$ be a semisimple ring. Then $$R \cong \bigoplus_{i \in I} L_i$$ where $L_i$ is an irreducible $R$-module for all $i$ and with $I$ being finite. 
\end{proposition}
\begin{proof}
	Since $R$ is semisimple, we can find $L_i$ irreducible such that $\varphi \colon R \cong \bigoplus_{i \in J} L_i$ as an $R$-module. 
	
	Write $1=\sum_{i \in J} \varphi^{-1}(e_i)$ with $e_i \in L_i$ with finitely many $e_i$ being nonzero. 
	Let $I \coloneqq \setdef{i \in J}{e_i \neq 0}$. Then $f=\res{\varphi^{-1}}{\bigoplus_{i \in I}L_i} \colon \bigoplus_{i \in I}L_i  \to R$ is injective because $\varphi^{-1}$ is injective. Furthermore, $1 \in \im f$ and then also $r 1 \in \im f$ because $f$ is $R$-module homomorphism. Therefore, $f$ is surjective and therefore an isomorphism. 
\end{proof}
Assume that $R$ is a ring and $M$ an $R$-module. Then $M$ is an $R^\prime \coloneqq \End_R(M)$-module via $f.m=f(m)$ for all $f \in \End_R(M)$ and $m \in M$.
We call $R^\prime$ the \textbf{centralizer of the $R$-action on $M$}. 
What is now the centralizer $R^{\prime\prime}$ of the $R^\prime$-action on $M$. 
By definition: $R^{\prime \prime}=\End_{R^\prime}(M)=\End_{\End_R(M)}(M)$. Are there situations where $R^{\prime \prime}=R$? (Double centralizer property)
\begin{theorem}[Jacobson density I] \label{theV.9}
	Let $R$ be a ring (with $1$) and $M$ a semisimple $R$-module. Consider the map 
	\begin{align*}
		\varphi \colon R & \longrightarrow \End_{\End_R(M)} (M) \\
		r & \longmapsto (m \mapsto rm)
	\end{align*}
	Then $\im \varphi$ is dense in the following sense. Given $f \in \End_{\End_R(M)}(M)$ and $m_1, \dots , m_s \in M$, there exists $a \in R$ such that $f(m_i)=am_i$ for all $1 \le i \le s$. 
\end{theorem}
\begin{remark}~ \vspace{- \topsep}
	\begin{enumerate}
		\item We can consider $\varphi$ as a map $R \to \End_{\End_R(M)}(M) \subset \Maps(M,M)$ with finite topology. Then this become density in a topological sense. 
		\item As a special case, we have $M=R$ ($R$-module via left multiplication). Then the Jacobson density gives 
		\begin{align*}
			\varphi \colon R & \longmapsto \End_{\End_R(M)}(M) =R\\
			r & \longmapsto (m \mapsto rm) \mapsto r
		\end{align*}
		which is an isomorphism of rings. 
	\end{enumerate}
\end{remark}
\begin{proof}
	First of all, $\varphi$ is well-defined. We have
	\begin{align*}
		\varphi(r)(f.m)=\varphi(r)(f(m))=rf(m)=f(rm)=f.(\varphi(r)(m)) \, .
	\end{align*}
	First assume $s=1$. 
	Let $m \coloneqq m_1 \in M$. 
	Since $M$ is as semisimple $R$-module, the submodule $Rm$ has a complement $C$, i.e. $M=Rm \oplus C$ as $R$-module.
	
	Consider $\pi \colon R = Rm \oplus V \xrightarrow{proj} Rm \xrightarrow{incl} R$. 
	Clearly, $\pi \in \End_R(M)$. 
	For any $f \in \End_R(M)$ we have $\pi \circ f = f \circ \pi$. 
	This implies $f(m)=f(\pi(m))=\pi(f(m)) \in Rm$.
	Therefore, there exists $a \in R$ such that $f(m)=am$. 
	
	For the general case, let $m_1, \dots , m_s \in M$ and $f \in \End_{\End_R(M)}(M)$. Define
	\begin{align*}
		\hat f = \bigoplus_{i =1}^s f \colon \qquad \qquad M^s &\longrightarrow M^s \\
		(n_1, \dots , n_s) & \longmapsto (f(n_1), \dots , f(n_s))
	\end{align*}
	Check that $\hat f \in \End_{\End_R(M^s)}(M^s)$ (exercise).
	By the case where $s=1$, we have $a \in \R$ such that $\hat f(m_1, \dots , m_s)=a(m_1, \dots , m_s)$. Therefore, there exists $a \in R$ such that $f(m_i)=am_i$ for all $1 \le i \le s$.
\end{proof}
\begin{corollary}[Density theorem] \label{corV.10}
	Let $k$ be a field. Let $A$ be a $k$-algebra with $1$. Let $M$ be a finite dimensional, semisimple $A$-module. Then 
	\begin{align*}
		\varphi \colon A & \longrightarrow \End_{\End_A(M)}(M) \\
		a & \longmapsto (m \mapsto am)
	\end{align*}
	is surjective. 
\end{corollary}
\begin{proof}
	We have $k \subset \End_A(M)$, hence $\End_{\End_A(M)} \subset \End_k(M)$. Let $m_1, \dots , m_s \in M$ be a basis of $M$. 
	For $f \in \End_{\End_A(M)}(M)$ we find by Jacobson density some $a \in A$ such that $f(m_i)=am_i$ for all $1 \le i \le s$.
	Therefore, $f(m)=am$ for all $m \in M$. 
	All in all, $f=\varphi(a)$. 
\end{proof}
\begin{theorem}[Jacobson density II] \label{theV.11}
	Let $R$ be a ring (with $1$), $N$ be a semisimple $R$-module. Let $n_1, \dots , n_s \in N$ linearly independent over $R$ and let $n_1^\prime, \dots , n_s^\prime \in N$ be arbitrary. Then there exists $r \in R$ such that $rn_i=n_i^\prime$ for all $1 \le i \le s$. 
\end{theorem}
\begin{proof}
	Let $x=(n_1, \dots , n_s) \in N^s$. Now $N$ is semisimple, hence $N^s$ is semisimple. Hence, $Rx$ has complement in $N^s$ say $N^s=Rx \oplus C$. Consider $\pi \colon N^s \xrightarrow{proj} C \xrightarrow{incl} N^s$. Check that $\pi \in \End_R(N^s)$ (exercise).
	Therefore, we can realize $\pi$ as a matrix $(a_{ij})_{1 \le i,j \le s}=A \in M_{s \times s}(\End_R(N))$. 
	Then, $a_{i1}n_1 + a_{i2}n_2 + \dots + a_{is}n_s=0$ for all $i$ (since $\pi(Rx)=0$). 
	Therefore, $a_{ij}=0$ for all $i,j$ because $n_1, \dots n_s$ are linearly independent over $\End_R(N)$. 
	We finally have $A=0 \implies \pi =0 \implies C=\{0\} \implies N^s=Rx$ which was the claim. 
\end{proof}
\begin{corollary}[Burnside theorem - coordinate free] \label{corV.12}
	Let $k = \overline k$ and $V$ be a finite dimensional $k$ vector space and $A \subset \End_k(V)$ be a subalgebra such that $V$ is an irreducible $A$-module. Then $A=\End_k(V)$.
\end{corollary}
\begin{proof}
	We have $\End_A(V)=k$ by Schur's lemma. Now $\varphi \colon A \to \End_{\End_A(V)}(V)=\End_k(V)$ is surjective by \Cref{theV.9}, hence an isomorphism. 
\end{proof}
\begin{corollary}[Burnside theorem - coordinate version] \label{corV.13}
	Let $k = \overline k$ and $A \subset M_{n \times n}(k)$ be a subalgebra such that $k^n$ is irreducible $A$-module. Then, $A=M_{n \times n}(k)$.
\end{corollary}
\begin{corollary} \label{corV.14}
	Let $k = \overline k$ and $A$ be a $k$-algebra and $M$ be a fintie dimensional $A$-module. Then the following are equivalent. 
	\begin{enumerate}
		\item $M$ is irreducible $A$-module.
		\item $\varphi \colon A \to \End_k(M), a \mapsto (m \mapsto am)$ is surjective. 
	\end{enumerate}
\end{corollary}
\begin{proof}
	$\implies$: By Schur's lemma, we have $\End_A(M)=k$ and $\varphi$ is surjective by \Cref{corV.10}. 
	
	$\impliedby$: Let $0 \neq m \im M$. 
	For all $m^\prime \in M$ there exists $\Phi \in \End_k(M)$ such that $\Phi(m)=m^\prime$. 
	Since $\varphi$ is surjective there exists $a \in A$ such that $\varphi(a)=\Phi$. 
	All in all, $m^\prime = \Phi(m)=am$ and $M$ is irreducible. 
\end{proof} 
\begin{corollary} \label{corV.15}
	Let $k= \overline k$ and $A$ be a $k$-algebra. Furthermore, let $M$ be a fintie dimensional, irreducible $A$-module. Then $(\dim_k M)^2 \le \dim_k A$.
\end{corollary}
\begin{proof}
	Consider $\varphi \colon A \to \End_{\End_A(M)}(M)=\End_k(M)$ which is surjective by \Cref{corV.10}. Therefore, $\dim_k \End_k(M)=(\dim_k M)^2 \le \dim_k A$.
\end{proof}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

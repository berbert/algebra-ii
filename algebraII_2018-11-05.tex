Consider $\chi_A(t) = t^n + s_1(A)t^{n-1} + \dotsb + s_{n-1}t + s_n(A)$.
If $A$ is a diagonal matrix with diagonal elements $d_1,\dotsc,d_n$, then $s_i(A) = e_i^{(n)} (d_1,\dotsc,d_n)$ for all $i$ (Vieta's theorem).

\begin{theorem}[Invariant theorem II]\label{theIV.2}
  Let $G = \GL_n(k)$ act on $M_{n\times n}(k)$ by conjugation, that is $S.A = SAS^{-1}$ for $S \in G$ and $A \in M_{n\times n}(k)$.
  Then $\mathcal P_k(M_{n\times n}(k))^G$ is generated as a $k$-algebra by $s_1,\dotsc,s_n$.
  Moreover, those elements are algebraically independent over $k$, that is $k[t_1,\dotsc,t_n] \to \mathcal P_k(M_{n \times n}(k))^G$, $t_i \mapsto s_i$ is an isomorphism of $k$-algebras.
\end{theorem}
\begin{proof}
  It is clear that $s_i \in \mathcal P_k(M_{n\times n})$.
  They are $G$-invariant, because $\chi_A(t)$ is invariant under conjugation.
  Therefore $\{s_1,\dotsc,s_n\} \subseteq \mathcal P_k (M_{n \times n})^G$.
  
  The $s_i$ are algebraically independent:
  Let $P \in \pol{k}{t}{1}{n}$ such that $P(s_1,\dotsc,s_n) = 0$.
  Then $P(s_1, \dotsc,s_n)(A) = 0$ for all $A \in \mathcal P_k (M_{n \times n}(k))$, in particular $P(s_1, \dotsc,s_n)(D) = 0$ for all diagonal $D \in \mathcal P_k (M_{n \times n}(k))$, which means $P(e_1^{(n)},\dotsc,e_n^{(n)})(d_1,\dotsc,d_n) = 0$ for all $(d_1,\dotsc,d_n) \in k^n$.
  But this implies $P(e_1^{(n)},\dotsc,e_n^{(n)}) = 0$, and by the fundamental theorem of symmetric polynomials, $P = 0$.

  The $s_i$ generate $P(M_{n \times n}(k))^G$ as a $k$-algebra:
  Let $f \in \mathcal P_k(M_{n \times n}(k))^G$.
  Since $f$ is polynomial, there exists a $p \in \pol{k}{t}{1,1}{n,n}$ such that $f(A) = p(a_{1,1},\dotsc,a_{n,n})$ for all $A = (a_{i,j})_{i,j \in [n]} \in M_{n \times n}(k)$.
  Define
  \begin{align*}
    \Psi \colon \pol{k}{t}{1,1}{n,n} &\longrightarrow \pol{k}{t}{1}{n}  \\
    t_{i,j} &\longmapsto \begin{cases} t_i & \text{if } i=j \\ 0 &\text{otherwise}\end{cases}
  \end{align*}
  We now define $\bar{p} := \Psi(p)$, hence, if $D = \operatorname{diag}(d_1,\dotsc,d_n)$, then $f(D) = \bar {p} (d_1,\dotsc,d_n)$.
  We will now show that $\bar{p}$ is symmetric.
  We have an isomorphism of algebras $\beta \colon \pol{k}{t}{1}{n} \to \mathcal P (k^n)$, $t_i \mapsto \varphi_i \text{ coordinate function in standard basis}$.
  We know that $\beta$ is $S_n$-equivariant.
  
  It is now enough to show that $\beta(\bar{p})$ is $S_n$-invariant.
  We realise $g \in S_n \subseteq \GL_n(k)$ as a permutation matrix $A_g \in \GL_n(k)$ so that $A_g . \epsilon_i = \epsilon_{\sigma(i)}$, where $\epsilon_i$ are the basis vectors.
  Then, for $D = \operatorname{diag}(d_1,\dotsc,d_n)$ we have
  \begin{align*}
    &A_g D A_{g^{-1}} = \operatorname{diag}(d_{g^{-1}(1)},\dotsc,d_{g^{-1}(n)}) \\
    \implies& (g.\beta(\bar p))(d_1,\dotsc,d_n) = \beta(\bar p)(g^{-1}.(d_1,\dotsc,d_n))\\
    &=\beta(\bar p)(\sum_{i \in [n]} d_i \epsilon_{g^{-1}(i)}) = \beta (\bar p)(d_{g(1)},\dotsc,d_{g(n)})  \\
    &= f(\operatorname{diag}(d_{g(1)},\dotsc,d_{g(n)})) = f(A_{g^{-1}}DA_g) = f(A_{g^{-1}}D(A_{g^{-1}})^{-1})  \\
    &= f(D) = \beta(\bar p)(d_1,\dotsc,d_n)  \\
    \implies& g.\beta (\bar p ) = \beta(\bar p) \text{ for all }g \in S_n \; ,
  \end{align*}
  which shows our claim.

  Therefore, $\bar{p}$ is an $S_n$-invariant symmetric polynomial, and by the fundamental theorem there exists a $Q \in \pol{k}{t}{1}{n}$ such that $\bar p = Q(e_1^{(n)},\dotsc,e_n^{(n)})$.
  Therefore for a diagonal matrix $D = \operatorname{diag}(d_1,\dotsc,d_n)$ we have $f(D) = Q(e_1^{(n)},\dotsc,e_n^{(n)})(d_1,\dotsc,d_n) = Q(s_1,\dotsc,s_n)(D)$, therefore $f - Q(s_1,\dotsc,s_n) = 0$ when restriced to diagonal matrices.

  \textbf{Claim (Zariski Property II):}
  
  If $h \in \mathcal P_k(M_{n \times n}(k))^G$ and $\res{h}{\text{diag. matr.}} = 0$, then $h=0$.
  We will prove this later.

  As a consequence, we have $f - Q(s_1,\dotsc,s_n) = 0$ (on all matrices!), which concludes the theorem that $s_1,\dotsc,s_n$ generate $\mathcal P_k(M_{n \times n})^G$.
\end{proof}
Another family of elements in $\mathcal P_k(M_{n \times n})^{\GL_n(k)}$ (under the conjugation action) are the power traces $\operatorname{Tr}_j \colon A \mapsto \operatorname{Tr}(A^j)$.
\begin{theorem}\label{thmiv.3}
  Let $n \geq 1$ and let $k$ be an infinite filed of characteristic zero or $> n$.
  Then $\operatorname{Tr}_1,\dotsc,\operatorname{Tr}_n$ generate $\mathcal P_k(M_{n \times n}(k))^{\GL_n(k)}$ as a $k$-algebra and are algebraically independent over $k$.
  Hence
  \begin{align*}
    \pol{k}{t}{1}{n} \longrightarrow & \mathcal P_k(M_{n \times n}(k))^{\GL_n(k)}  \\
    t_j \longmapsto & \operatorname{Tr}_j
  \end{align*}
  is an isomorphism of $k$-algebras.
\end{theorem}
\begin{proof}
  Let $D = \operatorname{diag}(d_1,\dotsc,d_n)$ be a diagonal matrix.
  Then $$\operatorname{Tr}_j(D) = \operatorname{Tr}(D^j) = d_1^j + \dotsb + d_n^j = p_j^{(n)}(d_1,\dotsc,d_n) \, .$$
  By \Cref{theIII.14} the $p_j^{(n)}$ generate $\pol{k}{X}{1}{n}^{S_n}$ as a $k$-algebra under the given assumptions on $k$ and are algebraically independent.
  Now argue as in the invariant theorem II (\Cref{theIV.2}) with $e_j^{(n)}$ replaced by $p_j^{(n)}$
\end{proof}
\begin{definition}[Zariski-dense]
  Let $W$ be a finite dimensional $k$-vector space ($k$ infinite field), $X \subseteq W$ is \textbf{Zariski-dense in $W$} if for all $f \in \mathcal P_k (W)$ we have $\res{f}{X} = 0 \implies f= 0$.
  For $X \subseteq Y \subseteq Z$, we say \textbf{$X$ is Zariski-dense in $Y$} if for all $f \in \mathcal P_k (W)$ we have $\res{f}{X} = 0 \implies \res{f}{Y} = 0$.
\end{definition}
%\begin{remark}
%  After \cref{propIII.3}, if $A$ is a filtered algebra, $\operatorname{can} \colon A \to \operatorname{gr}A$ is in general not an algebra homomorphism.
%\end{remark}
\begin{example}
  \begin{enumerate}
  \item
    $X \subseteq k$ infinite subset is Zariski-dense.
  \item
    $U \subseteq W$ vector subspace, $U \neq W$, then $U$ is not dense in $W$.

    Let $\omega_1,\dotsc,\omega_m$ be a basis for $U$.
    Extend this to a basis $\omega_1,\dotsc,\omega_n$ of $W$.
    Consider the map $\pi \colon W \to k$, $\sum_{i \in [n]} \lambda_i \omega_i \mapsto \lambda_n$.
    This map is polynomial and vanishes on $U$, but $\pi \neq 0$.
  \end{enumerate}
\end{example}
Warning: Zariski density depends on $k$, for instance, $\R \subset \C$ is not dense over $\R$, but dense over $\C$.
\begin{lemma}\label{lemIV.4}
  Let $k$ be an infinite field, $k \subseteq L$ a field extension, $W$ a finite dimensional $k$-vector space.
  Let $W_L := L \otimes_k W$.
  Then
  \begin{enumerate}
  \item $k^n \subseteq L^n$ is Zariski-dense over $L$
  \item $W \subseteq W_L$ is Zariski-dense over $L$
  \end{enumerate}
\end{lemma}
\begin{proof}
  Exercise.
\end{proof}

\begin{lemma}\label{lemIV.5}
  With the same assumptions as in \Cref{lemIV.4}, there exists a unique $k$-algebra homomorphism $\operatorname{incl} \colon \mathcal P_k(W) \to \mathcal P_L(W_L)$ such that the diagram
  \begin{equation*}
    \begin{tikzcd}
      W \arrow{r}{can}[swap]{w \mapsto 1 \otimes w} \arrow{d}{f} & W_L \arrow{d}{incl(f)} \\
      k \arrow{r}{incl} & L
    \end{tikzcd}
  \end{equation*}
  commutes for all $f \in \mathcal P_k(W)$.
  Moreover $\operatorname{incl}$ is injective.
\end{lemma}
\begin{proof}
  Let $\omega_1,\dotsc,\omega_n$ be a basis of $W$, and let $\varphi_1,\dots,\varphi_n$ be its coordinate functions in $\mathcal P_k(W)$.
  Then $1 \otimes \omega_1,\dotsc,1\otimes \omega_n$ is a basis of $W_L$.
  Let $\psi_1,\dotsc,\psi_n$ be be the corresponding coordinate functions in $\mathcal P_L(W_L)$.
  Define $\operatorname{incl}(\varphi_j) = \psi_j$.
  This defines a unique $k$-algebra homomorphism $\operatorname{incl} \colon \mathcal P_k(W) \to \mathcal P_L (W_L)$.
  Since the $\psi_1,\dotsc,\psi_n$ are algebraically independent over $L$, the map is injective, since the basis $\varphi^a := \varphi_1^{a_1},\dotsc,\varphi_n^{a_n}$ for $a \in \Z_{\geq 0}^n$ is mapped to linearly independent elements.

  Commutativity:
  Now, for $f \in \mathcal P_k(W)$, we write $f = p(\varphi_1,\dotsc,\varphi_n)$ for some polynomial $p \in \pol{k}{t}{1}{n}$.
  Now
  \begin{align*}
    \operatorname{incl}(f) \circ \operatorname{can}(\sum_{i \in [n]} \lambda_i \omega_i)
    =& \operatorname{incl}(f)(\sum_{i \in [n]} \lambda_i (1\otimes\omega_i)) \\
    =&p(\psi_1,\dotsc,\psi_n)(\sum_{i \in [n]} \lambda_i (1\otimes\omega_i)) \\
    =& p(\lambda_1,\dotsc,\lambda_n)
  \end{align*}
  On the other hand, $(i \circ f)(\sum_{i \in [n]} \lambda_i \omega_i) = i(p(\varphi_q,\dotsc,\varphi_n)(\sum_{i \in [n]} \lambda_i \omega_i)) = i(p(\lambda_1,\dotsc,\lambda_n))$.

  Uniqueness:
  Assume $\operatorname{incl}^\prime$ is another such algebra homomorphism.
  Then \[\operatorname{incl}(f) - \operatorname{incl}^\prime(f) \in \mathcal P_L(W_L)\] for all $f \in \mathcal P_k(W)$.
  By definition (meaning the diagram commutes), we have \[\res{(\operatorname{incl}(f) - \operatorname{incl}^\prime(f)}{W} = 0\,.\]
  Now $W \subseteq W_L$ is dense over $L$.
  Therefore $\operatorname{incl} - \operatorname{incl}^\prime = 0$ globally.
\end{proof}
\begin{corollary}\label{coriv.6}
  Wit the assumptions of \Cref{lemIV.4}, $\Phi \colon \mathcal P_k(W)_L \to \mathcal P_L (W_L)$, $\lambda \otimes f \mapsto \lambda \operatorname{incl}(f)$ is an isomorphism of $L$-algebras.
\end{corollary}
\begin{proof}
  Consider the $k$-basis $\setdef{\varphi^a}{a \in Z_{\geq 0}^n}$ of $\mathcal P_k(W)$ and the $L$-basis $\setdef{\psi^a}{a \in Z_{\geq 0}^n}$ of $\mathcal P_L(W_L)$.
  Then $\Phi (1 \otimes \varphi^a) = \operatorname{incl}(\varphi^a) = \psi^a$.
  Hence $\Phi$ is an isomorphism of $L$-vectorspaces, since it sends a basis to a basis.
  It is an algebra homomorphism by \Cref{lemIV.5}.
\end{proof}
\begin{lemma}\label{lemiv.7}
  Let $W$ be a finite dimensional $k$-vectorspace ($k$ infinite), $0 \neq h \in \mathcal P_k(W)$.
  Define $W_h := \setdef{w \in W}{h(w) \neq 0}$.
  Then $W_h \subseteq W$ is Zariski-dense.
\end{lemma}
\begin{proof}
  Let $f \in \mathcal P_k (W)$ and $\res{f}{W_h} = 0$.
  Then $(fh)(w) = f(w)h(w) = 0$ for all $w \in W$, implying $fh = 0$.
  Since $\mathcal P_k(W)$ is integral domain, we have $f=0$ since $h \neq 0$.
\end{proof}
\begin{corollary}\label{coriv.8}
  $\GL_n(k) \subseteq M_{n \times n}(k)$ is dense.
\end{corollary}
\begin{proof}
  Use \Cref{lemiv.7} with $W = M_{n\times n}(k)$, $h = \operatorname{det}$.
\end{proof}
This proves Zariski Property I.
\begin{lemma}\label{lemIV.9}
  Let $G$ be a group, $W$ a finite dimensional $G$-representation over $k$, $f \in \mathcal P_k(W)^G$.
  \begin{enumerate}
  \item
    If $X \subseteq W$ such that $G.X \subseteq W$ is Zariski-dense and if $\res{f}{X} = 0$, then $f=0$.
  \item
    If there is a Zariski dense $G$-orbit, then $f$ is constant.
  \end{enumerate}
\end{lemma}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

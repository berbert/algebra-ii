\newpage
\section{Representations of Groups}

\begin{definition}[Linear action]
  Let $G$ be a group, $V$ a $k$-vector space and let $G$ act on $V$.
  This action is called \textbf{linear} if the map $\pi_g \colon V \longrightarrow V$ is a linear map for all $g \in G$.
  Then $V$ is called a \textbf{$G$-space}, or a \textbf{representation} of $G$.
\end{definition}

\begin{example}
  If $V$ is a $k$-vector space then $\GL(V)$ acts linearly on V by $g.v = g(v)$ for $g \in G$ and $v \in V$.
  This is called the \textbf{standard representation}.
\end{example}

\begin{remark}
  We have
  \begin{align*}
    \set{\text{linear $G$-actions}} \longleftrightarrow & \set{\text{group homomorphisms $\varphi \colon G \rightarrow \GL (V)$}} \\
    \pi \longmapsto & (g \mapsto \pi_g)
  \end{align*}
\end{remark}

\begin{example}
  \leavevmode
  \begin{enumerate}
  \item
    Let $X$ be a $G$-set.
    Then $kX$ is a representation of $G$ via
    \begin{equation*}
        g.\left( \sum_{y\in C} a_y \chi_y \right)
      = \sum_{y \in X} a_y \chi_{g.y}
    \end{equation*}
    This is called the \textbf{regular representation on $kX$}.
  \item
    Let $V,W$ be representations of $G$ over $k$. Then the $G$-action on $\Maps(V,W)$ induces a $G$-action on $\Hom_k (V,W) \coloneqq \setdef{f \colon V \rightarrow W}{ \text{$f$ is $k$-linear}}$
  \item
    Let $V,W$ be representations of $G$ over $k$.
    Then $V \oplus W$ and $V \otimes W$ are again representations of $G$, called the \textbf{direct sum} and \textbf{tensor product} of~$V$ and~$W$, via $g.(v,w) = (g.v,g.w)$ and $g.(v \otimes w) = (g.v) \otimes (g.w)$ extended linearly for $g \in G$ and $v \in V$, $w \in W$.
  \end{enumerate}
\end{example}

\begin{definition}[Subrepresentation, proper subrepresentations, irreducible representations, indecomposable representations, completely reducible representations]
  Let $V$ be a representation of $G$ over $k$.
  \begin{itemize}
    \item
      A \textbf{subrepresentation} of $V$ is a vector subspace $U$ of $V$ such that $g.u \in U$ for all $g \in G$ and $u \in U$.
    \item
      A subrepresentation~$U$ of~$V$ is \textbf{proper} if $U \neq V$ and $U \neq 0$.
    \item
      The representation~$V$ is \textbf{irreducible} if $V \neq 0$ and there exist no proper subrepresentations.
    \item
      The representation~$V$ is \textbf{indecomposable} if there is no decomposition $V = U_1 \oplus U_2$ where $U_1,U_2$ are proper subrepresentations.
    \item
      The representation $V$ is \textbf{completely reducible} if $V = \bigoplus_{i\in I} V_i$, where the $V_i$ are irreducible subrepresentations.
  \end{itemize}
\end{definition}

\begin{remark}
  It is clear that irreducible representations are indecomposable.
  The converse is not true in general.

  Example:
  The group $G = \setdef{
  \begin{psmallmatrix}
    a & b \\
    0 & c
  \end{psmallmatrix}
  }{ a,b,c \in \C, a,c \neq 0}$
  acts on $V = \C^2$ by standard action.
  Then $U \coloneqq \langle \begin{psmallmatrix}1\\0\end{psmallmatrix}\rangle$ is a \textbf{proper subrepresentation} of $V$.
  It follows that $V$ is not irreducible.
  But $V$ is indecomposable, since $U$ is the unique proper subrepresentation of~$V$.
  
  (\textit{Explanation: Assume $U^\prime = \langle\begin{psmallmatrix}x\\y\end{psmallmatrix}\rangle$ is a proper subrepresentation.
  Then $\begin{psmallmatrix} 1 & 1 \\ 0 & 1 \end{psmallmatrix} \begin{psmallmatrix}x\\y\end{psmallmatrix} = \begin{psmallmatrix}x+y\\y\end{psmallmatrix} \in U^\prime$. Then $\begin{psmallmatrix}y\\0\end{psmallmatrix} \in U^\prime$, which implies $U^\prime = Y$.})
  
  The representation~$V$ is also not completely reducible.
\end{remark}

\begin{definition}[Group algebra]
  Let $G$ be a group and $k$ a field.
  The \textbf{group algebra of $G$ over $k$} is the $k$-algebra given by the $k$-vector space
  \begin{equation*}
    kG \coloneqq \{\, f \colon G \rightarrow k \mid \text{$\supp(f)$ is finite} \, \}
  \end{equation*}
  with the multiplication of functions
  \begin{equation*}
    (f_1 \cdot f_2)(x) = \sum_{y \in G} f_1 (y) f_2 (y^{-1}x)
  \end{equation*}
  with unit $1 = \chi_e$.
  (\textit{Indeed: $(f \cdot \chi_e)(x) = \sum_y f(y)\chi_e (y^{-1}x) = f(x)$ and $\chi_e(y)f(y^{-1}x) = f(x)$ for all $f \in kG$.})
  To check: associativity and distributivity.
  \begin{remark}
    The group algebra can be defined in the same way over any commutative ring with one.
    We write
    \begin{equation*}
      \sum_{g \in G} a_g g \coloneqq \sum_{g \in G} a_g \chi_g
    \end{equation*}
    where $a_g \in k$ and almost all $a_g = 0$.
  \end{remark}
\end{definition}

\begin{lemma} \label{lemii1}
  The algebra structure on $kG$ is given by extending bilinearly the multiplication on $G$.
\end{lemma}
\begin{proof}
  \begin{align*}
    \chi_g \cdot \chi_h (x)
    = \sum_{y \in G} \chi_g(y)\chi_h(y^{-1}x)
    = \begin{cases}
        1 & \text{if } h = g^{-1}x \,, \\
        0 & \text{otherwise} \,.
      \end{cases}  = \chi_{gh} (x)
  \end{align*}
  The convolution product is by definition the bilinear extension.
\end{proof}

\begin{note}
  The group algebra $kG$ is commutative if and only if $G$ is commutative/abelian.
\end{note}

\begin{lemma} \label{lemii2}
  Let $G$ be a group , $V$ a vector space.
  Then
  \begin{align*}
    \{ \text{linear $G$-actions on $V$} \} & \longleftrightarrow \{ \text{$kG$-module structures on $V$}\} \\
    (\pi\colon G\times V \rightarrow V) & \longmapsto \left( \left(\sum_{g \in G} a_g g \right).v \coloneqq \sum_{g \in G} a_g (g.v) \right)
  \end{align*}
\end{lemma}

\begin{proof}
  Check yourself.
\end{proof}

\begin{definition}[Morphism of representations]
  Let $V,W$ representations of $G$ over $k$.
  A \textbf{morphism of representations from $V$ to $W$} is a linear, $G$-equivariant map $f \colon V \rightarrow W$.
  Denote $$\Hom_G(V,W) \coloneqq \{\, f \colon V \rightarrow W \text{ morphism of representations}\,\}$$ and $\End_G(V) \coloneqq \Hom_G(V,V)$.
\end{definition}

\begin{note}
  $\Hom_G(V,W)$ is a vector space.
  Write $V \cong W$ if there exists an isomorphism $f \colon V \rightarrow W$.
\end{note}

\begin{lemma} \label{lemii3}
  Let $G$ be a group and $k$ a field.
  The representations of $G$ over $k$ together with morphisms of representations form a category, which we call $\operatorname{Rep}_k(G)$, that is for all $G$-representations $V$ we have $\id_V \in \End_G(V)$ and for all $G$-representations $V$, $W$ and $Z$ and for all $f \in \Hom_G(V,W)$ and $g \in \Hom_G(W,Z)$ we have $g \circ f \in \Hom_G(V,Z)$.
\end{lemma}
\begin{proof}
  See \Cref{lemI.2}.
\end{proof}

\begin{example}
  For a field $k$, the $k$-vector spaces together with $k$-linear maps form a category $\operatorname{Vect}_k$.
\end{example}

\begin{corollary}
  Let $k$ be a field.
  The assignments
  \begin{align*}
    F\colon V & \longmapsto V^G \\
    (f \colon V \to W) & \longmapsto \left(f^G \colon V^G \rightarrow W^G\right) \\
    \left(
    \begin{tabular}{c}
      representations of $G$ over $k$,  \\
      morphisms of representations
    \end{tabular}
    \right)
    &\phantom{\longrightarrow}
    \left(
    \begin{tabular}{c}
      $k$-vectors spaces, \\
      $k$-linear maps
    \end{tabular}
    \right)
  \end{align*}
  defines a functor from $\operatorname{Rep}_k$ to $\operatorname{Vect}_k$, i.e.\ $F(f_1 \circ f_2) = F(f_1) \circ F(f_2)$ and $F(\id_V)=\id_{F(V)}$ for all representations $V$ of $G$ over~$k$ and $f_1,f_2$ morphisms of representations, whenever $f_1 \circ f_2$ makes sense.
\end{corollary}
\begin{proof}
  See \Cref{lemI.4} and \enquote{linear}.
\end{proof}

$F$ is the functor of $G$-invariants.

\begin{lemma} \label{lemii5}
  If $f \colon V \rightarrow W$ is a morphism of representations of $G$, then $\ker f$ and $\im f$ are subrepresentations of $V$, respectively of $W$.
\end{lemma}

\begin{proof}
  Both $\ker f$ and $\im f$ are subspaces, because $f$ is linear.
  Let $g \in G$ and $x \in \ker f$.
  Then
  \[
    f(g.x) = g.(f(x)) = g.0 = 0 \,,
  \]
  which shows that~$g.x \in \ker f$, and hence that~$\ker f$ is a subrepresentation.
  Let $y \in \im f$, $ y = f(x)$ for some $x \in V$.
  Then $g.y = g.(f(x)) = f(g.x) \in \im f$.
\end{proof}

\begin{remark}
  It can be shown that $\operatorname{Rep}_k(G)$ is an abelian category.
\end{remark}

\begin{lemma}[Schur] \label{schur}
  Let $G$ be a group and $V,W$ irreducible representations of $G$ over~$k$.
  \begin{enumerate}[label=(\alph*)]
  \item We have
    \[\begin{aligned}
      \Hom_G(V,W)
      \begin{cases}
        = 0     & \text{if $V \ncong W$}  \,, \\
        \neq 0  & \text{if $V \cong W$} \,,
      \end{cases} \end{aligned}
    \]
    and if $V \cong W$ then every morphism~$V \to W$ is an isomorphism.
  \item
    If $k = \bar{k}$ and $V,W$ are finite-dimensional, then
    \begin{equation*}
      \Hom_G(V,W) \cong
      \begin{cases}
        k & \text{if $V \cong W$} \,,  \\ 
        0 & \text{if $V \not\cong W$} \,.
      \end{cases}
    \end{equation*}
  \end{enumerate}
\end{lemma}

\begin{proof}~ 
  \begin{enumerate}[label=(\alph*)]
  \item
    If $f \in \Hom_G(V,W)$ with~$f \neq 0$, then $\ker f \neq V$ and $\im f \neq 0$.
    Then \Cref{lemii5} implies $\ker f = 0$ since $V$ is irreducible and $\im f = W$ since $W$ is irreducible.
    Then $f$ is an isomorphism.

    If $V \cong W$ then $\Hom_G (V,W) \neq 0$ by definition.
    Then automatically every nonzero morphism is an isomorphism by the previous paragraph.
  \item
    Assume $V \cong W$ and $\alpha, \beta \in \Hom_G(V,W)$.
    It is enough to show $\beta = \lambda \alpha$ for some $\lambda \in k$.
    By 1), $\alpha$ has an inverse $\alpha^{-1}$, which is again a morphism.
    Then $\alpha^{-1} \circ \beta \in \End_G(V)$.
    If $k = \bar{k}$, and $V$ is finite-dimensional, then $\alpha^{-1} \circ \beta$ has an eigenvalue.
    This implies $K \coloneqq \ker (\alpha^{-1} \circ \beta - \lambda \id_V) \neq 0$ for some $\lambda \in k$.
    Now $\alpha \circ \beta - \lambda \id_V \in \End_G(V)$ (check!), thus $K$ is a subrepresentation of $V$, hence $V=K$ since $V$ is irreducible and $K \neq 0$.
    It follows that $\alpha^{-1} \circ \beta = \lambda \id_V$ and therefore $\beta = \lambda \alpha$.
  \qedhere
  \end{enumerate}  
\end{proof}

\begin{corollary}
  If $k = \bar{k}$ and $V_i$ with $1\leq i \leq r$ are finite-dimensional irreducible pairwise non-isomorphic representations of $G$ over $k$.
  Let $W_i \coloneqq V_i^{\oplus n_i} \coloneqq V_i \oplus \dotsb \oplus V_i$ \textup($n_i$ copies\textup) for some $n_i \in \Z_{>0}$ \textup(a representation of $G$\textup).
  Then
  \[
          \End_G(W_1 \oplus \dotsb \oplus W_r)
    \cong \operatorname{M}_{n_1 \times n_1}(k) \oplus \dotsb \oplus \operatorname{M}_{n_r \times n_r}(k)
  \]
  as algebras/rings.
\end{corollary}
\begin{proof}
  \begin{align*}
         {}& \End_G (W_1 \oplus \dotsb \oplus W_r)  \\
        ={}& \End_G(V_1^{\oplus n_1} \oplus \dotsb \oplus V_r^{\oplus n_r})  \\
    \cong{}& \End_G(V_1^{\oplus n_1}) \oplus \dotsb \oplus \End_G(V_r^{\oplus n_r})  \\
    \cong{}& \operatorname{M}_{n_1 \times n_1}(k) \otimes \dotsb \otimes \operatorname{M}_{n_r \times n_r}(k) \,.
    \qedhere
  \end{align*}
\end{proof}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

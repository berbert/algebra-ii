\begin{definition}[isotypical decomposition]
  Let $M$ be an $R$-module and $L$ an irreducible $R$-module.
  Then
  \begin{equation*}
    \operatorname{Iso}_L(M) = \sum_{\substack{E \subseteq M \text{ submodule} \\ E \cong L \text{ as $R$-modules}}} E \subseteq M
  \end{equation*}
  is then \textbf{$L$-isotypical component of $M$}.
\end{definition}
\begin{lemma}[Schur's Lemma for $R$-modules]\label{lemv.3}
  Let $R$ be a ring and $M$ an irreducible $R$-module.
  Then
  \begin{enumerate}
  \item
    If $N$ is an irreducible $R$-module and $f \colon M \to N$ an $R$-module homomorphism, then $f = 0$ or $f$ is an isomorphism
  \item
    $\End_R(M)$ is a skew field
  \item
    If $R$ is a $k$-algebra, then $\End_R (M)$ is a division $k$-algebra
  \item
    If $R$ is a $k$-algebra, $\bar k = k$ and $\dim_k M < \infty$, then $\End_R (M) = k$
  \end{enumerate}
\end{lemma}
\begin{proof}
  \begin{enumerate}
  \item omitted
  \item omitted
  \item omitted
  \item
    Claim:  If $D$ is a division algebra over $k$ and $\dim_k D < \infty$, then $D = k$.
    Let us assume this claim.
    We have $\End_R (M) \subseteq \End_k (M)$ by assumption, hence by (3) it is a finite dimensional algebra, which by the claim implies (4).

    To show is the claim.
    Let $a \in D$ and $a \neq 0$.
    The elements $1,a,a^2,a^3,\dotsc$  are linearly dependent because $\dim_k D < \infty$.
    Therefore there exists $0 \neq p \in k[t]$ such that $p(a) = 0$.
    We have $p(t) = \prod_{i = 1}^n (t-a_i)$ for some $a_i \in k$, as $k$ is algebraically closed.
    Now $0 = p(a) = \prod_{i=1}^n (a - a_i)$, therefore $a = a_i$ for some $i$, which implies $a \in k$.
    This concludes $D = k$. \qedhere
  \end{enumerate}
\end{proof}
\begin{lemma}\label{lemv.4}
  Let $M$ be a semisimple $R$-module.
  Let $\varphi \colon \bigoplus_{i \in I} L_i \to M$ be an isomorphism of $R$-modules with $L_i$ irreducible.
  Then $\operatorname{Iso}_L (M) = \varphi (\bigoplus_{j \in J} L_i)$, where $J = \setdef{i \in I}{L_i \cong L}$.
\end{lemma}
\begin{proof}
  Since $\varphi$ is an $R$-module isomorphism, hence injective, we have $\varphi (\bigoplus_{i \in I} L_i) = \bigoplus_{i \in I} \varphi (L_i)$ and $\varphi (L_i) \cong L_i$ for all $i$ by Schur's Lemma.
  \underline{``$\supseteq$'':} We have  $$\varphi \left(\bigoplus_{j \in J} L_j \right) = \bigoplus_{j \in J} \varphi(L_j) = \sum_{j \in J} \varphi (L_j) \subseteq \operatorname{Iso}_L(M) \, .$$
  \underline{``$\subseteq$'':}  Assume that $\operatorname{Iso}_L(M) \subsetneq \varphi (\bigoplus_{j \in J} L_j)$.
  Then there exists $i_0 \in I$ such that $f \colon \operatorname{Iso}_L(M) \hookrightarrow M \xrightarrow[\operatorname{pr}_{i_0}] \varphi (L_{i_0})$, $i_0 \neq J$ and the map $f$ is nonzero.
  Then there exists a submodule $L \subseteq M$ such that $\res{f}{L} \neq 0$.
  Then $f$ would define a nonzero $R$-module homomorphism $L \to \varphi (L_{i_0}) \cong L_{i_0}$, which is a contradiction to Schur's Lemma.
\end{proof}
\begin{definition}
  We denote by $\operatorname{Irr}(R)$ the isomorphism classes of irreducible $R$-modules.
  We often fix a system of representatives for the classes and identify the set of representatives with $\operatorname{Irr} (R)$.
\end{definition}
\begin{remark}
  $\operatorname{Irr}(R)$ form a set:  Let $L$ be an irreducible $R$-module.
  Pick $0 \neq m \in L$.
  This generates $L$ as an $R$-module.
  We therefore get a surjective $R$-module homomorphism $\varphi \colon R \to L$, $1 \mapsto m$, $r \mapsto rm$.
  Hence $R / \ker \varphi \cong L$ and $\ker \varphi = \operatorname{Ann}_R (m)$ is a left ideal.
  Since $L$ is irreducible, $\ker \varphi$ is in fact maximal.
  But maximal left ideals form a set.
\end{remark}
\begin{example}
  Let $R = k$ be a field.
  If $V$ is an $R$-module (that is a $k$-vectorspace).
  Then $\langle v \rangle \subseteq V$ is a submodule of $V$ for all $v \in V$.
  Therefore, $\operatorname{Irr}(R) = k$.
\end{example}
\begin{lemma}\label{lemv.5}
  Let $M$ be a semisimple $R$-module.
  Then we have $M = \bigoplus_{L \in \operatorname{Irr}(R)} \operatorname{Iso}_L (M)$, the \textbf{isotypical decomposition of $M$}.
\end{lemma}
\begin{proof}
  Since $M$ is semisimple, there exists an isomorphism of $R$-modules $\varphi \colon \bigoplus_{i \in I} L_i \to M$, where $L_i$ are irreducible $R$-modules.
  Now we group summands which belong to the same isomorphism class in $\operatorname{Irr}(R)$ and we use \Cref{lemv.4}.
\end{proof}
\begin{example}
  If $R = k$ is a field, then $M = \bigoplus_{L \in \operatorname{Irr}(R)} \operatorname{Iso}_L (M) = \operatorname{Iso}_k (M)$.
  Therefore $M \cong \bigoplus_{i \in I} k$.
  The existence of such an isomorphism is the existence of a basis.
  Each such isomorphism corresponds to a choice of a basis.
\end{example}
\begin{theorem}[Hilbert's theorem]\label{thmV.6}
  Let $G$ be a group and $W$ a finite dimensional representation of $G$ over an infinite field $k$.
  Assume $\cP_k (W) \cong \bigoplus_{i \in I} L_i$ as representations of $G$ with $L_i$ irreducible.
  Then $\cP_k(W)^G$ is finitely generated as a $k$-algebra.
\end{theorem}
\begin{remark}
  The assumption says precisely that $\cP_k (W)$ is a semisimple $k[G]$-module.
\end{remark}
\begin{remark}~ \vspace{-\topsep}
  \begin{itemize}
  \item
    If $G$ is finite such that $\operatorname{char} k \nmid \abs{G}$, then $\cP_k (W) = \bigoplus_{d\geq 0} \cP_k (W)_d$ is a graded algebra with finite dimensional homogeneous components $\cP_k (W)_d$ which are finite dimensional representations of $G$.
    By Maschke, $\cP_k (W)_d$ is semisimple and so $\cP_k (W) = \bigoplus_{d\geq 0} \cP_k(W)_d$ is semisimple by \Cref{lemV.2}.  
  \item
    In the case $R = k[G]$ for some group $G$, the semisimplicity is often called \textbf{complete reducibility}.
  \end{itemize}
\end{remark}
\underline{Goal:}  Find examples where Hilbert's theorem holds.
Such examples are $\GL_n (k)$ and $\SL_n (k)$ for infinite fields $k$.

We now prepare the proof of Hilbert's theorem.
\begin{lemma}\label{lemV.6}
  Let $B = \bigoplus_{d\geq 0} B_d$ be a non-negatively graded $k$-algebra.
  Consider the (two-sided) ideal $B_+ = \bigoplus_{d >0} B_d$.
  If $B_+$ is a finitely generated ideal in $B$, then $B$ is finitely generated as a $B_0$-algebra.
  Moreover, one can find a finite generating set of homogeneous elements.
\end{lemma}
\begin{proof}
  Exercise (homework).
\end{proof}
\begin{lemma}\label{lemv.7}
  Let $A$ be a commutative $k$-algebra.
  Let a group $G$ act on $A$ by algebra-automorphisms.
  Assume $A = \sum_{i \in I} L_i$ as representations of $G$ with $L_i$ irreducible.
  Then
  \begin{enumerate}
  \item
    $A = A^G \bigoplus N$ as representations of $G$ where $A^G = \sum_{i \in J} L_i$, $N = \sum_{i \in I \setminus J} L_i$, $J = \setdef{i \in I}{L_i \cong \text{ trivial represenations}}$
  \item
    Define the \textbf{Reynolds Operator} $\pi \colon A \twoheadrightarrow A^G$ as the projection of $A$ onto $A^G$ along $N$, that is, for $a = a_1 + a_2 \in A = A^G \oplus N$ with $a_1 \in A^G$ and $a_2 \in N$ as above, we have $\pi(a) = a_1$.

    The Reynolds operator is an $A^G$-module morphism, that is for all $a \in A$ and $b \in A^G$ we have $\pi(ba) = b\pi(a)$. 
  \end{enumerate}
\end{lemma}
\begin{proof}~ \vspace{-\topsep}
  \begin{enumerate}
  \item
    This follows from the isotypical decomposition $$A^G = \operatorname{Iso}_{\mathrm{triv}}(A)$$ and $$N = \bigoplus_{L \in \operatorname{Irr}(k[G]), L \not\cong \mathrm{ triv}} \operatorname{Iso}_L (A)$$
  \item
    For $b \in A^G$, consider $m_b \colon A \to A$, $a \mapsto ba$.
    Claim:  $m_b$ is a morphism of representations.
    Indeed: $m_b(g.a) = b\cdot(g.a) = (g.b)\cdot(g.a) = g.(ba) = g.m_b(a)$ for all $a \in A$ and $g \in G$.
    By Schur's Lemma, the restriction of $m_b$ to any $L_i$ has image isomorphic to $L_i$ or zero.
    Therefore $m_b (A^G) \subseteq A^G$, $m_b (N) \subseteq N$.
    This implies $\pi (ba) = \pi (ba_1 + ba_2) = ba_1 = b \pi(a)$, which was to show. \qedhere
  \end{enumerate}
\end{proof}
\begin{proof}[Proof of \Cref{thmV.6}]
  Set $A \coloneqq \cP_k(W)$.
  We know that $A = \bigoplus_{d \geq 0} A_d$ (decomposition into homogeneous components).
  By \Cref{lemv.7}, we have $A^G \oplus N$ (with notation from there).
  If $I \subseteq A^G$ is an ideal, then
  \begin{equation}\label{eye}
    \pi(IA) = I\pi(A) = IA^G = I 
  \end{equation}

  We have by \Cref{lemV.6} that $A^G = \bigoplus_{d \geq 0} A_d^G$ and we can take $I \coloneqq A_+^G = \bigoplus_{d>0} A_d^G$.
  Then $\tilde I = IA$ is the ideal in $A$ generated by $I$.
  Since $A$ is noetherian (as $A$ is a polynomial ring in finitely many variables), we can find $f_1,\dotsc,f_m \in I$ which generate $\tilde I$.
  
  \underline{Claim:}  $f_1, \dotsc, f_m$ generate $I$ as an ideal in $A^G$.
  By \ref{eye}, any $x \in I$ is contained in $\pi (IA)$, hence $x = \pi (\sum_{i=1}^m f_ia_i)$ for some $a_i \in A$.
  Now $x = \sum_{i = 1}^m f_i \pi (a_i)$ by \Cref{lemv.7}, which shows the claim.

  Apply \Cref{lemV.6} to $B := A^G$ with $B_+ = \bigoplus_{d > 0} A_d^G = I$.
  Then $B = A^G$ is finitely generated as a $B_0$ algebra.
  But $B_0 = A^G_0 = A_0 = k1 = k$.
  Hence $A^G$ is a finitely generated $k$-algebra.
\end{proof}
\subsection{Semisimple Rings and Algebras}
\begin{definition}[semisimple]
  A ring $R$ is called \textbf{semisimple} if it is semisimple on the (left) module over itself (via left multiplication).
  In this case, $R = \bigoplus_{L \in \operatorname{Irr}(R)} \operatorname{Iso}_L (R)$.

  An algebra $A$ is \textbf{semisimple} if it is semisimple as a ring.

  An ring $R$ is \textbf{simple} if $R \neq 0$ and $R = \operatorname{Iso}_L(R)$ for some $R$-module $L$.

  An algebra is \textbf{simple} if it is simple as a ring.
\end{definition}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

\begin{lemma}\label{lemvii.2}
  There is a bijection
  \begin{align*}
    \set{\text{mor. of aff. alg. var. } f \colon (X,k[X]) \to (Y,k[Y])} 
    &\longleftrightarrow
      \set{\text{alg. hom. } k[Y] \to k[X]}  \\
    (f,f^\natural) &\longmapsto f^\ast = f^\natural \\
    (\varphi_g\colon X\to Y,\varphi^\ast_g) &\longmapsfrom g \; ,
  \end{align*}
  where $\varphi_g(x) \in Y$ such that
  \begin{equation*}
    \begin{tikzcd}
      k[Y] \arrow[r,"g"] \arrow[dr,swap,dashed,"\operatorname{ev}_{f\varphi_g(x)}"] & k[X] \arrow[d,"\operatorname{ev}_x"] \\ &k \; .
    \end{tikzcd}
  \end{equation*}
  The bijection is compatible with compositions and $\id_{(X,k[X])} \leftrightarrow \id_{k[X]}$.

  \underline{Notation:}
  Denote by $\Hom_{\mathrm{Var}}(X,Y)$ the set of all morphisms of affine algebraic varieties from $X$ to $Y$.
\end{lemma}
\begin{remark}
  Behind this lemma is an equivalence of categories:
  \begin{align*}
    \set{\text{aff. alg. var. over }k \text{ with mor.}} & \longrightarrow \set{\text{f.g., com. $k$-alg. without nilpotents}}  \\
    (X,k[X]) &\longmapsto k[X]  \\
    (f,f^\natural) & \longmapsto f^\ast = f^\natural \; ,
  \end{align*}
  identifying $\Hom_{\mathrm{Var}}(X,Y)$ with $\Hom_{\mathrm{alg}}(k[Y],k[X])$.
\end{remark}
\begin{proof}[Proof (of \cref{lemvii.2})]
  To show: maps are inverses.
  \begin{itemize}
  \item
    $(f,f^\natural) \mapsto f^\natural \mapsto \varphi_{f^\natural}$.
    Let $h \in k[Y]$, $x \in X$.
    Then
    \begin{align*}
      &&\ev_{\varphi_{f^\natural(x)}} (h) &= \ev_x \circ f^\natural (h)  \\
      &&                                 &= \ev_x \circ f^\ast(h)
      &&                                &= \ev_x(h\circ f) = h\circ f(x) = \ev_{f(x)} (h) \\
      \implies&& \ev_{\varphi_{f^\natural(x)}} = \ev_{f(x)}  \\
      \implies && \varphi_{f^\natural(x)} = f(x)  \\
      \implies && f^\natural = f
    \end{align*}
  \item
    $g \mapsto \varphi_g \mapsto \varphi_g^\ast$.
    Let $h \in k[Y]$, $x \in X$.
    Then
    \begin{align*}
      &&\varphi^\ast_g(h)(x) &= h \circ \varphi_g (x) = \ev_{\varphi_g(x)}(h)  \\
      && &= \ev_x(g(h)) = g(h)(x)  \\
      \implies && \forall h \in k[Y]\colon \varphi^\ast_g (h) = g(h)  \\
      \implies && \varphi^\ast_g = g \; .
    \end{align*}
    In particular also $\varphi^\ast_g k[Y] \to k[X]$.
    Therefore $\varphi^\ast_g = \varphi^\natural_g$, the inverse map is well-defined.
  \item
    Compatibility: $(f\circ g)^\ast = g^\ast \circ f^\ast$, $(\id_{(X,k[X])})^\ast = \id_{k[X]}$. \qedhere
  \end{itemize}
\end{proof}
\begin{theorem}\label{thmvii.3}
  Every affine algebraic variety is isomorphic to some $(V(M), k[V(M)])$, where $M \subseteq \pol{k}{T}{1}{n}$ is a subset.
\end{theorem}
\begin{proof}
  Let $(X,k[X])$ be an affine algebraic variety.
  Then $k[X]$ is a finitely generated commutative $k$-algebra.
  Let $a_1,\dotsc,a_n$ be generators.
  Then there exists by the universal property a unique algebra homomorphism
  \begin{align*}
    \pi \colon \pol{k}{T}{1}{n} & \longrightarrow k[X]  \\
    T_i &\longmapsto a_i \; .
  \end{align*}
  This is surjective.
  We define $f \colon X \to k^n$, $x \mapsto (\pi(T_1)(x),\dotsc,\pi(T_n)(x))$.

  \underline{Claim 1: $f^\ast = \pi$ (with $k[k^n] = \pol{k}{T}{1}{n}$).}
  We have $f^\ast(T_i)(x) = T_i (f(x)) = \pi(T_i)(x)$ for all $x \in X$ and $1 \leq i \leq n$.
  Therefore $f^\ast(T_i) = \pi(T_i)$ for all $1 \leq i \leq n$, which in turn implies $f^\ast = \pi$, as both are algebra homomorphisms.

  Set $M := \ker \pi$.

  \underline{Claim 2: $\im f \subseteq V(M)$.}
  Let $\varphi \in M$, $x \in X$.
  Then $$\varphi(f(x)) = \varphi(f(x)) = f^\ast(\varphi)(x) = \pi (\varphi)(x) = 0 \,.$$

  Note that $\sqrt{\ker \pi} = \ker \pi$ since $$p^r \in \ker \pi \iff \pi (p^r) = 0 \iff \pi(p)^r = 0 \iff \pi(p) = 0 \iff p \in \ker \pi \, .$$
  By definition of $k[V(M)]$ a surjective algebra homomorphism $\pol{k}{T}{1}{n} \to k[V(M)]$, $f \mapsto \res{f}{V(M)}$, and the kernel is $$\ker \pi = \sqrt{ker \pi} = \sqrt{M} = I(V(M)) := \setdef{f \in \pol{k}{T}{1}{n}}{\forall x \in V(M)\colon f(x) = 0} \,.$$
Hence $k[V(M)] = \pol{k}{T}{1}{n}/M = \pol{k}{T}{1}{n}/\ker \pi \cong k[X]$.
Therefore $(f,f^\ast)$ defines an isomorphism $(X,k[X]) \to (V(M),k[V(M)])$, because $f^\ast = f^\natural$ is an isomorphism (cref{lemvii.2}).
\end{proof}
Consequence: Via this identification for any $(X,k[X])$ an affine algebraic variety, $X$ is a topological space (via the Zariski topology).
\begin{remark}
  One can show that this is independent (up to isomorphism) from the chosen realization.
\end{remark}
\begin{lemma}\label{lemvii.4}
  Every morphism of algebraic varieties is continuous.
\end{lemma}
\begin{proof}
  Let $f \colon (X,k[X]) \to (Y,k[Y])$, or rather $f \colon (V(M),k[V(M)]) \to (V(M^\ast),k[V(M^\ast)])$ be a morphism.
  To show: preimages of closed subsets are closed.

  Let $Z \subseteq Y$ be closed.
  Then $Z = V(N) \cap Y$ for some $N$ in the filling polynomial ring.
  Then \begin{align*}f^{-1} (Z) &= \setdef{x \in X}{f(x) \in V(N)} \\ &= \setdef{x \in X}{\forall \varphi \in N\colon \varphi(f(x)) = 0}\\ &= \setdef{x \in X}{\forall \varphi \in N \colon f^\ast(\varphi)(x) = 0} \\ &= \setdef{x \in X}{x \in V(f^\ast(N))}\\ & = V(f^\ast(N))\cap X \subseteq X
\end{align*}closed.
\end{proof}
\newpage
\section{Products and Hopf Algebras}

Goal:

\begin{tikzpicture}
  \draw (0,0) ellipse (3 and 4);
  \draw (0,3) node {affine alg.};
  \draw (0,2.5) node {varieties};
  \draw (0,-1) ellipse (2 and 2.5);
  \draw (0,0) node {linear alg.};
  \draw (0,-0.5) node {groups};

  \draw (10,0) ellipse (3 and 4);
  \draw (10,3) node {affine alg.};
  \draw (10,2.5) node {varieties};
  \draw (10,-1) ellipse (2 and 2.5);
  \draw (10,0) node {affine alg.};
  \draw (10,-0.5) node {groups};

  \draw[<->] (1,-0.25) -- (9,-0.25);
  \draw (5,0.25) (5,0.25) node {relation?};

  \draw[->] (6,-3) -- (10,-1);
  \draw (4.5,-2.5) node {We need};
  \draw (4.5,-3) node {products to};
  \draw (4.5,-3.5) node {define these};
\end{tikzpicture}

\begin{definition}[(co)product]
  Let $(X_i,k[X_i])$, $i \in \set{1,2}$, be affine algebraic varieties.
  Then define
  \begin{equation*}
    (X_1 \dot\cup X_2,k[X_1 \dot\cup X_2]) := \setdef{f\colon X_1 \dot\cup X_2 \to k}{\res{f}{X_i} \in k[X_i]} \; .
  \end{equation*}
  This is called the \textbf{coproduct} of $(X_1,k[X_2])$ and $(X_2,k[X_2])$.

  Define $(X_1 \times X_2,k[X_1 \times X_2])$ as the subalgebra of $\operatorname{Maps}(X_1\times X_2,k)$ generated by $\im \res{p_i^\ast}{k[X_i]}$, $i=1,2$, where $p_i$ are the canonical projections.
  This is called the \textbf{product} of $(X_1,k[X_2])$ and $(X_2,k[X_2])$.
\end{definition}
\begin{proposition}[Universal property]\label{propviii,1}
  Let $(X_i,k[X_i])$, $i \in \set{1,2}$, be affine algebraic varieties.
  Their (co)product are affine algebraic varieties satisfying for any affine algebraic variety $(Z,k[Z])$:
  \begin{enumerate}
  \item
    \begin{equation*}
      \begin{tikzcd}
        X \arrow[r,hook,"\operatorname{incl}"] \arrow[dr,"f_1"'] & X_1 \dot\cup X_2 \arrow[d,dashed,"\exists ! h"'] & X_2 \arrow[l,hook',"\operatorname{incl}"'] \arrow[dl,"f_2"] \\
        &Z&
      \end{tikzcd}
    \end{equation*}    
  \item
    \begin{equation*}
      \begin{tikzcd}
        X_1  & X_1 \times X_2 \arrow[l,twoheadrightarrow,"\operatorname{pr}_1"'] \arrow[r,twoheadrightarrow,"\operatorname{pr}_2"]  & X_2 \\
        &Z\arrow[u,dashed,"\exists ! h"] \arrow[ul, "f_1"'] \arrow[ur,"f_2"]&
      \end{tikzcd}
    \end{equation*}
  \end{enumerate}
\end{proposition}
\begin{proof}
  \begin{enumerate}
  \item Translate into algebra homomorphisms.
    \begin{equation*}
      \begin{tikzcd}
        k[X_1]  & k[X_1 \dot\cup X_2] \arrow[l,"\operatorname{incl}^\ast"'] \arrow[r,"\operatorname{incl}^\ast"]  & k[X_2] \\
        &Z\arrow[u,dashed,"\exists ! h^\prime"] \arrow[ul,"f_1^\ast"] \arrow[ur,"f_2^\ast"']&
      \end{tikzcd}
    \end{equation*}
    Note that $\incl^\ast$ is just the restriction to $X_i \subseteq X_1 \dot\cup X_2$.
    Define $h^\ast(\varphi)(w) := f^\ast_i(w)$ for $w \in X_i$.
    Then clearly $(\incl^\ast \circ h^\ast)(\varphi)(w) = f^\ast_i(w)$, so the diagram commutes.
    Obviously $h^\prime$ is unique.
  \item
    Exercise:  We have an isomorphism \begin{align*}\beta \colon k[X_1]\otimes k[X_2] & \longrightarrow k[X_1 \times X_2] \\ f\otimes g & \longmapsto \beta(f\otimes g) =( (x_1,x_2) \mapsto f(x_1)f(x_2)) \, . \end{align*}
    Then the universal property of the tensor product gibes a unique $h := f_1^\ast \otimes f_2^\ast \circ \beta^{-1}$ such that the diagram commutes
  \end{enumerate}
  We still have to check that the coproduct is an affine algebraic variety.

  Let $\mathbbm{1}_{X_i} \in \Maps(X_1 \dot\cup X_2,k)$.
  \begin{equation*}
    \mathbbm{1}_{X_i}(w) = \begin{cases}1 & \text{if } w \in X_i \\ 0 & \text{otherwise} \end{cases}
  \end{equation*}
  We have $\res{\mathbbm{1}_{X_i}}{X_i} = \text{unit }1 \in k[X_i]$ and for $i\neq j$ we have $\res{\mathbbm{1}_{X_i}}{X_j} = \text{zero map} \in k[X_i]$.
  Therefore $\mathbbm{1}_{X_i} \in k[X_1 \dot\cup X_2]$.
  Now $1 = \mathbbm{1}_{X_1} + \mathbbm{1}_{X_2} \in k[X_1 \dot\cup X_2]$ (because we are closed under addition) with $1$ as the unit in $\Maps(X_1 \dot\cup X_2,k)$.
  Therefore $k[X_1 \dot\cup X_2]$ has $1$.

  Now let $h\in k[X_i]$.
  Then $\tilde h \in \Maps(X_1 \dot\cup X_2,k)$, where $\tilde h (w) = h(w)$ for $w \in X_i$ for $i=1$ or $i=2$ and $h(w) = 0$ otherwise.
  By definition of $k[X_1 \dot\cup X_2]$ we have $\tilde h \in k[X_1 \dot\cup X_2]$.
  Moreover any $\varphi \in k[X_1 \dot\cup X_2]$ can be written as $\varphi = \varphi 1 = \varphi \mathbbm{1}_{X_1} + \varphi \mathbbm{1}_{X_2}$ with $\varphi \mathbbm{1}_{X_i} = \tilde h_i$ for some $h_i \in k[X_i]$.
  In particular $k[X_1 \dot\cup X_2]$ is finitely generated as algebras, since the $k[X_i]$ are finitely generated as algebras.
  Still to show:
  \begin{align*}
    X_1 \dot\cup X_2 &\leftrightarrow \Hom_{\mathrm{alg}}(k[X_1 \dot\cup X_2],k)  \\
    w &\mapsto \ev_w \; .
  \end{align*}

  \underline{Claim: $f \in \Hom_{\mathrm{alg}}(k[X_1 \dot\cup X_2], k) \implies \forall \varphi \in k[X_1 \dot\cup X_2] \colon f(\varphi \mathbbm{1}_{X_i}) = 0$.}
  We have $$f(\mathbbm{1}_{X_i}) = f(\mathbbm{1}_{X_i}^2) = (f(\mathbbm{1}_{X_i}))^2 \, .$$
  Therefore $f(\mathbbm{1}_{X_i}) \in \set{0,1}$.
  Since $1 = f(1) = f(\mathbbm{1}_{X_1} + \mathbbm{1}_{X_2}$, we have $f(X_i) = 0$ for one $i$.

  \underline{$w \mapsto \ev_w$ is injective:}
  Let $\ev_w = \ev_{w^\prime}$.
  \begin{align*}
    \implies & \ev_w(\mathbbm{1}_{X_i}) = \ev_{w^\prime}(\mathbbm{1}_{X_i}) \text{ for }i=1,2 \\
    \implies & w,w^\prime \in X_i \text{ for some } i \in \set{1,2}  \\
    \implies & \ev_w (\tilde h) = \ev_{w^\prime}(\tilde h) \text{ for all } h \in k[X_i]  \\
    \implies & h(w) = h(w^\prime) \text{ for all } h \in k[X_i]  \\
    \implies & w = w^\ast
  \end{align*}

  \underline{$w \mapsto \ev_w$ is surjective:}
  Let $f \in \Hom_{\mathrm{alg}}(k[X_1 \dot\cup X_2],k)$.
  Assume $f(\varphi \mathbbm{1}_{X_1}) = 0$ (see above).
  \begin{align*}
    \implies & \exists x_2 \in X_2 \colon f(\varphi\mathbbm{1}_{X_2}) = \ev_{x_2}(\varphi\mathbbm{1}_{X_2}) \\
             & = \ev_{x_2}(\varphi\mathbbm{1}_{X_1} + \mathbbm{1}_{X_2})  = \ev_{x_2}(\varphi 1) = \ev_{x_2}(\varphi)  \\
    \implies & f(\varphi) = \ev_{x_2}(\varphi)  \\
    \implies & f = \ev_{x_2}
  \end{align*}
\end{proof}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

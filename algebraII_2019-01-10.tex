\begin{proof}[Proof (cont.).]
	We obtain 
	\begin{align*}
		\beta^\ast \colon k[\GL_n(k)] = k[X_{11}, \dots , X_{nn}][\operatorname{det}^{-1}] & \longrightarrow \Maps(G,k) \\
		X_{ij} & \longmapsto (g \mapsto a_{ij}(g)) \in k[G] \\
		\det & \longmapsto p(a_{11},\dots , a_{nn})
	\end{align*}
	where $p$ is the polynomial expression  determinant in the matrix entries. In particular, $p(a_{11},\dots , a_{nn}) \in k[G]$ since $a_{ij} \in k[G]$ and $k[G]$ algebra $\det^{-1} \mapsto \beta^\ast(\det) \circ \operatorname{inv} \in k[G] $. Therefore, $\im \beta^\ast \subset k[G]$ and $\beta$ is a morphism of affine algebraic varieties. 

	\textbf{Claim 1}: $\beta$ is injective. We have $$f_i(g)= f_i(eg)=\rho_g(f_i)(e) =\sum_{j=1}^n a_{ji}(g)f_i(e)$$ for all $g \in G$. This implies $f_i = \sum_{j=1}^n f_i(e)a_{ji} \in k[G]$ for all $i$. Therefore, the $a_{ij}$ generate $k[G]$ as an algebra because the $f_i$'s do. Finally, $\beta^\ast$ is surjective implying that $\beta$ is injective. 
	
	\textbf{Claim 2}: $\im \beta \eqqcolon H \subset \GL_n(k)$ is closed. We are going to prove this in the next section.
	
	This implies that $H$ is a closed subgroup of $\GL_n(k)$ and hence linear. 
	
	\textbf{Claim 3} $\beta \colon G \to H$ is an isomorphism of affine algebraic varieties. Consider $\res{\beta^\ast}{k[H]} \to k[G]$ which is still surjective (easy exercise). On the other hand if $h_1,h_2 \in k[H]$ and $\beta^\ast(h_1)=\beta^\ast(h_2)$. Then, $h_1 \circ \beta = h_2 \circ \beta$ implying $h_1(h) = h_2(h)$ for all $h$ and therefore $h_1=h_2$ as elements in $k[H]$..
	
	All in all $\beta^\ast$ is an isomorphism which implies the claim. 
\end{proof}
\newpage
\section{Affine Algebraic Varieties/Groups as Topological Spaces}
We want to study $X$ as a topological space (with Zariski topology) for $(X,k[X])$ an affine algebraic variety.
\begin{definition}[noetherian, irreducible, connected]
	A topological space $X$ is called 
	\begin{itemize}
		\item \textbf{noetherian} if open sets satisfy the ascending chain condition, i.e. for $U_1 \subset U_2 \subset U_3 \subset \dots$ with $U_i \subset X$ open then there exists $i_0 \in \N$ such that $U_i = U_{i_0}$ for all $i \ge i_0$.
		\item \textbf{irreducible} if $X=X_1 \cup X_2$ where $\emptyset \neq X_i \subset X$ closed implies $X=X_1$ or $X=X_2$.
		\item \textbf{connected} if $X=X_1\dot \cup X_2$ (i.e. $X_1 \cap X_2 = \emptyset$) where $\emptyset \neq X_i \subset X$ closed implies $X_1=X$ or $X_2=X$.
	\end{itemize}
	 A subspace $Y \subset X$ is \textbf{irreducible} if $Y=(X_1 \cap Y) \cup (X_2 \cap Y)$ (both not $\emptyset$) where $X_1 \subset X$ closed implies $X_1 \cap Y = Y$ or $X_2  \cap Y = Y$.
\end{definition}
\begin{notation}
	Let $X$ be a topological space, $U \subset X$. Write $U \SubsetO$ X if $U \subset X$ is open and $U \Subset X$ if $U$ is closed.
\end{notation}
\begin{remark}~ \vspace{-\topsep}
	\begin{itemize}
		\item irreducible $\implies$ connected
		\item $X$ irreducible $\iff$ any nonempty open subset is dense (i.e. $\emptyset \neq U \subset X$ open, then $\overline U = X$) $\iff$ $U_1,U_2 \subset X$ open, both nonempty $\implies$ $U_1 \cap U_2 \neq \emptyset$.
		
		\textit{Proof.} First equivalence. 
		
		``$\implies$'': Let $U \SubsetO X$. Write $X=\overline U \cup X \setminus U$ with $\overline \neq \emptyset$. Since $X$ is irreducible we have $\overline U = X$ or $X \setminus U = \emptyset$
		
		``$\impliedby$'': Let $X=X_1 \cup X_2$ with $X_i \neq \emptyset$ and $X_i \Subset X$. Assume $X_i \neq X$. Then $X_2 \supset X \setminus X_1$ which is open. Hence $X_2 = \overline X_2 \supset \overline{X \setminus X_1}=X$ which shows $X_2=X$. 
		
		\textit{Proof.} Second equivalence. 
		
		``$\implies$'': Assume that $X$ is irreducible. If $U_i=X$ for some $i$ then we are done. Assume $U_i \neq X$ ($i=1,2$). Since $X$ is irreducible $X \neq (X \setminus U_1) \cup (X \setminus U_2)$ impying that there exists $x \in X$ such that $x \notin X \setminus U_1$ and $x \notin X \setminus U_2$. Therefore, $x \in U_1 \cap U_2$. 
		
		``$\impliedby$'': Assume that $X=X_1 \cup X_2$, $X_i \neq \emptyset$ with $X_i \Subset X$ (i.e. $X \setminus X_i \eqqcolon U_i \SubsetO X$, $U_i \neq \emptyset$). By assumption $U_1 \cap U_2 \neq \emptyset$. This implies that there exists $x \in U_1 \cap U_2$, so $x \notin X \setminus U_1$  for $i=1,2$ implying that $x \notin X_1 \cup X_2$ which is a contradiction. Therefore, $X_1=X$ or $X_2=X$.
	\end{itemize}
\end{remark}
\begin{lemma} \label{lemX.1}
	Let $(X,k[X])$ be an affine algebraic variety. Then $X$ s noetherian.
\end{lemma}
\begin{proof}
	Exercise.
\end{proof}
\begin{lemma} \label{lemX.2}
	Let $X,X^\prime$ be topological spaces. 
	\begin{enumerate}[label=(\alph*)]
		\item $Y \subset X$ irreducible $\implies$ $\overline Y$ irreducible
		\item $\varphi \colon X \to X^\prime$ continuous, $X$ irreducible $\implies$ $\varphi(X)$ irreducible
		\item $X,X^\prime$ irreducible $\implies$ $X \times X^\prime$ irreducible
	\end{enumerate}
\end{lemma}
\begin{proof}
	Exercise.
\end{proof}
\begin{proposition} \label{propX.3}
	Let $X$ be a noetherian topological space. 
	\begin{enumerate}[label=(\alph*)]
		\item There exists $r \in \N$ and irreducible $X_i \Subset X$ ($1 \le i \le r$) such that $X = X_1 \cup \dots \cup X_r$
		\item If one assumes moreover that $X_i \not \subset X_j$ for $i \neq j$, then the decomposition is unique up to permutation of the $X_i$'s.
		
		In this case the $X_i$'s are called irreducible components  of $X$ and are the maximal irreducible subsets (with respect to inclusion)
	\end{enumerate}
\end{proposition}
\begin{proof}~ \vspace{-\topsep}
	\begin{enumerate}[label=(\alph*)]
		\item If $X$ is irreducible then set $r=1$, $X_1=X$. Otherwise write $X=Y_1 \cup Y_2$ with $\emptyset \neq Y_i \Subset X$ with $Y_i \neq X$ (for $i=1,2$). If the first statement holds for $Y_1$ and $Y_2$, then we are done. 
		Otherwise wlog the first statement doesn't hold for $Y_1$. In particular $Y_1$ is \textbf{not} irreducible. We can write $Y_1 = Y_{2} \cup Y_{2^\prime}$ with $\emptyset \neq Y_{i} \subset Y_1$ and $Y_{i} \neq Y_1$ (for $i=2,2^\prime$). 
		If the first statement hold for $Y_{2}$ and $Y_{2^\prime}$ we are done. 
		So wlog assume that the first statement does not hold for $Y_{2}$. 
		
		Repeat this argument with $Y_{2} = Y_{3} \cup Y_{3^\prime}$. Repeating this argument we get $Y_1 \supsetneq Y_{2} \supsetneq Y_{3} \supsetneq Y_{4}$ a chain of closed subset which doesn't stabilize. 
		
		This is a contradiction to $X$ be noetherian. 
		\item Assume that $X=X_1 \cup \dots \cup X_r$ and $X=X^\prime_1 \cup X_2^\prime \cup \dots \cup X_{r^\prime}^\prime$ be a decomposition as given in the statement. 
		
		For fixed $i$ ($1 \le i \le r$) we have $X_i = (X_1^\prime \cap X_i) \cup \dots \cup (X_{r^\prime} \cap X_i)$ with $(X_j^\prime \cap X_i) \Subset X$ for all $1 \le j \le r^\prime$.
		
		Since $X_i$ is irreducible we have $X_i=X_j^{\prime} \cap X_i$ form some $1 \le j^\prime \le r^\prime$. Then $X_i \subset X_j^\prime$ for some $1 \le j \le j^\prime$.
		Similarly, $X_j^\prime \subset X_k$ for some $1 \le k \le r$. 
		This implies $X_i \subset X_k$ which leads to $i=k$ and $X_i=X_j^\prime$ for some unique $j$.
		
		Similarly, $X_j^\prime = X_k$ for some unique $k$.
		All in all $r=r^\prime$ and there exists $\sigma \in S_r$ such that $X^\prime_{\sigma(i)}=X_i$.
		
		We still need to show that the $X_i$'s are maximal irreducible. Assume that $Y \Subset X$ is irreducible. We have $Y=\bigcup_{i=1}^r (Y \cap X_i)$ which implies $Y = Y \cap X_i$ for some $i$ and hence $Y \subset X_i$ for some $i$. \qedhere
	\end{enumerate}
\end{proof}
\begin{lemma} \label{lemX.4}
	Let $(G,k[G])$ be an affine algebraic group. Then there exists a unique irreducible component $G_0$ containing $e \in G$. It is called the identity component. 
\end{lemma}
\begin{proof}
	By \cref{lemX.1}, $G$ is noetherian. Therefore, there exist finitely many irreducible components. Let $X_1, \dots , X_m$ be those containing $e$. Consider
	\begin{align*}
		\varphi \colon \qquad X_1 \times X_2 \times \dotsb \times X_m & \longrightarrow X_1X_2\dotsb X_m \subset G \\
		(z_1, z_2, \dots , z_m) & \longmapsto z_1z_2 \cdots z_m & (\text{multiplication in }G)
	\end{align*}
	This is a morphism of affine algebraic varieties (check!). 
	By \Cref{lemX.2} we have $\varphi(X_1 \times \dots \times X_m)=X_1X_2 \cdots X_m \subset G$ is irreducible and obviously contains $e$. By \Cref{lemX.2} and \Cref{propX.3} we have $X_1X_2 \cdots X_m \subset \overline{X_1X_2 \cdots X_m} \subset X_i$ for some $1 \le i \le m$ (because contained in an irreducible component and contains $e$). Obviously $X_i \subset X_1X_2 \cdots X_m \subset X_i$ (since $e \in X_j$ for all $j$). All in all, $X_i = X_1X_2 \cdots X_m$ and $X_j \subset X_i$ for all $1 \le j \le m$. This is a contradiction unless $m=1$.
\end{proof}
\begin{definition}[connected]
	An affine algebraic group $G$ is called \textbf{connected} if $G_0=G$.
\end{definition}
\begin{proposition} \label{propX.5}
	Let $(G,k[G])$ be an affine algebraic group 
	\begin{enumerate}[label=(\alph*)]
		\item $G_0 < G$ is a closed normal subgroup 
		\item $G_0$ has finite index in $G$, i.e. $(G:G_0) < \infty$
		\item The cosets $gG_0$ for $g \in G$ are the connected and irreducible components of $G$.
		\item Each closed subgroup of finite index contains $G_0$.
	\end{enumerate}
\end{proposition}
\begin{proof}~
	\begin{enumerate}[label=(\alph*)]
		\item $G_0$ subgroup: Let $g \in G_0$. Then $gG_0, g^{-1}G_0$ are irreducible (because $G_0$) is irreducible and mult. with $g$. But $g^{-1}G_0$ contains $e$. Therefore $g^{-1}G_0 \subset G_0$ and therefore $g^{-1} \in G_0$. If $g,h \in G_0$, then $gh \in gG_0 \subset G_0$.
		
		By definition, $G_0$ is closed. 
		
		For $g \in G$ we have that $gG_0g^{-1}$ is an irreducible component contains $e$ and closed. Hence, $gG_0g^{-1} \subset G_0$. All in all, $G_0$ is normal. 
		\item By definition $G_0$ is closed, irreducible and maximal with these properties. Therefore, $gG_0$ is again, closed irreducible since multiplication with $g$ is continuous with continuous inverse. It's maximal with these properties because multiplication with $g$ and $g^{-1}$ preserves inclusions. All in all, $gG_0$ is irreducible component for all $g \in G$.
		Since $G=\bigcup_{g \in I}gG_0$ where $I$ is a system of representatives for cosets $G/G_0$ this is a decomposition of $G$ into irreducible components. By \Cref{propX.3} there are only finitely many irreducible components. Thus, $|I|<\infty$ implying $(G:G_0)<\infty$
		\item $G_0$ is irreducible for all $g \in I$, in particular connected and of course closed. Assume for $g \in I$ that $gG_0 \subsetneqq Y$ with $Y$ closed connected. This implies \[Z \coloneqq Y \cap \bigcup_{\substack{g^\prime \in I \\ g^\prime \neq g}} g^\prime \neq \emptyset\,.\] Since $I$ is finite, $Z$ is closed.
		Therefore, \[Y = Z \dot\cup (gG_0 \cap Y)\] which contradicts the fact that $Y$ is connected. 
		
		Hence, $gG_0 =Y$ and $gG_0$ is connected component. 
		
		If $Z$ is a connected component, then $Z \cap gG_0 \neq \emptyset$ for some $g \in I$. Since $Z$ is connected, we get $Z \subset gG_0$. Since $Z$ is component,we get $Z=gG_0$. 
		Hence $\setdef{gG_0}{g \in I}$ are the connected components. 
		\item Let $H < G$ be closed with $(G:H)<\infty$. Define $H^\prime = H \cap G_0$. Then 
		\begin{align*}
			G_0/H^\prime & \longrightarrow G/H \\
			gH^\prime & \longmapsto gH
		\end{align*} is an injective map. Hence, \[|G_0/H^\prime|\le |G/H|<\infty\] implying that $H^\prime \SubsetO H$ (because it is the components of finitely many cosets for $H^\prime$ and $H^\prime \Subset G_0$). We know that $G_0$ is irreducible. Therefore, $H^\prime = \overline{H^\prime}=G_0$ implying $G_0 \subset H$. \qedhere
	\end{enumerate}
\end{proof}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

\begin{definition}
  Let $(C,\Delta,\varepsilon)$ be a coalgebra, and let $\Delta_M \colon M\to M \otimes C$ be a (right) comodule.
  A \textbf{cosubmodule} of $M$ is a vector subspace $N \subseteq M$ such that $\res{\Delta_M}{N} \colon N \to N \otimes C$.
\end{definition}
\begin{proposition}\label{propxiv.4}
  Every (right) comodule over a coalgebra $C$ is the union of finite dimensional cosubmodules.
\end{proposition}
\begin{proof}
  Let $0 \neq m \in M$.
  Consider $\Delta_M \colon M \to M \otimes C$ applied to $m$.
  We have $\Delta_M(m) = \sum_{i\in I} m_i \otimes c_i$ for a basis $\set{c_i}_{i \in I}$ of $C$ and some $m_i$ (with finite support).
  We define $N := N_m := \spann\setdef{m_i}{i \in I}$, which is finite dimensional.

  \textbf{Claim 1: $m \in N$.}
  We have $m = \sum_{i \in I} m_i \otimes \varepsilon (c_i) \in N$.
  \begin{equation*}
    \begin{tikzcd}
      M \arrow[dr,"\id"'] \arrow[r,"\Delta_M"] & M \otimes C \arrow[d,"\id \otimes \varepsilon"] \\
      & M = M \otimes k
    \end{tikzcd}
  \end{equation*}

  \textbf{Claim 2: $N$ is a comodule.}
  Let $m_i \neq 0$.
  Then
  \begin{align*}
    N \otimes C \otimes C \ni & \sum_{i \in I} m_i \otimes \Delta_C (c_i) \\
    =& \id \otimes \Delta_C \circ \Delta_M (m) \stackrel{\text{(C2)}}{=} (\Delta_M \otimes \id) \circ \Delta_M (m)\\
    =& \sum_{i \in I} \Delta_M (m_i) \otimes c_i \; ,
  \end{align*}
and as $\set{c_i}_{i \in I}$ is a basis, we must have $\Delta_M (m_i) \in N \otimes C$ for all $i \in I$.
\end{proof}
\begin{remark}
  One can show that for an affine algebraic group $(G,k[G])$, there is an equivalence of categories:
  \begin{align*}
    \set{ \begin{matrix}\text{rational rep. of } $G$ \\ \text{with morphisms of rat. rep}\end{matrix}} &\xrightarrow{\sim} \set{\begin{matrix} \text{ right comodules for }k[G] \\ \text{with morphisms of comodules} \end{matrix}} \\
    \alpha \colon G \times V \to V &\mapsto \Delta \colon V \to V \otimes k[G], \, v_j \mapsto \sum_i v_i \otimes a_{i,j} \; ,
  \end{align*}
  where $\set{v_i}_i$ is a basis of $V$ and $a_{i,j} \in k[G]$ are given by $g.v_j = \sum_{i \in I} a_{i,j}(g)v_i$ for all $g \in G$.
\end{remark}
\begin{definition}
  A morphism $f \colon V \to W$ of rational representations of $(G,k[G])$ is a linear map of vector spaces which is $G$-equivariant, that is $\forall g \in G \, \forall v \in V \colon f(g.v) = g.(f(v))$.
\end{definition}
\begin{definition}
  Let $(C,\Delta_C,\varepsilon)$ be a coalgebra, and let $(M,\Delta_M)$ and $(N,\Delta_N)$ be (right) comodules.
  Then a morphism of comodules from $(M,\Delta_M) \to (N,\Delta_N)$ is a linear map $f \colon M \to N$ such that
  \begin{equation*}
    \begin{tikzcd}
      M \arrow[d,"f"'] \arrow[r,"\Delta_M"] & M \otimes C \arrow[d,"f\otimes \id"]\\
      N \arrow[r,"\Delta_N"] & N \otimes C
    \end{tikzcd}
  \end{equation*}
  commutes.
\end{definition}
\section{Linearly reductive groups}
\begin{theorem}
  For an affine algebraic group $(G,k[G])$, the following are equivalent:
  \begin{enumerate}
  \item Every finite dimensional rational representation is semisimple    
  \item Every rational representation on semisimple
  \item $k[G]$ is a semisimple rational representation with respect to the left translation
  \end{enumerate}
  If $G \subseteq \GL(V)$ is a linear algebraic group with $V$ finite dimensional:
  \begin{enumerate}
    \setcounter{enumi}{3}
  \item The tensor powers $V^{\otimes d}$, $d \in \mathbb{N}_0$ are rational representations of $G$.
  \end{enumerate}
\end{theorem}
\begin{proof}
  \textbf{``(1) $\implies$ (2)'':}
  Let $V$ be a rational representation.
  Then $V = \bigcup_{i \in I} V_i$, where the $V_i$ are finite dimensional rational representations of $G$.
  By assumption, all $V_i$ are semisimple, hence $V_i$ is a sum of irreducible representations, hence $V$ is also a sum of irreducible representations, hence $V$ is semisimple.

  \textbf{``(2) $\implies$ (3)'':}
  Since $k[G]$ is a rational representation with left translation, it is semisimple by assumption.

  \textbf{``(2) $\implies$ (4)'':}
  Let $G \subseteq \GL(V)$ be a linear algebraic group.
  Then $V$ is a rational representation of $G$.
  Then $V^{\otimes d}$ is also a rational representation of $G$ for any $n \in \N_0$.
  Via assumption, $V^{\otimes d}$ is semisimple for all $d \in \N_0$.

  \textbf{``(3) $\implies$ (1)'':}
  Let $U$ be a finite dimensional rational representation of $(G,k[G])$.
  Then by \cref{propxiv.4}, $U$ is a subrepresentation of $k[G]^{\oplus \dim U}$.
  By assumption, $k[G]$ is semisimple.
  Therefore $k[G]^{\oplus \dim U}$ is semisimple.
  Then $U$ is semisimple, as it is a subrepresentation of a semisimple representation.

  \textbf{``(4) $\implies$ (1)'':}
  Let $U$ be a finite dimensional rational representation of $(G,k[G])$ ($G \subseteq \GL(V)$).
  Note that For any rational representation of $N$ of $(G,k[G])$, $N$ is irreducible if and only if $N \otimes \det^{\otimes r}$ is irreducible (for any fixed $r \in \N_0$).
  This is true as $M \subseteq N$ is a subrepresentation iff $M \otimes \det^{\otimes r} \subseteq N \otimes \det^{\otimes r}$ is a subrepresentation.
  Now $V^{\otimes r}$ is semisimple for all $d \in \N_0$.
  Therefore all finite direct sums $S$ of $V^{\otimes d}$'s are semisimple, in particular any subrepresentation and any quotient $Q$ of a subprepresentation of such an $S$ is semisimple.
  Then $U \otimes \Det^{\otimes r} \cong Q$ for such a $Q$ with apropriately chosen $r$.
  Therefore $U \otimes \Det^{\otimes r} $ is semisimple, so $U$ is semisimple.
\end{proof}
\begin{definition}
  An affine algebraic group $(G,k[G])$ satisfying any of the conditions above is called \textbf{linearly reductive}.
\end{definition}
We want an (at least sufficient) criterion for when $(G,k[G])$ is linearly reductive.
\begin{definition}
  If $A \in \GL_n(\C)$, then $A^\ast = \bar A^T$, the adjoint of $A$.
\end{definition}
\begin{lemma}\label{lemxv.2}
  Let $H \subseteq \GL_n(C)$ be a subgroup, and assume that $A \in H \implies A^\ast \in H$.
  Then any finite dimensional (rational) representation of $H$ over $\C$ is semisimple.
\end{lemma}
\begin{proof}
  Let $W$ be any finite dimensional representation.
  Choose a basis $\set{w_i}_i$ of $W$.
  Identify $W \cong \C^m$, $c_i \mapsto e_i$ (standard basis vectors).
  Let $U \subseteq W = \C^m$ be a subrepresentation.
  \textbf{Claim:} There exists a complement of $U$ which is again a subrepresentation.
  Let $(\cdot,\cdot)$ denote the usual scalar product on $\C^n$.
  We will show $U^{\bot}$ (orthogonal complement) is a complement of $U$ which is a subrepresentation.
  Let $w \in U^\bot$ $h \in H$.
  Let $h = A = (A^\ast)^\ast \in \GL_n(\C)$.
  Then for all $u \in U$, we have $(u,h.w) = (u,(A^\ast)^\ast w) = (A^\ast u,w) = 0$ by definition of $U^bot$.
  Therefore $h.w \in U^\bot$, and $U^\bot$ is a subrepresentation.
  We have shown that any subrepresentation has a representation complement, so $W$ is semisimple.
\end{proof}
\begin{corollary}\label{corxv.3}
  $\GL_n(\C)$, $\operatorname{SL}_n (\C)$, $\operatorname{O}_n (\C)$, $\operatorname{SO}_n(\C)$, $\operatorname{Sp}_n(\C) = \{\, A \in \GL_n(\C) \mid A^T J A \allowbreak = J, \, J = \begin{pmatrix} 0 & 1_n \\ 1_n & 0\end{pmatrix}\, \}$ are all linearly reductive.
\end{corollary}
\begin{proposition}\label{propxv.4}~ 
  \begin{enumerate}
  \item If $G \subseteq \GL(V)$, $V$ finite dimensional, is linearly reductive, then any irreducible rational representation of $G$ can be found up to some power of $\det$ as a subrepresentation of $V^{\otimes d}$ for some $d \in \N_0$.
  \item If $G \subseteq \operatorname{SL}_n(V)$, $V$ finite dimensional, is linearly reductive, then any irreducible subrepresentation can be found as a subrepresentation of some $V^{\otimes d}$, $d \in \N_0$.
  \end{enumerate}
\end{proposition}
\begin{proof}~ 
  \begin{enumerate}
  \item $G$ linearly reductive implies that $V^{\otimes d}$ is semisimple for all $d \in \N_0$.
    We know that any finite dimensional representation $U$ appears up to powers of $\det$ as a quotient of some subrepresentation of some finite sum of $V^{\otimes d}$.
    As $V^{\otimes d}$ is semisimple, $U$ is a quotient / direct summand of some direct sum of $V^{\otimes d}$, so we have $\bigoplus_{i \in I} V^{\otimes d_i} \twoheadrightarrow U$ ($I$ finite).
    If $U$ is irreducible, there exists a summand which already surjects, so $V^{\otimes d_i} \twoheadrightarrow U$ for some $i \in I$.
    As $V^{\otimes d_i}$ is semisimple, $U$ is a subrepresentation of $V^{\otimes d_i}$.
    Therefore we have shown that any finite dimensional subrepresentation can be found up to powers of $\det$ inside some $V^{\otimes d}$.

    Assume that $U$ is an irreducible ration representation (not necessarily finite dimensional).
    Then $U = \bigcup_{i \in I} U_i$, where $U_i$ are finite dimensional rational representations.
    As $U$ is irreducible, $U = U_i$ for some $i \in I$.
  \item clear.
  \end{enumerate}
\end{proof}
\begin{theorem}
  If $G \subseteq \GL(V)$, $V$ finite dimensional, is linearly reductive, then
  \begin{enumerate}
  \item If $U$ is an irreducible rational representation, then
    \begin{align*}
      U^\ast \otimes U &\to k[G]\\
      f \otimes U &\mapsto f_U = (g \mapsto f(g^{-1}.u))
    \end{align*}
is injective and the image is the isotypical component of $U$.
  \item $k[G] \cong \bigoplus_{i \in I} U_i^\ast \otimes U_i$ as rational representations, where the sum runs over all $i \in I$ such that $\set{U_i}_{i \in I}$ are representatives for the isomorphism classes of irreducible rational representations.
  \end{enumerate}
\end{theorem}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

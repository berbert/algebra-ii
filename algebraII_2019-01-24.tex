\begin{corollary}\label{corxii.8}
  Let $(X,k[X])$ be an algebraic variety.
  Then $\dim X < \infty$.
\end{corollary}
\begin{proof}
  $k[X]$ is a commutative finitely generated $k$-algebra.
  Hence $k[X]  \cong \pol{k}{x}{1}{n}/I$ for an ideal $I$.
  Take any chain of prime ideals in $k[X]$.
  Any chain can be lifted to $\pol{k}{x}{1}{n}$.
  Since $\dim \pol{k}{x}{1}{n} = n$, we have $\dim k[X] \leq n < \infty$.
\end{proof}
\begin{corollary}\label{corxii.9}
  Let $(X,k[X])$ be an algebraic variety.
  Then any chain of prime ideals stabilizes.
\end{corollary}
\begin{remark}
  This finishes the proof of the construction theorem of connected groups (existence of maximal $\bar Y_a$).
\end{remark}
\begin{corollary}\label{corxii.10}
  Let $Y \subseteq k^n$ be an irreducible subvariety, $Y \neq k^n$.
  Then $\dim Y < n$.
\end{corollary}
\begin{proof}
  $k[Y] \cong \pol{k}{x}{1}{n}/I(Y)$, and $I(Y)$ is prime.
  Let $p_0 \subsetneq p_1 \subsetneq \dotsb \subsetneq p_m \subseteq k[Y]$ be a chain of prime ideals.
  This lifts to a chain in $\pol{k}{x}{1}{n}$ via $\varphi^{-1}(p_i)$.
  Then $(0) \subsetneq \varphi^{-1}(p_0) \subsetneq \dotsb \subsetneq \varphi^{-1}(p_m)$ is a strictly longer sequence of prime ideals.
\end{proof}
\begin{remark}
  The same statement is true for irreducible $X$ and an irreducible subvariety $Y\subseteq X$.
\end{remark}
\begin{corollary}\label{corxii.11}
  Let $(X.k[X])$ be an irreducible algebraic variety, and let $Y \subseteq X$ be a closed and irreducible subvariety with $\dim Y = \dim X -1$.
  Then $Y$ is an irreducible component of $V(f)$ for some $f \in k[X] = \pol{k}{x}{1}{n}/I(X)$.
\end{corollary}
\begin{proof}
  $Y=X$: clear.
  Assume $Y \neq X$.
  Then there exists some $0 \neq f \in k[X]$ with $f(y) = 0$ for all $y \in Y$.
  Then $V \subseteq V(f)$.
  Take an irreducible component of $V(f)$ containing $Y$.
  Then $\dim Z < \dim X$.
  We have $\dim X -1 = \dim Y \leq \dim Z$ with equality iff $Y=Z$.
  Therefore $Y=Z$.
\end{proof}
\begin{remark}
  Let $(X,k[X])$ be an irreducible affine algebraic set, let $0 \neq f \in k[X]$ be not invertible and le $Y \subseteq V(f)$ be an irreducible component.
  Then $\dim Y = \dim X -1$.

  \underline{Application:}
  $\mathbb A^{n^2}$.
  $f = \det - 1$.
  $V(f) = \SL_n$.
  Since $\SL_n$ is irreducible, $\dim \SL_n (k) = n^2 -1$

  \underline{Application:}
  Let $(X,k[X])$ be irreducible, $Y \subsetneq X$ be irreducible, and $\dim Y = \dim X-r$.
  Then there exists a chain $Y = Y_r \subseteq \dotsb \subseteq Y_1$ such that $\dim Y_i = \dim X - i$.
  For $r=1$ we already know this.
  Assume $r >1$.
  Since $Y \neq X$, there exists $f \in I(Y)$ with $Y \subseteq V(f)$.
  Take an irreducible component $Y_1$ of $V(f)$ containing $Y$.
  Then we have $\dim Y_1 = \dim X -1$, then iterate (induction).

  If $f_1, \dotsc f_r \in k[X]$ and $Y \subseteq V(f_1,\dotsc,f_r)$ is irreducible, then $\dim Y \leq \dim X -r$
\end{remark}

\section{Orbits of group actions}
\begin{theorem}\label{thmxiii.1}
  Let $(G,k[G])$ be an affine algebraic group and $(X,k[X])$ an affine algebraic set, and let $G \curvearrowright X$.
  Then
  \begin{enumerate}[label=(\roman*)]
  \item Each orbit is a locally closed subset of $X$.
  \item For an orbit $O$, $\bar O \setminus O$ is a union of orbits of strictly smaller dimension.
  \item Orbits $O$ with $\bar O$ of minimal dimension are closed.
  \end{enumerate}
\end{theorem}
Recall that $A$ is locally closed if and only if $A$ is open in its closure.
\begin{proof}~ 
  \begin{enumerate}[label=(\roman*)]
  \item Let $O = \setdef{g.y}{g \in G}$ be the orbit of $y \in X$.
    The action map $\alpha\colon G \times X \to X$ is a morphism, so as $G \times \set {y}$ is a constructible subset, $\alpha (G \times \set{y}) = O$ is constructible.
    Therefore there exists a nonempty $U \subseteq O$ which is open in $\bar O$ (Stroppel added, which I don't think is necessary or even true: ``with $\bar U = \bar O$'').
    $G$ acts transitively on $O$, so $O = \bigcup_{g \in G} g.U$ is open dense in $\bar O$ (the map $x \mapsto g.x$ is an open map for any $g \in G$, so as $U$ is open in $\bar O$, $g.U$ is also open in $\bar O$ for any $g \in G$).
    Therefore $O$ is open in its closure, so $O$ is locally closed.
  \item Since $O \subseteq \bar O$ is open, $\bar O \setminus O$ is closed in $\bar O$.
    Therefore $\bar O \setminus O < \dim \bar O$ due to the remark after \cref{corxii.10}.
    But $\bar O \setminus O$ is stable under $G$, so it is a union of other $G$-orbits.
  \item If $\dim \bar O$ is minimal, then we can apply (ii), so we have $\bar O \setminus O = \emptyset$, hence $O = \bar O$. \qedhere
\end{enumerate}
\end{proof}
\section{Rational Representations}
\underline{Reminder:}
\begin{itemize}
\item For a finite group $G$, Maschke's theorem says that every finite dimensional representation of $G$ over $C$ is semisimple.
\item Schur-Weyl-duality:  $G = \GL_n(\mathbb{C})$ acts on $V^{\otimes d}$ where $V = \C^n$.
  The subalgebra of $\End_\C (V^{\otimes d})$ generated by the image of $G$ is semisimple.
\end{itemize}
\underline{Question:} What happens if you can replace $G$ by an affine algebraic group?

For geberal algebrabraic groups $G$ the semisimplicity fails even for $k = \C$, but it is true for $\GL_n, \SL_n, O_n, \SL_n$.

$\GL_n$ is a Lie group, an abstract group, and an algebraic group.

\begin{definition}[finite dimensional rational representation]
  A \textbf{finite dimensional rational representation} of an affine algebraic group $(G, k[G])$ is a morphism of algebraic groups $\rho \colon G \to \GL_n (k)$.
\end{definition}
\begin{remark}
  This is in one-to-one correspondence to a linear algebraic action $\alpha \colon G \times k^n \to k^n$.
\end{remark}
\begin{definition}[rational representation]
  Let $(G,k[G])$ be an affine algebraic group, and $V$ a vector space, not necessarily finite dimensional.
  Let $\alpha \colon G \times V \to V$ be an action of $G$.
  Then $\alpha$ is  called a \textbf{rational representation of $G$} if $V = \bigcup_{i \in I} V_i$ for some subspaces of $V_i$ of $V$, which via restriction are finite dimensional rational representations of $G$.
\end{definition}
\begin{definition}
  A \textbf{comodule} for a Hopf algebra $(H,m,\eta, \Delta, \epsilon, \iota)$ is a finite dimensional vector space $V$ together with a linear map $\Delta_V \colon V \to H \otimes V$ satisfying
  \begin{enumerate}
  \item[(C1)]
    \begin{equation*}
      \begin{tikzcd}
        V \arrow[d, "\Delta_V"] \arrow[r,"\Delta_V"] & H \otimes V \arrow[d, "\id \otimes \Delta_V"]  \\
        H \otimes V \arrow[r,"\Delta_H \otimes \id"] & H \otimes H \otimes V
      \end{tikzcd}
    \end{equation*}
  \item[(C2)]
    \begin{equation*}
      \begin{tikzcd}
        V \arrow[rr,"\Delta_V"] \arrow[dr,"\id"'] &&H \otimes V\arrow[dl,"\epsilon \otimes \id"] \\
        & V \cong k \otimes V &
      \end{tikzcd}
    \end{equation*}
\end{enumerate}
\end{definition}
\begin{lemma}
  $\alpha \colon G \times V \to V$ is a rational representation of $G$ if and only if $\alpha^\ast \colon k[V] \to k[G \times V] = k[G] \otimes k[V]$ is a comodule for $k[G]$
\end{lemma}
\begin{example}
  $G$ acts on $V=k[G]$ by left/right translation. Im particular, 
  \begin{align*}
  	G  \times V & \longrightarrow V \\
  	(g,f) & \longmapsto \lambda_g(f) & \text{left translation} \\
  	(g,f) & \longmapsto \rho_g(g) & \text{right translation}
  \end{align*}
  where $\lambda_g(f)(x) = f(g^{-1}x)$ and $\rho_g(f)(x)=f(xg)$. We say in the proof of the linearizaion Theorem that for all $0 \neq f \in V$ there exists $E_f \subset V$ a finite dimensional vector subspace such that $g.\psi \in E_f$ for all $\psi \in E_f$ (for either of the two actions).
  
  So $V = \bigcup_{0 \neq f \in E} E_f$. We proved that \begin{align*}
  	G & \longrightarrow \GL(E_F) \\
  	g & \longmapsto (v \mapsto g.v)
  \end{align*}
  is a morphism of affine algebraic groups.
  
  All in all, $E_f$ is a finite dimensional rational representation ($G$ acts algebraically on $E_f$). Therefore, $V$ is a rational representation of $(G,k[G])$
\end{example}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

\begin{remark}
	For $(A,F_\bullet(A))$ a filtered algebra, the canonical map 
	\begin{align*}
	A &\longrightarrow \gr(A)=\bigoplus_{i \geq 0} \nicefrac{F_i(A)}{F_{i-1}(A)} \\
	a &\longmapsto (a+F_{i-1}(A))_{i \geq 0} 
	\end{align*}
	is an algebra homomorphism. 
\end{remark}
\begin{remark}
	This remark was stated in the lecture but some tutors think it's wrong.
\end{remark}
\begin{definition}[graded $A$-module]
	Let $A=\bigoplus_{i \in \Z} A_i$ be a graded algebra and $M$ an $A$-module. Then a \textbf{grading on $M$} is a decomposition $M=\bigoplus_{i \in \Z} M_i$ into vector spaces such that $A_iM_j \subseteq M_{i+j}$ for all $i,j \in \Z$.
	Then $M$ is called a \textbf{graded $A$-module}.
	
	For $M=\bigoplus_{i \in Z} M_i, N = \bigoplus_{i \in \Z} N_i$, graded $A$-modules, a \textbf{morphism of graded $A$-modules from $M$ to $N$} is a morphism $f: M \to N$ of $A$-modules such that $f(M_i) \subseteq N_i$. 
\end{definition}
\begin{remark}
  Graded $A$-modules with graded $A$-module homomorphisms form a category (where $A$ is a graded algebra).
\end{remark}
\subsection{Symmetric Polynomials} %should be section III.2
\begin{definition}[symmetric polynomial]
  Let $k$ be a field.
  Let $G=S_n=S(\{1,\dots,n\})$.
  It acts linearly on $k[X_1, \dots , X_n]$ by $g.X_1^{a_1}X_2^{a_2}\dots X_n^{a_n}=X_{g(1)}^{a_1}X_{g(2)}^{a_2} \dots X_{g(n)}^{a_n}$ for all $g \in G, a_i \in \Z_{\ge 0}$.
	
  A polynomial in $k[X_1, \dots , X_n]^G$ is called a \textbf{symmetric polynomial} (in $n$ variables). 
\end{definition}
\begin{remark}
  We could replace $k$ by any commutative ring with $1$ and extend the action $R$-linearly to get an action of $G$ on $R[X_1, \dots , X_n]$. 
\end{remark}
\begin{example}
  In $k[X_1,X_2,X_3]^{S_3}$ we have e.g. the following elements
  \begin{itemize}
  \item $p_2^{(3)} = X_1^2+X_2^2+X_3^2$,
  \item $h_2^{(3)}=X_1^2+X_1X_2+X_1X_3+X_2^2+X_2X_3+X_3^2$,
  \item $e_2^{(3)}=X_1X_2+X_1X_3+X_2X_3$ and
  \item $m_{(4,4,2)}^{(3)}=X_1^4X_2^4X_3^2+X_1^4X_2^2X_3^4+X_1^2X_2^4X_3^4$.
  \end{itemize}
\end{example}
\begin{definition}
  Let $n \in \Z_{\ge 0}, r \in \Z_{\ge 0}$. Define the symmetric polynomials
  \begin{itemize}
  \item $p_r^{(n)}=X_1^r+X_2^r+\dots+X_n^r$, the $r$-th power symmetric polynomial (with $p_0^{(n)}=n$)
  \item $h_r^{(n)}=\sum_{|a|=r} X_1^{a_1}X_2^{a_2} \dots X_n^{a_n}$ where $a_i \in \Z_{\ge 0}$ and $\abs{a}:=\sum_{i=1}^n a_i$, the $r$-th complete symmetric polynomial (with $h_0^{(n)}=1$)
  \item $e_r^{(n)}=\sum_{1 \le i_1 < i_2 < \dots < i_r \le n} X_{i_1}X_{i_2} \dots X_{i_r}=\sum_{\substack{I \subset \{1, \dots , n\}\\|I|=r}} \prod_{i \in I} X_i$, the $r$-th elementary symmetric polynomial (with $e_0^{(n)}=1$ and $e_r^{(n)}=0$ if $r>n$) 
  \end{itemize}
\end{definition}
\begin{lemma} \label{lemIII.3}
  For all $n \in \Z_{\ge 0}$ we have in $\Z[X_1, \dots X_n][t]$ that
  \begin{align*}
    \prod_{i=1}^n (t-X_i)&=t^n-e_1^{(n)}t^{n-1}+e_2^{(n)}t^{n-2} - \dots + (-1)^n e_n^{(n)} \\
    &= \sum_{i=0}^n (-1)^i e_i^{(n)}t^{n-i}
  \end{align*}

  \begin{proof} \phantom\qedhere
    The coefficients of $t^{n-j}$ on the left hand side are equal to \[\pushQED{\qed}  \sum_{1 \le i_1 < \dots < i_j \le n} (-X_{i_1})(-X_{i_2}) \dots (-X_{i_j})=(-1)^j e_j^{n} \, . \qedhere
    \popQED \]
  \end{proof}
\end{lemma}
\begin{theorem}[Fundamental theorem of symmetric polynomials] \label{theIII.4} $ $\newline
  The elements $e_1^{(n)}, \dots , e_n^{(n)}$ generate $k[X_1, \dots , X_n]^G$ as a $k$-algebra. Moreover they are algebraically independent over $k$, that means
  \begin{align*}
    k[X_1, \dots , X_n]^{S_n} & \longrightarrow k[t_1, \dots ,t_n] \\
    e_j^{(n)} &\longmapsto t_j
  \end{align*}
  is an isomorphism of algebras.
\end{theorem}
We are first going to prove the following lemmas
\begin{lemma} \label{lemIII.5}
  Let $G$ be a group, $V_i$ representations of $G$ (over some fixed field $k$) for $i \in I$.
  
  Then, $$\left(\bigoplus_{i \in I} V_i\right)^G = \bigoplus_{i \in I} \left(V_i^G\right)$$ as a vector subspace of $\bigoplus_{i \in I} V_i$.
  \begin{proof}
    ``$\supseteq$'' is obvious.
    
    ``$\subseteq$'': Let $v=\sum_{i \in I} v_i \in \left(\bigoplus_{i \in I} V_i\right)^G$. Then $v=g.v=g.\left(\sum_{i \in I} v_i\right)=\sum_{i \in I} g.v_i$ for all $g \in G$. 
    Since the sum is direct, we get $v_i=g.v_i$ for all $i \in I, g \in G$ which means that $v_i \in V_i^G$ for all $i \in I$
  \end{proof}
\end{lemma}
\begin{lemma} \label{lemIII.6}
  A polynomial $f \in k[X_1, \dots , X_n]$ is symmetric $\iff$ its homogeneous parts $f_i \in k[X_1, \dots X_n]_i$ are symmetric.
  \begin{proof}
    Let $A=k[X_1, \dots , X_n]=\bigoplus_{i \in Z} k[X_1, \dots X_n]_i$ the decomposition (since $A$ is graded algebra) where $$k[X_1,\dots , X_n]_i=\begin{cases} \langle\{X_1^{a_1}\dots X_n^{a_n}\}\rangle \mid \sum_{j=1}^n a_j = i, &\text{ if } i \ge 0 \\ \{0\}, & \text{ otherwise} \end{cases}.$$
    $G=S_n$ acts on $A$ as above, preserves $k[X_1, \dots , X_n]_i=A_i$. Using \Cref{lemIII.5}, we get that $k[X_1, \dots , X_n]^{S_n}=A^G=\bigoplus_{i \in \Z} A_i^G=k[X_1, \dots , X_n]_i^{S_n}$
  \end{proof}
\end{lemma}
The following formula holds for all $1 \le r \le n$ (where $n \in \Z_{\ge 0}$): $$e_r^{(n)}=e_r^{(n-1)}+X_ne_{r-1}^{(n-1)}$$ since 
\begin{align*}
  e_r^{(n)}&=\sum_{\substack{I \subset \{1, \dots , n\}\\|I|=r}} \prod_{i \in I} X_i \\
           &=\sum_{\substack{I \subset \{1, \dots , n-1\}\\|I|=r}} \prod_{i \in I} X_i + X_n \sum_{\substack{I \subset \{1, \dots , n-1\}\\|I|=r-1}} \prod_{i \in I} X_i \\
           &= e_r^{(n-1)}+X_ne_{r-1}^{(n-1)}.
\end{align*}
\begin{lemma} \label{lemIII.7} A polynomial $f \in k[X_1. \dots , X_n]$ is symmetric $\iff$ it can be expressed as a polynomial in the $e_r^{(n)}$'s.
\end{lemma}
\begin{proof}
  ``$\impliedby$'': $e_r^{(n)} \in k[X_1, \dots , X_n]^{S_n}$. 
  But $k[X_1, \dots , X_n]^{S_n}$ is a subring, even a subalgebra.
  
  ``$\implies$'': Let $f \in k[X_1, \dots , X_n]^{S_n}$ be a symmetric polynomial.
  With \cref{lemIII.5} we can assume that $f$ is homogeneous.
  
  Induction on $n$.
  
  If $n=1$, then $k[X_1]^{S_1}=k[X_1]^{\{e\}}=k[X_1]=k[e_1^{(1)}]$.
  
  Assume that the lemma holds for $n-1$. Let $d=\deg (f)$.
  
  If $d \le 1$, then the statement is obvious.
  
  Let $d \ge 2$ and assume that the lemma holds for any symmetric polynomial $h$ with $\deg (h)<d$. 
  Consider the map
  \begin{align*}
    q \colon k[X_1, \dots , X_n]  &\longrightarrow  k[X_1, \dots, X_{n-1}] \\
    p(X_1, \dots , X_n) &\longmapsto p(X_1, \dots , X_{n-1},0)
  \end{align*}
  One can easily check that $q$ is an algebra homomorphism (in fact, this map is just modding out $X_n$, and such projections are algebra homos). We have
  \begin{itemize}
  \item $q(e_j^{(n)})=e_j^{(n-1)}$ for all $0 \le j < n$ and
  \item $q(e_n^{(n)})=0$ 
  \end{itemize}
  In the following, when needed, we view $S_{n-1} \leq S_n$ as the subgroup acting on the first $n-1$ elements.
  We have $q(f) \in k[X_1, \dots , X_{n-1}]^{S_{n-1}}$ because for $\sigma \in S_{n-1}$ we have
  \begin{align*}
    (\sigma.q)(f)&=q(f)\left(X_{\sigma^{-1}(1)}, X_{\sigma^{-1}(2)}, \dots , X_{\sigma^{-1}(n-1)} \right) \\
               &=q(f(X_{\sigma^{-1}(1)}, X_{\sigma^{-1}(2)}, \dots, X_{\sigma^{-1}(n-1)},X_n))\\
               &=q((\sigma.f)(X_1, \dots , X_n))\\
               &=q(f)
  \end{align*}
  which gives us $q(f) \in k[X_1, \dotsc , X_{n-1}]^{S_{n-1}}$. %$q(f) \in \pol{k}{X}{1}{n-1}^{S_{n-1}} \subseteq k[X_1, \dotsc , X_n]^{S_{n-1}}$.
  By induction hypothesis, we can write $ q(f) = P(e_1^{(n-1)},\dotsc , e_{n-1}^{(n-1)})$ for some $P \in \pol{k}{t}{1}{n-1} $.
  % in $e_1^{(n-1)}, \dots , e_{n-1}^{(n-1)}$.
  % Set $g=P(e_1^{(n-1)}, \dots , e_{n-1}^{(n-1)}) \in k[X_1, \dots , X_n]$.
  We now define $$g \coloneqq P(e_1^{(n)},\dotsc , e_{n-1}^{(n)}) \in \pol{k}{X}{1}{n}^{S_n}$$ (symmetric as per ``$\implies$''). %or if we define $Q \in \pol{k}{t}{1}{n}$ to be $P$ viewed in $\pol{k}{t}{1}{n}$ (we ``add an input for $X_n$''), we can write $g = Q(e_1^{(n)},\dotsc , e_{n-1}^{(n)},0)$, which is symmetric as per ``$\implies$''.
  Because $q$ is an algebra homomorphism, we have
  \begin{align*}
    q(g)&=q(P(e_1^{(n)},\dotsc,e_{n-1}^{(n)}))  \\
        &=P(q(e_1^{(n)}),\dots,q(e_{n-1}^{(n)}))\\
        &=P(e_1^{(n-1)},\dotsc,e_{n-1}^{(n-1)})
         =q(f)
  \end{align*}

  This implies $q(f-g) = q(f)-q(g) = q(f) - q(f)=0$, which means $X_n \mid f-g$.
  
 Since $f$ and $g$ are symmetric,  $f-g$ is symmetric and therefore $X_i \mid f-g$ for all $1 \le i \le n$ and $X_1X_2 \dotsb X_n \mid f-g$. 
  
  We set $h=\frac{f-g}{X_1X_2 \dotsb X_n} = \frac{f-g}{e_n^{(n)}}$ (here we use that $k[X_1, \dots , X_n]$ is a unique factorization domain).
  
  Now, since $f$ is homogeneous,  so is $g$, so therefore we have $\deg g = \deg q(f) \le \deg{f} =d$, which implies $\deg(h)<d$. 
  
  By assumption $h$ can be written as a polynomial in $e_1^{(n)}, \dots , e_n^{(n)}$ which means that $f-g=e_n^{(n)}h$ can be written as such a polynomial. 
  Therefore, $f=e_n^{(n)}h+g$ can be written like this (by construction of $g$).
\end{proof}
\begin{proof}[Proof of \Cref{theIII.4})]
	
	Left to show: The $e_1^{(n)}, \dotsc , e_n^{(n)}$ are algebraically independent. 
	We will prove this by induction on $n$.
	
	If $n=1$, then $k[X_1]^{S_1}=k[X_1]=k[e_1^{(1)}]$.
	
	Let $n \ge 2$ and assume that the claim holds for $n-1$.
	Let $P \in k[t_1, \dots , t_n]$ be a polynomial with $P(e_1^{(n)},\dots , e_n^{(n)})=0$, chosen to be of minimal possible degree.
	Then \begin{align*}
		0&=q(P(e_1^{(n)},\dots , e_n^{(n)})) \\
		& = P(q(e_1^{(n)}),\dots,q(e_n^{(n)})) \\
		& = P(e_1^{(n-1)}, \dots , e_{n-1}^{(n-1)}, 0)
	\end{align*}
	By induction hypothesis, $t_n \mid P$.
	There exists $\hat P \in k[t_1, \dots , t_n]$ such that $P=t_n\hat P$.
	Furthermore, \[0=P(e_1^{(n)}, \dots , e_n^{(n)})=e_n^{(n)}\hat P(e_1^{(n)},\dots , e_n^{(n)})\] which means that $\hat P(e_1^{(n)}, \dots , e_n^{(n)}) = 0$ since $e_n^{(n)}\neq 0$. 
	As $P$ was chosen of minimal degree, we must have $\hat P = 0$, therefore $P = 0$.
\end{proof}
\begin{remark}
  The proof gives also an algorithm how to express a symmetric polynomial in the $e_1^{(1)}, \dots e_n^{(n)}$
\end{remark}
To better understand the interaction of these symmetric polynomials $e_r^{(n)},p_r^{(n)},h_r^{(n)}$, we use the generating series (in $k[X_1, \dots , X_n][[t]]$). 

Fix $n \in \Z_{\ge 0}$. We have \begin{align*} E(t)& \coloneqq\sum_{r=0}^n e_r^{(n)}t^r \\ H(t)& \coloneqq \sum_{r \ge 0} h_r^{(n)} t^r \\ P(t)& \coloneqq\sum_{r \ge 0} p_{r+1}^{(n)}t^r \end{align*}
\begin{lemma}~ \label{lemIII.8} 
	\begin{enumerate}
		\item $\begin{aligned}E(t)=\prod_{i=1}^n (1+X_it)\end{aligned}$
		\item $\begin{aligned}H(t)=\prod_{i=1}^n \frac{1}{1-X_it}\end{aligned}$ 
		\item $\begin{aligned}P(t)=\sum_{i=1}^n \frac{X_i}{1-X_it}\end{aligned}$
	\end{enumerate}
\end{lemma}
\begin{proof}~ \vspace{-\topsep}
  \begin{enumerate}
  \item clear
  \item  $1-X_it$ is invertible in $k[X_1, \dots , X_n][[t]]$ namely $$Q_i(t)=\frac{1}{1-X_it}:=1+X_it+X_i^2t^2+X_i^3t^3+ \dots \, .$$
    Then,
    \begin{equation*}
      \prod_{i=1}^n \frac{1}{1-X_i^t} = Q_i(t)Q_2(t) \dots Q_n(t) \; .
    \end{equation*}
    
    But here the coefficient of $t^j$ equals $h_j^{(n)}$.
  \item Exercise \qedhere
  \end{enumerate}
\end{proof}

\begin{corollary} \label{corIII.9}
  For all $s \ge 1$, we have
  \begin{align*}
h_s^{(n)}-e_1^{(n)}h_{s-1}^{(n)} +e_2^{(n)}h_{s-2}^{(n)}-\dots + (-1)^s e_s^{(n)} h_0^{(n)}&= \sum_{i=0}^n (-1)^i e_i^{(n)} h_{s-i}^{(n)} = 0 \; .
  \end{align*}

	
	The same holds with $e$ and $h$ swapped. 
\end{corollary}
\begin{proof}
	Exercise
\end{proof}
\begin{corollary} \label{corIII.10}
	For all $j \ge 1$, we have $$jh_j^{(n)}=p_1^{(n)}h_{j-1}^{(n)}+p_2^{(n)}h_{j-2}^{(n)}+ \dots + p_{j-1}^{(n)}h_1^{(n)}+p_j^{(n)}h_0^{(n)}.$$
\end{corollary}
\begin{proof}
	Let $H'(t)$ be the formal derivative of $H(t)$ with respect to $t$. 
	So $$H^\prime(t)=\sum_{r \ge 0} rh_r^{(n)}t^{r-1}.$$ 
	On the other hand (by \Cref{lemIII.8}) $$H'(t)=\sum_{i=1}^n \frac{X_i}{(1-X_it)^2} \prod_{j \neq i} \frac{1}{1-X_jt} = \sum_{i=1}^n \frac{X_i}{1-X_it} \left( \prod_{j=1}^n \frac{1}{1-X_jt} \right)$$
	
	Compare the coefficient of $t^{r-1}$. We receive the lemma  using \Cref{lemIII.8}.
\end{proof}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

\begin{definition}[transposed partition]
  For $\lambda \in \Par$ let $\lambda^t$ be the \textbf{transposed partition} that is $\lambda^t_i = \abs{\setdef{j}{ \lambda_j = i }}$.

   We can picture this as Young diagrams.
\end{definition}
\begin{theorem}
  The $\{\,e_\lambda^{(n)}\,\}$ and $\{\, h_\lambda^{(n)}\,\}$ for $\lambda \in \Par$, $\lambda_1 \leq n$ form a $k$-vectorspace basis of $\pol{k}{X}{1}{n}^{S_n}$ for $k$ any field or $k = \Z$.
  Moreover, $\{\, m_\lambda^{(n)}\,\}$ for $\lambda \in \Par$, $l(\lambda) \leq n$ is also a basis.
\end{theorem}
\begin{proof}
  By the fundamental theorem the monomials in the $e_j^{(n)}$'s are algebraically independent.
  Moreover they generate $\pol{k}{X}{1}{n}^{S_n}$ as a vector space because the $e_j^{(n)}$'s generate $\pol{k}{X}{1}{n}^{S_n}$ as a $k$-algebra, therefore $\{\,e_\lambda^{(n)} \,\}$ form a basis of $\pol{k}{X}{1}{n}$.
  Then the $\{\, h_\lambda^{(n)}\,\}$ form a basis of $\pol{k}{X}{1}{n}$ by applying the Fourier transform.

  In $e_\lambda^{(n)} = e_{\lambda_1}^{(n)} \dotsb e_{\lambda_{l(\lambda)}}^{(n)}$ the maximal possible degree of $X_1$ is $l(\lambda) = \lambda_1^t$.
  Then the maximal possible degree of $X_2$ is $\lambda_2^t$ et cetera.
  In fact $e_\lambda^{(n)} = m_{\lambda^t}^{(n)} + \text{ sum of } m_{\mu^t}^{(n)}$ where $\mu^t < \lambda^t$.
  Therefore the $m_{\lambda^t}^{(n)}$ form a basis, since $e_\lambda^{(n)}$ form a basis with $\lambda_1 < n$.
  (We have $\setdef{\lambda \in \Par}{\lambda_1 \leq n} = \setdef{\lambda \in \Par}{l(\lambda^t) \leq n }$.)
\end{proof}
\subsection{Polynomial Maps}
In this section, $k$ is an infinite field, $V$ a finite-dimensional $k$-vectorspace and $v_1,\dotsc,v_n$ a basis of $V$.
\begin{definition}
  We set $\mathcal{P}_k(V) := \setdef{f \colon V \rightarrow k}{\exists \bar f \in k[V^\ast] = \operatorname{Sym}  V^\ast \colon f = (v \mapsto \ev_v (\bar f))}$.
  This definition is also valid for infinite dimensional vector spaces.

  An alternative (first, supposedly basis dependent) definition:  $\mathcal{P}_k (V) :=  \{\, f \colon V \to k \mid \exists \bar f \in \pol{k}{X}{1}{n} \colon f = \ev_{\Phi_B (v)}(\bar f)\, \}$, where $B = \set{v_1,\dotsc,v_n}$ is the aforementioned fixed chosen basis of $V$, $\dim V = n$, and $\Phi_B \colon V \to k^n$  maps a vector to its coordinates w.r.t $B$.
\end{definition}
\begin{remark}~ \vspace{-\topsep}
  \begin{itemize}
  \item clear: $\mathcal{P}_k(V)$ is a $k$-vector space with pointwise addition and scalar multiplication
  \item the property ``polynomial function'' as in the second given definition does not depend on the choice of basis of $V$ (Exercise, show that the two definitions are actually equivalent.  Then this claim folllows as the first definition is already independent of the basis).
  \end{itemize}
\end{remark}
\begin{lemma}
  Let $W\subseteq V$ be a vector subspace.
  Then if $f \in \mathcal{P}_k(V)$, we also have $\left. f \right|_W \in \mathcal{P}_k(W)$.
\end{lemma}
\begin{proof}
  Choose a basis $w_1,\dotsc,w_m$ of $W$ and extend this to a basis $w_1,\dotsc,w_m,\dotsc,w_n$ of $V$.
  Now we have $f(\sum_{i \in [m]} \alpha_i w_i) = p(\alpha_1,\dotsc,\alpha_m,0,\dotsc,0)$ for some polynomial $p \in \pol{k}{X}{1}{n}$.
  Consider the image $\tilde{p}$ of $p$ under the canonical map $$\pol{k}{X}{1}{n} \rightarrow \pol{k}{X}{1}{n}/(X_{m+1},\dotsc,X_n) \cong \pol{k}{X}{1}{m}\, .$$
  Then by construction $f(\sum_i \in [m] \alpha_i w_i) = \tilde{p}(\alpha_1,\dotsc,\alpha_m)$ with $\tilde{p} \in \pol{k}{X}{q}{m}$.
\end{proof}
\begin{definition}
  For $f,g \in \mathcal{P}_k(V)$ define $fg$ as $(fg)(v) = f(v)g(v)$ for $v \in V$ (this is again a polynomial function).
  It turns $\mathcal{P}_k(V)$ into a $k$-algebra.
\end{definition}
\begin{theorem} \label{theIII.17}
  There is an isomorphism of $k$-algebras
  \begin{align*}
    \beta \colon \pol{k}{X}{1}{n} &\longrightarrow \mathcal{P}_k(V)  \\
    q &\longmapsto f_q \; ,
  \end{align*}
  where $f_q(\sum_{i \in [n]} \alpha_iv_i ) = q(\alpha_1,\dotsc,\alpha_n)$.

  Furthermore, there is also an isomorphism of algebras $\operatorname{Sym} V^\ast \to \mathcal P_k (V)$ given by $\bar f \mapsto (v \mapsto \operatorname{ev}_v (\bar f))$.
\end{theorem}
\begin{proof}
  Only first part, the second is analogous, and we also have $\pol{k}{X}{1}{n} \cong \operatorname{Sym} V^\ast$ anyway.
  
  Define for $1 \leq j \leq n$ the map $\varphi_j \colon V\rightarrow k$ as $\varphi_j(\sum_{i \in [n]} alpha_i v_i) = \alpha_j$, the $j$-th coordinate function.
  We have $\varphi_j \in \mathcal{P}_k(V)$.
  By the universal property of the polynomial algebra $\pol{k}{X}{1}{n}$ there exists a unique algebra homomorphism $\beta \colon \pol{k}{X}{1}{n} \rightarrow \mathcal{P}_k(V)$, $X_j \mapsto \varphi_j$.
  By definition of the multiplication in $\mathcal{P}_k(V)$, we have $$\varphi_1^{a_1} \dotsb \varphi_n^{a_n} (\sum_{i \in [n]} \alpha_i v_i ) = \alpha_1^{a_1}\dotsb\alpha_n^{a_n} \, ,$$ therefore $\beta$ sends $p$ to $f_p$.
  by definition of polynomial maps $\beta$ is surjective.
  $\beta$ is injective, since $k$ is infinite (also an easy exercise).    
\end{proof}
\begin{remark}
  The theorem does not hold if $k$ is not infinite.
  Take for instance the poynomial $t^2 + t \in \F_2[t]$ and $0 \in \F_2[t]$, which both map to the zero polynomial function.
\end{remark}
\begin{definition}[homogeneous]
  $f \in \mathcal{P}_k(V)$ is \textbf{homogeneous of degree $d$} if $f(\lambda v) = \lambda^d f(v)$ for all $\lambda \in k$ and $v \in V$.
\end{definition}
\begin{proposition}
  $\mathcal{P}_k(V) = \bigoplus_{d \geq 0} \mathcal{P}_k(V)_d$, $\mathcal{P}_k(V)$ becomes a non-negatively graded algebra.
\end{proposition}
\begin{proof}
  Clearly, $\mathcal{P}_k(V)_d \cap \mathcal{P}_k (V)_{d^\prime} = \{0\}$ for $d \neq d^\prime$ (again by argument of $k$ being infinite), therefore $\bigoplus_{d \geq 0} \mathcal{P}_k(V)_d \subseteq \mathcal{P}_k (V)$.
  Via the isomorphism $\beta$ from \cref{lemIII<.17} a monomial $ p = X_1^{a_1}\dotsb X_n^{a-n} \in \pol{k}{X}{1}{n}$ to $f_p$ with $f_p(\sum{i \in [n]} \lambda_i v_i) = p(\lambda_1,\dotsc,\lambda_n)$.
  Then $f_p(\lambda v) = p(\lambda\lambda_1,\dotsc,\lambda\lambda_n) = \lambda^{a_1 + \dotsb + a_n} p(\lambda_1, \dotsc , \lambda_n)$.
  Hence $f+p$ is homogeneous of degree $d = a_1 + \dotsb + a_n$.
  Therefore
  \begin{align*}
    \beta (\pol{k}{X}{1}{n})
    = \beta(\bigoplus_{d \geq 0} \pol{k}{X}{1}{n}_d)
    = \bigoplus_{d \geq 0} \beta(\pol{k}{X}{1}{n}_d)
    \subseteq \bigoplus_{d \geq 0} \cP_k(V)_d = \cP_k(V)
  \end{align*}
  Alltogether $\beta$ is an isomorphism of graded algebras.
\end{proof}
\begin{definition}[polynomial function]
  Let $W$ be a $k$-vector space.
  $f \colon W \rightarrow V$ is a \textbf{polynomial function} if the functions $f_i \colon W \rightarrow k$ defined by the relation $f (w) = \sum_{i \in [n]} f_i(w) v_i$ are polynomial functions in $\cP_k(V)$.
  We denote the set of all polynomial functions by $\cP_k(W,V)$.

  An alternative definition (which should also work for infinite dimensional vector spaces):  $f \colon W \to V$ is polynomial if and only if for all $\varphi \in V^\ast$ we have $\varphi \circ f \in \mathcal P_k (W)$.
\end{definition}
\begin{remark}
  This property is independent of the choice of basis. (Exercise)
\end{remark}
\begin{remark}
  Special case $V = k$, then $f$ is a polynomial function if and only if $f \in \cP_k(W)$.
\end{remark}
\begin{lemma}\label{lemIII.20}
  $\cP_k(W,V)$ with $W$ finite dimensional and $V$ arbitrary is a $\cP_k(W)$-module via $(f.g)(w) = f(w)g(w)$ for $f \in \cP_k(W)$, $g \in \cP_k(W,V)$ and $w \in W$.
\end{lemma}
\begin{proof} \phantom\qedhere
  $\operatorname{Maps}(W,V)$ is a $\operatorname{Maps}(W,k)$-module.
  To check:  $\cP_k(W,V)$ is preserved under the action of $P_k(W) \subseteq \operatorname{Maps}(W,k)$.
  Let $f \in \cP_k(W)$, $g \in \cP_k(W,V)$.
  Then
  \begin{align*}
    (fg)\left(\sum_{i \in [m]} \lambda_i w_i \right)
    = & f\left(\sum_{i \in [m]}\lambda_i w_i\right) g\left(\sum_{i \in [m]}\lambda_i w_i\right) \\
    = & \sum_{j \in [m]}\left( p(\lambda_1,\dotsc,\lambda_m)g_j\left(\sum_{i \in [m]} \lambda_i w_i\right)v_j \right) \; ,
  \end{align*}
  where $g_j (\sum_{i \in [m]} \lambda_i w_i) = q_j(\lambda_1,\dotsc,\lambda_m)$ for some $p,q_j \in \pol{k}{X}{1}{m}$.
  Therefore $$fg\left(\sum_{i \in [m]}\right) = \sum_{j \in [n]} (fg)_j\left(\sum_{i \in [m]}\lambda_i w_i\right)v_j$$ where \[\pushQED{\qed} (fg)_j\left(\sum_{i \in [m]} \lambda_i w_i\right) = p (\lambda_1,\dotsc,\lambda_m)q_j(\lambda_1,\dotsc,\lambda_m) \, . \qedhere \popQED\]
\end{proof}
\begin{proposition}\label{III.22}
  For $f \in \cP_k(W,V)$ we have that $f^\ast \colon \cP_k(V) \rightarrow \cP_k(W)$, $h \mapsto h \circ f$ is an algebra homomorphism, called the \textbf{pullback of $f$}, or the \textbf{comorphism attached to $f$}.
\end{proposition}
\begin{proof}
  \begin{itemize}
  \item
    $f \in P_k(W,V)$, $h \in \cP_k(V)$ implies by \Cref{lemIII.20} that $h \circ f \in \cP_k(W)$.
  \item
    $\begin{aligned}
      f^\ast(h_1 + h_2)(w) = (h_1 + h_2) (f(w))
      = h_1(f(w)) + h_2(f(w))
      = (f^\ast(h_1) + f^\ast(h_2))(w)
    \end{aligned}$
  \item 
    $k$-linearity
  \item
    $f^\ast(h_1h_2) = f^\ast(h_1)f^\ast(h_2)$ \qedhere
  \end{itemize}
\end{proof}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

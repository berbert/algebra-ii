\begin{corollary}\label{corxii.2}
  Let $G$ be a linear algebraic group and $H_i$, $i \in I$ closed connected subgroups which generate $G$ as a group.
\end{corollary}
\begin{proof}
  Let $\varphi_i \colon H_i \to G$ be the inclusions and apply \cref{propxii.1}.
\end{proof}

Goal:  Notion of dimension of affine algebraic varieties.
\begin{definition}
  Let $R$ be a commutative ring with $1$.
  The \textbf{Krull dimension} of $R$ is defined as
  \begin{equation*}
    \dim(R) = \operatorname{max} \setdef{d \in \N}{\exists \; p_d \supsetneq p_{d-1} \supsetneq \dotsb \supsetneq p_1 \supsetneq p_0, \, p_i \, \text{prime}} \, ,
  \end{equation*}
  and we define the dimension of the zero ring as $-1$ (the maximum over the empty set).
\end{definition}
\begin{remark}
  $0$-dimensional varieties are usually called points, and $1$-dimensional varieties are usually called curves.
\end{remark}
\begin{definition}
  Let $I \subseteq R$ be an ideal.
  We define the \textbf{height} of the ideal $I$ as
  \begin{equation*}
    \operatorname{ht} (I) := \operatorname{max} \setdef{d \in \N}{\exists \; I \supseteq p_d \supsetneq p_{d-1} \supsetneq \dotsb \supsetneq p_1 \supsetneq p_0, \, p_i \, \text{prime}} \, .
  \end{equation*}
\end{definition}
\begin{example}~ 
  \begin{enumerate}
  \item If $R = k$ is a field then $\dim (R) = 0$.
  \item $\dim k[t] = 1$ for a field $k$.
    We have the chain $0 \subseteq (t)$ of prime ideals, and since we are in a principal ideal domain, this is maximal
  \item $\dim R = 1$ for any principal ideal domain, argument as in (2)
  \end{enumerate}
\end{example}
\begin{definition}
  Let $(X,k[X])$ be an affine algebraic variety.
  Then its \textbf{dimension} is defined as $\dim X := \dim k[X]$.

  For $Y \subseteq X$ closed we call $\operatorname{codim} (Y,X) := \hgt(I(Y))$ the \textbf{codimension} of $Y$ in $X$ (sometimes if the context is clear: $\operatorname{codim} (Y)$).
\end{definition}
\begin{example}~ 
  \begin{enumerate}
  \item $Y = X$ means $I(Y) = 0$, therefore $\operatorname{codim}(Y) = 0$.
  \item $R = k[X_1,X_2]$ and consider $\mathbb{A}^2 = (k,R)$.
    Then $R \supseteq (X_1,X_2) \supsetneq (X_2) \supsetneq 0$ as a chain of prime ideals.
    We have
    \begin{equation*}
      \begin{array}{c|ccc}
        I & (X_1,X_2) & (X_2) & 0 \\
        \hline 
        R/I & k & k[X_1] & k[X_1,X_2] \\
        \dim R/I &0 & 1 & \geq 2  \\
        V(I)& \mathrm{point} & \mathrm{curve} & \mathrm{plane}
      \end{array}
    \end{equation*}
  \end{enumerate}
\end{example}
\begin{remark}
  For any ideal $J$ of $R$ we have a bijection
  \begin{align*}
    \set{\text{prime ideals in } R/J} &\leftrightarrow \setdef{p \subseteq R \, \text{prime}}{J \subseteq p}  \\
    I & \mapsto \varphi^{-1}(I) \, ,
  \end{align*}
  where $\varphi \colon R \to R/J$ is the canonical projection.

  Hence our chain is a maximal chain of prime ideals, hence $k[X_1,X_2]/(X_2) \cong k[X_1]$ has dimension $1$.
\end{remark}
\begin{example}
  Let $p$ be a prime ideal of $R$, and let $n := \dim R/p$, $m:= \hgt(p)$.
  Then there are chains of prime ideals (using the above remark $p \subseteq p_0 \subsetneq p_1 \dotsb \subsetneq p_n \subseteq R$ and $q_0 \subsetneq q_1 \subsetneq \dotsb \subsetneq q_m \subseteq p$.
  Therefore $\dim R \geq \dim R/p + \hgt (p)$.

  Geometrically:  Let $(X,k[X])$ be an affine algebraic variety.
  Let $Y \subseteq X$ be closed and irreducible.
  Then $\dim k[X] \geq \dim k[X] / I(Y) + \hgt I(Y)$, where $I(Y)$ is prime as $Y$ is irreducible.
  Or: $\dim X \geq \dim Y + \operatorname{codim} Y$.

  Warning:  Maximal chains of prime ideals in a commutative ring need not all have the same length!
\end{example}
\begin{example}
  Let $X = V(\set{x_1x_3,x_2x_3}) \subseteq k^3$.
  
  We can show that $X = Y_1 \cup Y_2$ where $Y_1 = V(\set{x_1,x_2})$, $Y_2 = V(\set{x_3})$, $Y_1$ and $Y_2$ are the irreducible components.
  We have $Y_1 = V(\set{x_1,x_2}) \supsetneq V(\set{x_1,x_2,x_3-a})$ for some $a \in k$, and $Y_2 = V(\set{x_3}) \supsetneq V(\set{x_3,x_1-x_2}) \supsetneq V(\set{x_3,x_2,x_1})$.
  These are chains of prime ideals.

  Now $k[X_1,X_2,X_3]/(X_1,X_2) \cong k[X_3]$ has dimension $1$,
  $k[X_1,X_2,X_3]/(X_3,X_1-X_2) \cong k[X_1]$ has dimension $1$ and $k[X_1,X_2,X_3]/(X_3)$ has dimension $\geq 2$.

  We can check that both chains are maximal.
  Therefore $\dim Y_1 = 1$, $\dim Y_2 = 2$.
\end{example}
\begin{remark}
  If $p$ is a prime ideal of $R$, then we have a bijection
  \begin{align*}
    \setdef{q\subseteq R \, \text{prime}}{q \subseteq p} &\leftrightarrow \set{ \, \text{prime ideals of } R_p}  \\
    I &\mapsto \alpha(I) := \setdef{\frac{r}{s}}{r \in I, s \in R\setminus p} \\
    \varphi^{-1}(J) & \mapsfrom J \, ,
  \end{align*}
  where $\varphi \colon R \to R_p$ is the localization at $p$.
\end{remark}
\begin{remark}
  $\dim R = \operatorname{sup} \setdef{\dim R_m}{m \subseteq R \text{ maximal ideal}}$.

  \underline{``$\leq$'':} Let $p_0 \subsetneq p_1 \subsetneq p_2 \subsetneq \dotsb \subsetneq p_l$ be a chain of prime ideals in $R$.
  Let $m \subseteq R$ be a maximal ideal containing $p_l$.
  With our above renark we have $\dim R \leq \dim R_m$.

  \underline{``$\geq$'':} Any chain of prime ideals in $R_m$ can be lifted to a chain of prime ideals in $R$, therefore $\operatorname{max} \setdef{\dim R_m}{m \text{ maximal ideal}} \leq \dim R$.
\end{remark}

Goal:  Theorem $\dim \pol{k}{X}{1}{n} = n$ for any field $k$.

Clear: $0 \subsetneq (X_n) \subsetneq (X_n,X_{n-1}) \subsetneq \dotsb \subsetneq (X_n, \dotsc,X_1)$, so we have $\dim \pol{k}{X}{1}{n} \geq n$.

For $x \in R$ we set $S_x := \setdef{r \in R}{r = x^m(1-ax) \text{ for some } m \in \N,\, a \in R}$.

Clear: $x,1 \in S_x$, $z_1,z_2 \in S_x \implies z_1z_2 \in S_x$, $x$ invertible $\implies 0 \in S_x$.

We set $R_x$ as the localization of $R$ at $S_x$, that is $R_x = \setdef{\frac{r}{s}}{r \in R, \, s \in S_x}/\sim$.

\begin{proposition}\label{propxii.4}
  Let $R$ be a commutative ring.
  Then $\dim R \leq l \iff \forall x \in R \colon \dim R_x \leq l-1$.
\end{proposition}
\begin{proof}
  \textbf{Claim 1:} $\forall x \in R, \, \forall m \subseteq R \text{ maximal}\colon m \cap S_x \neq \emptyset$.
  
  If $x \in m$, then $x \in m \cap S_x$.
  If $x \notin m$, then there exists $y \in R$ such that $xy = 1+z$ for some $z \in m$.
  Therefore $1-xy \in m \cap S_x$, which shows claim 1.

  \textbf{Claim 2:}  If $m \subseteq R$ is a maximal ideal, $p \subsetneq m$ is prime and $x \in m \setminus p$, then $p \cap S_x = \emptyset$.

  Assume that $p \cap S_x \neq \emptyset$.
  Let $x^m(1+ax) \in p$ with $a \in R$.
  Then $1 + ax \in p$ therefore $1 \in m$, which is a contradiction, so claim 2 follows.

  \underline{``$\implies$'':} Any chain of prime ideals in $R_x$ gives rise to a chain of prime ideals in $R$ (by remark).
  If this chain is maximal, then the last ideal in the chain must be a maximal ideal, say $m \subseteq R$.
  Then $\varphi(m) = S_x$, where $\varphi \colon R \to R_p$ is the localization (by claim 1).
  Therefore $\dim R \geq \dim R_x +1$ for all $x$ and therefore $\dim R_x \leq l-1$ for all $x$.

  \underline{``$\Longleftarrow$'':} By claim 2 we can fine $x \in R$ such that $\dim R_x = l-1$.
  Then $\dim R \leq \dim R_x + 1 = l$.
\end{proof}
\begin{corollary}\label{corxii.5}
  Let $l \in \N$ and let $R$ be a commutative ring.
  Then $\dim R \leq l$ if and only if for given $x_0,\dotsc,x_l \in R$ there exist $a_0,\dotsc,a_l \in R$ and $m_0,\dotsc,m_l \in \N$ such that
  \begin{eqnarray*}
    \dotsb x_{l-1}^{m_{l-1}}((x_l^{m_l}(1+a_lx_l)+a_{l-1}x_{l-1})+a_{l-2}x_{l-2}) + \dotsb = 0
  \end{eqnarray*}
\end{corollary}
\begin{proof}
  Induction in $l$.

  For $l = 0$ we have
  \begin{align*}
    \dim R = 0 &\stackrel{\text{\cref{propxii.4}}}{\iff} \forall x \in R \colon \dim R_x < 0\\
               &\iff R_x = 0\\
               &\iff 0 \in S_x \\
               &\iff \exists m \in \N, \, a \in R \colon x^m(1+ax) = 0 \; .
  \end{align*}


  Assume these statements for all ringe $\neq 0$ with dimension $\leq l-1$.
  In particular we can apply this to any localization $S^{-1}R$ of $R$ with dimension $\leq l-1$.
  Our induction hypothesis implies $\dim (S^{-1}R) \leq l-1$ if and only if $ \dotsb x_{l-2}^{m_{l-2}}((x_{l-1}^{m_{l-1}}(s+a_{l-2}x_{l-2})+a_{l-2}x_{l-2}) + \dotsb = 0$.
  Now take $S^{-1}R = R_{x_l}$, and replace $s$ by $x_l^{m_l}(1 +a_lx_l$, which shows the induction step.
\end{proof}
\begin{corollary}\label{corxii.6}
  Let $k$ be a field and let $R$ be a commutative $k$-algebra.
  If any sequence $x_0,x_1,\dotsc,x_l$ of elements in $R$ is algebraically dependent over $k$, then $\dim R \leq l$.
\end{corollary}
\begin{proof}
  Let $Q(x_0,\dotsc,x_l) = 0$ for some $Q \in \pol{k}{t}{0}{l}$.
  Order the monomials in $Q$ lexicographically with smallest element first, wlog leading term is $1$.
  Then
  \begin{align*}
    0 =& Q(x_0,\dotsc,x_l)\\
    =& x_0^{m_0}\dotsb x_l^{m_l} \\
       &+ x_0^{m_0}x_1^{m_1} \cdot \dotsb \cdot x_{l-1}^{m_{l-1}}x_l^{m_l +1}r_l  \\
       &+ x_0^{m_0}x_1^{m_1} \cdot \dotsb \cdot x_{l-1}^{m_{l-1}+1}r_{l-1}  \\
       &\dots  \\
       &+ x_0^{m_0+1}r_0 \; ,
  \end{align*}
  where $r_j \in k[x_i \mid i >j]$.
  But then \cref{corxii.5} gives $\dim R \leq l$.
\end{proof}
\begin{corollary}\label{corxii.7}
  $\dim \pol{k}{x}{1}{n} = n$
\end{corollary}
\begin{proof}
  Any $n+1$ elements are algebraically dependent.
\end{proof}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

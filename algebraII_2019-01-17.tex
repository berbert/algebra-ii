\textbf{Recall: }  $\mathbb A^n = (k^n,k[k^n])$ is the affine algebraic variety of dimension $n$, where $k[k^n]$ are the polynomial functions $k^n \to k$, which is isomorphic to $\pol{k}{x}{1}{n}$, the polynomial ring in $n$ indeterminates over $k$.
Now, given $n,m \in \N$, and any affine algebraic variety $(Y,k[Y])$ and morphisms $f_1,f_2$ as follows:
\begin{equation*}
  \begin{tikzcd}
    &Y \arrow[ddl,"f_1"'] \arrow[ddr, "f_2"]&  \\
    & k^m \times k^n \arrow[dl, "\operatorname{pr}_1"] \arrow[dr, "\operatorname{pr}_w"']&\\
    k^m && k^n \; ,
  \end{tikzcd}
\end{equation*}
we then get comorphisms
\begin{equation*}
  \begin{tikzcd}
    &k[Y]&  \\
    &k[k^m \times k^n]& \\
    k[k^m] \arrow[uur, "f_1^\ast"] \arrow[ur, "\operatorname{pr}_1^\ast"'] && k[k^n] \arrow[uul, "f_2^\ast"'] \arrow[ul, "\operatorname{pr}_2^\ast"] \; .
  \end{tikzcd}
\end{equation*}
Now we will identify the following algebras:
\begin{align*}
  k[k^m \times k^n] &= \set{\text{polynomial maps} \, k^m \times k^n \to k}\\
                    &= \set{\text{polyn. maps} \, k^{m+n} \to k}  \\
                    &= \pol{k}{X}{1}{n+m} \; .
\end{align*}
The projections $\pr_i$ project to the subspace spanned by the first $m$ resp. last $n$ standard basis vectors.
We get inclusions (as algebra homomorphisms)
\begin{align*}
  \iota_1 = \pr_1^\ast \colon \pol{k}{X}{1}{m} &\to \pol{k}{X}{1}{m+n}  \\
  X_i &\mapsto X_i  \\
  \iota_1 = \pr_1^\ast \colon \pol{k}{X}{1}{n} &\to \pol{k}{X}{1}{m+n}   \\
  X_i &\mapsto X_{m+i}
\end{align*}
\begin{lemma}\label{lemxi.1}
  Let $X \Subset k^m$, $Y \Subset k^n$.
  Then $X \times Y \Subset k^m \times k^n$.
  Moreover, $I(X\times Y) = I(X) + I(Y)$.
\end{lemma}
\begin{proof}
  Let $X = V(M)$ and $Y= V(N)$, with $M \subset \pol{k}{X}{1}{m}$, $N \subset \pol{k}{X}{1}{n}$.
  We view $M$ and $N$ as subsets of $\pol{k}{X}{1}{m+n}$.
  Then $X \times Y = V(M\cup N)$.

  \underline{``$\subseteq$'':} $f \in M \implies f((x,y)) = f(x) = 0$ for all $(x,y) \in X\times Y$, as $X = V(M)$.
  The same goes for $f \in N$.

  \underline{``$\supseteq$''} Let $(a,b) \in k^m \times k^n$, $(a,b) \notin X \times Y$.
  Wlog $a \notin X$, which means that there exists $f \in \langle M \rangle$ such that $f(a) \neq 0$, therefore there exists an $f \in \langle M \cup N \rangle$ such that $f((a,b)) \neq 0$.

  We have concluded $X \times Y = V(M \cup N)$, so we have shown that $X \times Y$ is closed.
  Now to show is the statement $I(X\times Y) = I(X) + I(Y)$.

  \underline{``$\supseteq$'':} obvious.

  \underline{``$\subseteq$'':} Let $f \in I(X \times Y)$.
  We can write $f = \sum_{i=1}^s f_ig_i$ with $f_i \in \pol{k}{X}{1}{m}$ and $g_i \in \pol{k}{X}{m+1}{m+n}$.

  If $g_i (y) = 0$ for all $y \in Y$, then $f \in I(Y) \subseteq I(X) + I(Y)$.

  If there exists a $y \in Y$ and a $g_i$ such that $g_i(y) \neq 0$, wlog $i =1$, then $f \in I(X \times Y)$ implies $ \sum_{i=1}^s f_ig_i(y) = 0$ as a function on $X$.
  Then $h = \sum_{i=1}^s f_ig_i(y) \in I(X)$.
  We have $f_1 = \frac{h - g_2(y)f_2 - \dotsb - g_n(y)f_n}{g_1(y)}$.
  If we plug this into $f = \sum_{i=1}^s f_ig_i$ with $f_i \in \pol{k}{X}{1}{m}$ and we get $f = \sum_{i=1}^{s-1} f_i^\prime g_i^\prime + h^\prime$.
  Repeating this gives $f \in I(X)$.
  Therefore $f \in I(X) + I(Y)$ for all $ f\in I(X \times Y)$.
\end{proof}
\begin{proposition}\label{propxi.2}
  Let $X \Subset k^m$, $Y \Subset k^n$ both be irreducible.
  Then $X \times Y \Subset k^{m+n}$ is irreducible.
\end{proposition}
\begin{remark}
  Comparing this with \cref{lemX.2} gives us that $X \times Y$ is irreducible as a topological space for the Zariski topology and also for the product topology.
\end{remark}
\begin{proof}
  Assume that $X \times Y = (Z_1 \cap (X \times Y)) \cup (Z_2 \cup (X \times Y))$, where $Z_i \subseteq k^m \times k^n$ is closed.

  To show: $X \times Y = Z_i \cap (X \times Y)$ for some $i$.

  For $x \in X$, let $C_x := \set{x} \times Y$.
  then $C_x$ is closed in $k^m \times k^n$ by \cref{lemxi.1}.
  Let $C_x = (C_x \cap W_1) \cup (C_x \cap W_2)$ where $W_1 , W_2 \Subset k^m \times k^n$.
  Thus $W_i = V(M_i)$, $M_i \subseteq \pol{k}{X}{1}{m+n}$.
  Let $\tilde M_i = \setdef{\tilde f = f(x,-) }{f \in M_i} \subseteq \pol{k}{X}{m+1}{m+n}$.
  Then $Y = (V(\tilde M_1) \cap Y) \cup (V(\tilde M_2)\cap Y)$.
  As $Y$ is irreducible, $Y = V(\tilde M_i) \cap Y$ for some $i$.
  Therefore, $C_x = V(M_i) \cap C_x$ for some $i$.
  This means that $C_x$ is irreducible.
  Now $C_x = (C_x \cap Z_1) \cup (C_x \cap Z_2)$, but because $C_x$ is irreducible, $C_x \subseteq Z_i$ for some $i$.
  Let $X_i := \setdef{x \in X}{C_x \subseteq Z_i}$.
  Hence $X = X_1 \cup X_2$.

  \underline{Claim : $X_i$ is closed.}
  Since $X \times \set{y} \subseteq k^m \times k^n$ is closed for all $ y \in Y$ by \cref{lemxi.1}, the set $X_i^y := \setdef{x \in X}{(x,y) \in (X \times \set{y}) \cap Z_i}$ is closed as well.
  Now $\bigcap_{y \in Y} X_i^y = X_i$, which impiles that $X_i$ is closed.
  As $X$ is irreducible and since $X = X_1 \cup X_2$, we know $X = X_1$ or $X = X_2$.
  This implies $X\times Y \subseteq Z_1$ or $X \times Y \subseteq Z_2$.
  Therefore $(X \times Y) \times Z_i = X \times Y$ for some $i$.
\end{proof}
\begin{corollary}\label{corxi.3}
  Let $(X,k[X])$ and $(Y,k[Y])$ be affine algebraic varieties and $X^\prime \subseteq X$, $Y^\prime \subseteq Y$ be closed and irreducible.
  Then $X^\prime \times Y^\prime \subseteq X \times Y$ is closed and irreducible.
\end{corollary}
\newpage
\section{Generalization of irreducible sets and dimension}

Group theoretic fact:
$\SL_n (k)$ can be generated (as an ordinary group) by the sets
\begin{equation*}
  U_{i,j}:= \setdef{
    \begin{pmatrix}
      1 & & a_{i,j} \\
      &\ddots& \\
      && 1
    \end{pmatrix} = A = (a_{i,j})_{i,j}}{\forall r \colon a_{r,r} =1, \,  a_{i,j} \text{ arbitrary}, \, a_{r,s} = 0 \text{ otherwise}}
\end{equation*}
Check $U_{i,j} \cong \mathbb{G}_a$ as ordinary groups.

\underline{Idea:} $\mathbb{G}_a$ is connected.
Find a tool to build connected groups together to create new connected groups.
Use this equation to show $\SL_n(k)$ is connected.
\begin{definition}
  Let $(G,k[G])$ be an affine algebraic group and let $M \subseteq G$ be a subset.
  Then the \textbf{algebraic group generated by $M$} is the smallest closed subgroup of $G$ containing $M$, denoted $\langle M \rangle_{\mathrm{alggrp}}$.
  More precisely, $\langle M \rangle_{\mathrm{alggrp}} = \bigcap_{\substack{H \leq G \\ H \Subset G, \, M \subset H}} H$.
\end{definition}
\begin{proposition}\label{propxii.1}
  Let $(G,k[G])$ be an affine algebraic gorup and let $(X_i,k[X_i])_{i \in I}$ be a family of affine algebraic varieties.
  Assume that all $X_i$ are irreducible, and let $\varphi_i \colon X_i \to G$ be a morphism of affine algebraic varieties, such that $e \in \varphi_i (X_i)$ for all $i \in I$.
  Let $Y_i := \varphi_i (X_i)$ for $i \in I$ and let $M := \bigcup_{i \in I} Y_i$.
  Then
  \begin{enumerate}
  \item $ \langle M \rangle_{\mathrm{alggrp}} $ is a connected and closed subgroup of $G$.
  \item $H = Y_{a_1}^{\varepsilon_1} \cdot \dotsb Y \cdot Y_{a_n}^{\varepsilon_n}$ for some $a_1, a_2, \dotsc,a_n \in I$, $n \in \N$, $\varepsilon_1,\dotsc,\varepsilon_n \in \set{-1,1}$
  \end{enumerate}
\end{proposition}
\begin{proof}
  Wlog we assume that for any $i \in I$ the morphism $\varphi_i \colon X_i \to G$, $x \mapsto \varphi_i(x)^{-1}$, is amongst th $\varphi_j$'s.
  Now for a finite sequence $a$ of elements in $I$, say $a_1,\dotsc,a_n$, we denote $Y_a := Y_{a_1}\cdot\dotsb\cdot Y_{a_n}$.
  Then $Y_a$ is irreducible, because it is the image of the morphism $X_1 \times \dotsb \times X_n \xrightarrow{\varphi_1 \times \dotsc \times \varphi_n} G \times \dotsb \times G \xrightarrow{"\text{multiply}"} G$, and $X_1 \times \dotsb \times X_n$ is irreducible.
  Then, also $\bar{Y_a}$ is irreducible and by assumption $e \in Y_{a_i}$ for all $i \in I$, so $e \in \bar{Y_a}$.
  Therefore $\bar{Y_a}$ is an irreducible closed subset of $G$ containing $e$.
  
  Now we pick $a$ such that $Y_a$ is maximal with these properties (justification that this can be done: later).
  Let $b,c$ be finite sequences in $I$.
  The multiplication $y \mapsto yx$ with $x \in Y_c$ sends $Y_b$ to $Y_{bc}$, hence $\bar{Y_b} Y_c \subseteq \bar{Y_{bc}}$.
  The multiplication with $x \in \bar{Y_b}$, $x \mapsto xz$ sends $Y_c$ to $\bar{Y_{bc}}$.
  Hence it sends $\bar{Y_c}$ to $\bar{Y_{bc}}$.
  Therefore $\bar{Y_b} \bar{Y_c} \subseteq \bar{Y_{bc}}$.
  
  Now, for all $b$, we have $ \bar{Y_a} \subseteq \bar{Y_a} \subseteq \bar{Y_a} \bar{Y_b} \subseteq \bar{Y_{ab}}$, as $e \in Y_b$ for all $b$.
  Since $Y_a$ was chosen maximal, $\bar{Y_a} = \bar{Y_a}\bar{Y_b} = \bar{Y_{ab}}$ for all $b$.
  Take $b =a$.
  Then $\bar{Y_a}\bar{Y_a} \subseteq \bar{Y_a}$.
  Take $b$ such that $Y_nb = Y_b^{-1}$ (our wlog assumption).
  Then $\bar{Y_a}\bar{Y_b} = \bar{Y_a}\bar{Y_a^{-1}} = \bar{Y_a}\bar{Y_a^{-1}} = \bar{Y_a}$, as $e \in \bar{Y_a}$.
  Therefore $\bar{Y_a}$ is a subgroup of $G$, obviously closed.
  It contains $\bar{Y_b} = e \bar{Y_b} \subseteq \bar{Y_a}\bar{Y_b}$ for all $b$ and is the smallest possible containing $\bar{Y_a}$.
  Therefore $\bar{Y_a} = \langle M \rangle_{\mathrm{alggrp}}$.

  Now $Y_a$ is the image of a morphism applied to a constructable set, hence constructable.
  $Y_a$ is the image of an irreducible set because $X_{a_1} \times \dotsb \times X_{a_n}$ is irreducible by assumption.
  Therefore $\bar{Y_a} = (\bar{Y_a})_0$, therefore $\bar{Y_a}$ is connected.
  On the other hand \cref{propX.8} gives us $Y_a = \bar{Y_a}$ because $Y_a$ is a group.
  Therefore $Y_a = \bar{Y_a}$ is a connected closed subgroup.
  By definition: $Y_a = Y_{a_1} \cdot \dotsb \cdot Y_{a_n}$, which implies (2).
\end{proof}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

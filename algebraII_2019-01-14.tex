\begin{lemma} \label{lemX.6}
	Let $(G,k[G])$ be an affine algebraic group. Let $U,V \subset G$ be open and dense. Then $G=U \cdot V$
\end{lemma}
\begin{proof}
	Since $V \SubsetO G$, for all $g \in G \colon gV^{-1}$ is open. Therefore, $U \cap gV^{-1} \neq \emptyset$ for all $g \in G$ implying that $g \in U \cdot V$ for all $g \in G$. 
\end{proof}
We know: Closed subsets of affine algebraic groups are again affine algebraic groups. What can we say more generally?
Is there a way to construct closed subgroups?
\begin{definition}[locally closed, constructible] 
	Let $X$ be a topological space. Then $Y \subset X$ is \textbf{locally closed} if $Y= U \cap Z$ for some $U \SubsetO X$ and $Z \Subset X$. 
	
	Finite unions of locally closed subsets in $X$ are called \textbf{constructible}. 
\end{definition}
\begin{remark}
	One can show that the set if all constructible subsets in $X$ contains all open and closed sets and it is closed under taking finite unions and complements. In fact, it is minimal with these conditions. 
\end{remark}
\begin{proposition}~ \vspace{-\topsep} \label{propX.7}
	\begin{enumerate}
		\item 	A constructible set $Y \subset X$ contains a subset wich is dense and open in $\overline Y$.
		\item (Chevalley) Images of constructible sets under 	morphisms of affine algebraic varieties are constructible. 
	\end{enumerate}
\end{proposition}
\begin{proof}~ \vspace{-\topsep}
	\begin{enumerate}
		\item end of section
		\item Exercise (uses dimension of affine algebraic variety). \qedhere
	\end{enumerate}
\end{proof}
\begin{proposition} \label{propX.8}
	Let $(G,k[G])$ be an affine algebraic group and $H<G$ a subgroup.
	\begin{enumerate}
		\item $\overline H < G$ is a subgroup
		\item If $H$ is constructible, then $H = \overline H$. 
	\end{enumerate}
\end{proposition}
\begin{proof}~ \vspace{-\topsep}
	\begin{enumerate}
		\item clear: $\overline{H}^{-1} = \overline{H^{-1}}$. For all $g \in H$ we have $g \overline H = \overline{gH} = \overline{H}$. Therefore, $H\overline H \subset \overline H$. Rest exercise.
		\item Since $H$ is constructible, by \Cref{propX.7} there exists $U  \subset H$ such that $U \SubsetO \overline H$ with $\overline U = \overline H$. 
		By \Cref{lemX.6}, $\overline H = U \cdot U \subset H \cdot H \subset J$. Therefore, $\overline H = H$.\qedhere
	\end{enumerate}
\end{proof}
\begin{proposition} \label{propX.9}
	Let $\varphi \colon G \to G^\prime$ be a morphism between affine algebraic groups $(G,k[G])$, $(G^\prime,k[G^\prime])$. Then 
	\begin{enumerate}
		\item $\ker \varphi <G$ is a closed subgroup
		\item $\im \varphi < G^\prime$ is a closed subgroup
		\item $\varphi(G_0) = \varphi(G)_0$
	\end{enumerate}
\end{proposition}
\begin{proof}~ \vspace{-\topsep}
	\item $\ker \varphi < G$ is clear. Furthermore, $\{e\} \Subset G^\prime$ and so $\varphi^{-1}(\{e\}) = \ker \varphi \Subset G$ because $\varphi$ is continuous.
	\item $\varphi(G)<G^\prime$ is clear. Furthermore, $\varphi(G)$ is constructible by \Cref{propX.7} since $G = G \cap G$ is constructible. By \Cref{propX.9} we have that $\varphi(G)=\overline{\varphi(G)}$.
	\item We have 
	\begin{itemize}
		\item $\varphi(G_0) \subset \varphi(G) \subset G^\prime$ is closed by the previous statement and \Cref{propX.5}. 
		\item $\varphi(G_0) \subset G^\prime$ is irreducible (\Cref{lemX.2}) and definition of $G_0$.
		\item $\varphi(G_0)$ contains $e$ by definition of $G_0$. 
	\end{itemize}
	All in all, $\varphi(G_0) \subset \varphi(G)_0$ by definition of identity component in $\varphi(G)$. 
	Consider 
	\begin{align*}
		G/G_0 & \longrightarrow \varphi(G)/\varphi(G_0) \\
		gG_0 & \longmapsto \varphi(g)\varphi(G_0)
	\end{align*}
	which is well-defined and surjective. Therefore, $(\varphi(G):\varphi(G_0)) \le (G:G_0)<\infty$. Therefore, $\varphi(G_0) \supset \varphi(G)_0$. Hence $\varphi(G_0)=\varphi(G_0)$.
\end{proof}
\begin{proof}[Proof of \Cref{propX.7} $(i)$]
	Let $X$ be a topological space, $Y \subset X$ be constructible. To show: There exists $U \subset Y$ such that $U \Subset Y$ and $\overline U = \overline Y$. Let $Y = \bigcup_{i =1}^n Y_i$ where $Y_i$ are locally closed. Set $Z_i \coloneqq \overline Y_i \setminus Y_i$ and define $Z = \bigcup_{i=1}^n Z_i$. We claim that $U \coloneqq \overline Y \setminus Z \Subset Y$ does the job.
	
	Consider \[Y \cup Z = \bigcup_{i=1}^nY_i \cup \bigcup_{i=2}^n Z_i=\bigcup_{i=1}^n (Y_i \cup Z_i)=\bigcup_{i=1}^n \overline{Y_i} = \overline{\bigcup_{i=1}^n Y_i}=\overline Y\] Then $U = \overline Y \setminus Z \subset Y$.
	
	Since $Y_i$ is locally closed we have $Y_i \SubsetO \overline{Y_i}$ hence $Z_i \Subset \overline{Y_i}$. Therefore, $Z_i \Subset X$ for all $i$. All in all, $Z \Subset X$ and hence $U \SubsetO X$.
	
	Assume that $U \subset \overline Y$ is not dense. Then there exists $V \SubsetO \overline Y$ such that $U \cap V = \emptyset$. Hence, $V \subset Z = \bigcup_{i=1}^n Z_i$. Let $i_0$ be minimal such that $\bigcup_{i=1}^{i_0}$ contains a nonempty open subset $W$ of $\overline V$. We claim that $W \not \subset Z_i$ for all $i$. If $W \subset Z_i$, then $W \subset \overline{Y_i}$ with $W \cap Y_i = \emptyset$ which is a contradiction since $Y_i \subset \overline{Y_i}$ is dense.
	
	Hence $i_0>1$ and $W \not \subset Z_{i_0}$. Therefore, $\emptyset \neq W \setminus Z_{i_0} \subset \bigcup_{i=1}^{i_0-1} Z_i$ which is a contradiction to the minimality of $i_0$ 
\end{proof}
\newpage
\section{More on Products}
If $(X,k[X]),(Y,k[Y])$ are affine algebraic varieties, then so is $(X \times Y,k[X,Y])$ is an affine algebraic variety.

\textbf{Warning:} The Zariski topology on $X \times Y$ is not (in general) the product of the Zariski topologies. 
\begin{example}
	Consider the product $\mathbb A^1 \times \mathbb A^1$ where $\mathbb A=(k,k[k]=k[t])$
	
	In the product topologies are unions of $U_1 \times U_2$ where $U_1,U_2$ are open in $k$ (with Zariski).
	
	Closed sets are $\emptyset, Z_1 \times k, k \times Z_2$, finte sets (for Zariski) where $Z_i \subset k$ closed in Zariski
\end{example}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

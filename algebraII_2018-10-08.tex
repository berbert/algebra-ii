\section{Preliminaries}
The recommended books for this lecture are: 
\begin{itemize}
\item Hungerford -- Algebra
\item Knapp -- Basic Algebra
\item Knapp -- Advanced Algebra
\item Procesi -- Lie Groups: An Approach through Invariant Theory
\item Borel -- Linear Algebraic Groups
\item Humphreys -- Linear Algebraic Groups
\item Springer -- Linear Algebraic Groups
\end{itemize}
\section{Group Actions}
If $G$ is a group, denote by $e \in G$ the neutral element, by $g^{-1}$ the inverse of $g \in G$ and by $gh$ the composition $g \circ h$.
\begin{definition}\label{group action}
  Given a group $G$ and a set $X$, an \textbf{action of $G$ on $X$} is a map 
  \begin{align*}
    \pi \colon G \times X & \longrightarrow X  \\
    (g,x) & \longrightarrow g.x 
  \end{align*}
  such that
  \begin{enumerate}[label=(A\arabic*)]
  \item $e.x=x$
  \item $(gh).x=g.(h.x)$
  \end{enumerate}
  for all $x \in X$ and $g,h \in G$.
\end{definition}
\begin{definition}[Symmetric group]
  Given a set $X$, define
  \[
    S(X) \coloneqq \{f \colon X \to X \mid \text{$f$ is bijective}\},
  \]
  the \textbf{symmetric group of $X$} (with composition as group multiplication).
  
  Given a $G$-set $X$ and $g \in G$. Let $\pi_G \in S(X)$ be defined as $\pi_g(x)=g.x$.
  
  We have $\pi_g \in S(X)$ since $\pi_{g^{-1}}$ is the inverse map because
  \[
      (\pi_{g^{-1}} \circ \pi_g )(x)
    = (g^{-1} g).x
    =
    x
    = (\pi_g \circ \pi_{g^{-1}})(x)
  \]
  for all $x$.
\end{definition}
\begin{lemma} \label{lemI.1}
  For any group $G$ and set $X$, we have a one-to-one-correspondence 
  \begin{align*}
    \{\text{$G$-action on $X$} \} & \longleftrightarrow  \{\text{group homos $G \to S(X)$}\} \\
    (\pi \colon G \times X \to X) & \longmapsto \hat{\pi} \\
    \mathring{\varphi} & \longmapsfrom \varphi
  \end{align*}
  where $\hat{\pi} (g)(x) = \pi((g.x))=g.x$ and $\mathring{\varphi}((g,x)) \coloneqq \varphi(g)(x)$ for all $g \in  G$, $x \in X$.
\end{lemma}
\begin{proof}
  Do it yourself!
\end{proof}
\begin{example}
  \leavevmode
  \begin{enumerate}[label=(\roman*)]
  \item The group $G$ acts on itself by
    \begin{itemize}
    \item left multiplication: $g.x=gx$;
    \item right multiplication: $g.x=xg^{-1}$;
    \item conjugation: $g.x=gxg^{-1}$.
    \end{itemize}
  \item Any set is a $G$-set via the trivial action $g.x=x$ for all $g \in G$, $x \in X$.
  \item Let $X$, $Y$ be two $G$-sets. Then $G$ acts on
  \[
    \Maps(X,Y) \coloneqq \{\text{$f \colon X \to Y$ is a map}\}
  \]
  via $(g.f)(x)=g.(f(g^{-1}.x))$ for $f \in \Maps(X,Y)$, $g \in G$, $x \in X$.
    
    Special case: If the action on $Y$ is trivial, then $(g.f)(x)=f(g^{-1}.x)$.
  \end{enumerate}
\end{example}
\begin{definition}[$G$-equivariant]
  Let $X,Y$ be two $G$-sets. A map $f \colon X \to Y$ is called \textbf{$G$\nobreakdash-equivariant} if $f(g.x)=g.(f(x))$ for all $g \in G$, $x \in X$.
  
  We write
  \[
    \Hom_G(X,Y) \coloneqq \{f \colon X \to Y \mid \text{$f$ is $G$-equivariant}\} \,.
  \]
\end{definition}
\begin{lemma}\label{lemI.2}
  Let $G$ be a group. 
  \begin{enumerate}
  \item If $X$ is a $G$-set, then $\id_X \in \Hom_G(X,X)$.
  \item If $X,Y,Z$ are $G$-sets, then it holds for all $f_1 \in \Hom_G(X,Y)$, $f_2 \in \Hom_G(Y,Z)$ that $f_2 \circ f_1 \in \Hom_G(X,Z)$.
  \end{enumerate}
\end{lemma}
\begin{example}
  Let $G$ be a group.
  \begin{enumerate}[label=(\roman*)]
    \item If $G$ acts on itself by left multiplication then
    \begin{align*}
      \Hom_G(G,G) & \cong G \quad \text{(as sets)} \\
      f & \mapsto f(e) \\
      m_a & \mapsfrom a 
    \end{align*}
    where $m_a$ denotes right multiplication with $a$.
    \begin{proof}
      Well-definedness: $m_a(g.x)=m_a(gx) = (gx)(a)=g(xa)=g.m_a(x)$.
      The maps are inverse to each other: Check this on your own.
    \end{proof}
    \item If $X,Y$ are trivial $G$-sets then $\Hom_G(X,Y) = \Maps(X,Y)$.
  \end{enumerate}
\end{example}
\begin{definition}[Orbit]
  Let $X$ be a $G$-set. We write
  \[
    \orbit{G}{X} \coloneqq \{\text{orbits of the $G$-action on $X$}\}
  \]
  where the orbits are given by $\mathcal{O}_x \coloneqq \{\, g.x \mid g \in G \,\}$ for any $x \in X$.
\end{definition}
\begin{note}
  We have $\mathcal{O}_x = \mathcal{O}_y$ if and only $y \in \mathcal{O}_x$ since $g^{-1}.(g.x)=x$.
\end{note}
\begin{remark}
  We can view $\orbit{G}{X}$ as a $G$-set via the trivial action, then  the map
  \begin{align*}
    \can \colon X & \mapsto \orbit{G}{X} \,, \\ 
    g & \mapsto \mathcal{O}_g
  \end{align*}
  is $G$-equivariant since $\can(g.y)=\mathcal{O}_{g.y}=\mathcal{O}_y=\can(y)=g.\can(y)$.
\end{remark}
\begin{definition}[$G$-invariant]
  Let $X$ be a $G$-set. Then
  \[
    X^G \coloneqq \{\,x \in X \mid \text{$g \in G: g.x=x$ for all~$g \in G$} \,\}
  \]
  is the set of \textbf{$G$-fixed} points or \textbf{$G$-invariants} in $X$. 
\end{definition}
\begin{lemma} \label{lemI.3}
  Let $X,Y$ be $G$-sets and let $f \in \Hom_G(X,Y)$. Then $f(X^G) \subseteq Y^G$.
  \begin{proof}
    If $x \in X^G$ then $g.(f(x))=f(g.x)=f(x)$ and therefore $f(x) \in Y^G$.
  \end{proof}
\end{lemma}
Thus, $f$ induces a map $f^G \colon X^G \to Y^G$ by restriction.
\begin{lemma} \label{lemI.4}
  Let $G$ be a group.
  \begin{enumerate}[label=(\roman*)]
    \item If $X$ is a $G$-set then $\id_X^G=\id_{X^G}$.
    \item If $X,Y,Z$ are $G$-sets and $f_1 \in \Hom_G(X,Y)$, $f_2 \in \Hom_G(Y,Z)$ then
    \[
      (f_2 \circ f_1)^G = f_2^G \circ f_1^G .
    \]
  \end{enumerate}
  \begin{proof}
    \begin{enumerate}[label=(\roman*)]
      \item This is obvious.
      \item $f_2^G \circ f_1^G = \res{f_2}{X^G} \circ \res{f_1}{X^G}=\res{(f_2 \circ f_1)}{X^G} = (f_2 \circ f_1)^G$.
      \qedhere
    \end{enumerate}
  \end{proof}
\end{lemma}
\begin{lemma} \label{lemI.5}
  Let $X,Y$ be $G$-sets. Then
  \[
    \Hom_G(X,Y)=\Maps(X,Y)^G .
  \]
  \begin{proof} We have
    \begin{align*}
          {}& f \in \Hom_G(X,Y) \\
      \iff{}& \forall g \in G, x \in X: f(g.x)=g.f(x) \\
      \iff{}& \forall g \in G, x \in X: g^{-1}f(g.x)=g^{-1}.(g.f(x))=f(x) \\
      \iff{}& \forall g \in G, x \in X: gf(g^{-1}x)=f(x) \\
      \iff{}& f \in \operatorname{Maps}(X,Y)^G \,.
      \qedhere
    \end{align*}
  \end{proof}
\end{lemma}
\begin{definition}[$G$-invariant]
  Let $X$ be a $G$-set and $k$ a field (or a commutative ring with $1$). A map $f \colon X \to k$ is \textbf{$G$-invariant} if $f(x)=f(g.x)$ for all $g \in G$, $x \in X$.
\end{definition}
\begin{example}
  Let $G=\Z/2\Z=\{e,s\}$. Let $G$ act on $X=\R$ by $s.\lambda = -\lambda$ (where $\lambda \in \R$). Let $k= \R$. Any polynomial $\sum_{i=0}^\infty a_it^i = p(t) \in \R[t]$ can be viewed as an element in $\Maps(\R,\R) = \Maps(X,\R)$. 

  Then $p(t)$ is $G$-invariant if and only if $p(t)$ is even (i.e.\ $a_i=0$ for all odd $i$)
  
  \begin{proof}
    \begin{align*}
          {}& \text{$p(t)$ is $G$-invariant} \\
      \iff{}& \forall \lambda \in \R: p(s.\lambda)=p(\lambda) \\
      \iff{}& \forall \lambda \in \R: p(-\lambda)=p(\lambda) \\
      \iff{}& \forall \lambda \in \R: \sum_{\text{$i$ odd}} (-1)^i a_i \lambda^i = \sum_{\text{$i$ odd}} a_i\lambda^i \\ 
      \iff{}& \forall \lambda \in \R: 2 \sum_{\text{$i$ odd}} a_i \lambda^i = 0 \\
      \iff{}& \forall \text{$i$ odd}: a_i=0 \\
      \iff{}& \text{$p(t)$ is even}.
      \qedhere
    \end{align*}
  \end{proof}
\end{example}
\begin{remark}
  A function $f \colon X \to k$ is $G$-invariant if and only if $f \in \Maps(X,k)^G$ where we have the trivial $G$-action on $k$.
\end{remark}
\begin{lemma}[Universal property of orbit sets] \label{lemI.6}
  Let $X$ be a $G$-set, $k$ a field \textup(or commutative ring with $1$\textup). Then $f \colon X \to k$ is $G$-invariant if and only if $f$ factors through $\can$ \textup(i.e.\ there exist a unique map~$\overline{f} \colon \orbit{G}{X} \to k$ such that $f = \overline f \circ \can$\textup).
  \begin{equation*}
    \begin{tikzcd}
      & X \arrow{r}{f} \arrow{d}[left]{\can} & k \\
      & \orbit{G}{X}\arrow[swap]{ur}{\exists! \overline{f}}
    \end{tikzcd}
  \end{equation*}
\end{lemma}
\begin{proof}
  \begin{align*}
    & \text{$f$ is $G$-invariant} \\
    \iff & \forall g \in G, x \in X: f(g.x)=f(x) \\
    \iff & \text{$f$ is constant on orbits} \\
    \iff & \text{$\overline{f}$ exists, namely $\overline{f}(\mathcal{O}_x)=f(x)$} .
    \qedhere
  \end{align*}
\end{proof}
\begin{lemma} \label{lemI.7}
  Let $X$ be a finite $G$-set and $k$ a field \textup(or commutative ring with $1$\textup).
  \begin{enumerate}[label=(\roman*)]
  \item $\Maps(X,k)$ is a $k$-vector space \textup(or $k$-module\textup) with pointwise addition and scalar multiplication.
  \item A $k$-basis of $\Maps(X,k)$ is given by $\chi_y$, where $y \in X$ and
  \[
    \chi_z(y) =
    \begin{cases}
      1 &\text{if $y= z$} \,, \\
      0 &\text{otherwise} \,.
    \end{cases}
  \]
  \item $\Maps(X,Y)^G$ forms a subspace (or a submodule) with basis $\chi_\mathcal{O}$, where $\mathcal{O} \in \orbit{G}{X}$ and
  \[
    \chi_\mathcal{O}(y) =
    \begin{cases}
      1 &\text{if $y \in \mathcal{O}$} \,, \\
      0 & \text{otherwise} \,.
    \end{cases}
  \]
  \end{enumerate}
\end{lemma}
\begin{proof}
  \begin{enumerate}[label=(\roman*)]
  \item This is clear.
  \item Generating set: Let $f \in \Maps(X,k)$. Then $f= \sum_{y \in X} f(y) \chi_y$ because
  \[
      \sum_{y \in X} f(y) \chi_y(x)
    = \sum_{y \in X} f(y) \delta_{xy}
    = f(x)
  \]
  for all $x \in X$. 
    
    Linear independence: $\sum_{x \in X} a_x \chi_x = 0$ gives
    \[
      a_y = \sum_{x \in X} a_x \chi_x(y) = 0
    \]
    for all $y \in X$ and therefore $a_y=0$ for all $y \in X$.
  \item To show: If $f_1, f_2, f \in \Maps(X,k)^G$ and $\lambda \in \R$ then $f_1 + f_2, \lambda f \in \Maps(X,k)^G$. Check this!
    
    Generating set: That $f \in \Maps(X,k)^G$ means that $f$ is constant on orbits and is therefore of the form $f = \sum_{\mathcal{O} \in \orbit{G}{X}} a_{\mathcal{O}} \chi_\mathcal{O}$ for every $a_\mathcal{O}=f(x)$ with $x \in \mathcal{O}$.
    
    Linear independence: See \textit{(ii)}.
    \qedhere
  \end{enumerate} 
\end{proof}
If $X$ is an infinite set then we replace $\Maps(X,k)$ by
\[
  kX \coloneqq \{\,f\colon X \to k \mid \text{$\supp(f)$ is finite}\,\}
\]
where
\[
  \supp(f) \coloneqq \{\,x \in X \mid f(x) \neq 0\,\}
\]
is the \textbf{support} of $f$.
\begin{note} We have
  $\supp(f_1+f_2) \subseteq \supp(f_1) \cup \supp(f_2)$ and $\supp (\lambda f) \subseteq \supp (f)$ for all $f_1,f_2,f \in \Maps(X,k)$ and $\lambda \in k$
\end{note}
This gives that $kX \subseteq \Maps(X,k)$ is a vector subspace (usually just call it $kX$ as well). 
The subspace $kX$ is preserved under the $G$-action: Let $f \in kX$ and $g \in G$. Then
\begin{equation*}
       (g.f)(x) \neq 0
  \iff f(g^{-1}.x) \neq 0
  \iff g^{-1}.x \in \supp(f) \, .
\end{equation*}

\Cref{lemI.7} generalizes to $kX$.
\begin{lemma} \label{lemI.8}
  Let $G$ be a group and $R$ a ring. Let $G$ act on $R$ by ring homomorphisms \textup(i.e.\ if $\pi \colon G \times R \to R$ is the action, then $\pi_g \colon R \to R$ is a ring homomorphism for all $g \in G$\textup), then $R^G$ is a subring of $R$.
\end{lemma}
\begin{proof}
  Let $r_1,r_2 \in R^G$. It needs to be shown that $r_1+r_2,r_1r_2 \in R^G$. For $g \in G$, we have
  \begin{align*}
    g.(r_1+r_2) = \pi_g(r_1+r_2)=\pi_g(r_1)+\pi_g(r_2)=gr_1+gr_2 \; .
  \end{align*}
  Similarly, $g.(r_1r_2)=r_1r_2$.
\end{proof}
\begin{example}
  The even polynomials form a subring of $\R[t]$.
\end{example}
\begin{definition}[Commuting group actions]
  Let $G,H$ be groups and let $X$ be both a $G$-set and a $H$-set. Then the two actions \textbf{commute} if
  \[
    g.(h.x)=h.(g.x)
  \]
  for all $g \in G$, $h \in H$, $x \in X$. The $G \times H$ acts naturally on $X$ via $(g,h).x=g.(h.x)$.
\end{definition}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:

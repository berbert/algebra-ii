\begin{lemma}\label{lemviii.8}
  Let $(A,m,\eta)$ be an algebra and $(C,\Delta,\epsilon)$ be a coalgebra.
  Then the following are equivalent:
  \begin{enumerate}[label=(\Roman*)]
  \item
    $(A,m,\eta,\Delta,\epsilon)$ is a bialgebra (id est (H1)-(H6))
  \item
    $\Delta,\epsilon$ are algebra homomorphisms sending $1$ to $1$
  \item
    $m,\eta$ are coalgebra homomorphisms sending the counit to the counit.
  \end{enumerate}
\end{lemma}
\begin{proof}
  Follows directly from the definitions and the axioms.
\end{proof}

Consider the (H7), the antipode condition, which we can formulate as follows:
\begin{equation*}
  \begin{tikzcd}
    & 
  \end{tikzcd}
\end{equation*}
What are properties of the antipode $i$?
Easy to check:  $(C,\Delta,\epsilon)$ is a coalgebra.
Therefore $(C^\ast,\res{\Delta^\ast}{}, \eta = \epsilon^\ast)$ is an algebra.
Where $  \Delta \colon C \to C \otimes C$ with
\begin{equation*}
  \begin{tikzcd}
     (C\otimes C)^\ast \arrow[r,"\Delta^\ast"] & C^\ast \\
     C^\ast \otimes C^\ast \arrow[u,hookrightarrow,"C^\ast \otimes C^\ast"] \arrow[ur,dashed,"\res{\Delta^\ast}{}"']& .
  \end{tikzcd}
\end{equation*}
This is a special case of
\begin{lemma}\label{lemviii.9}
  Let $(C,\Delta,\epsilon)$ be a coalgebra and $(A,m,\eta)$ an algebra.
  Then $\Hom_k(C,A)$ is an algebra with multiplication $f_1 \ast f_2 = m \circ (f_1 \otimes f_2) \circ \Delta$ for $f_1,f_2 \in \Hom_k(C,A)$, that is $f_1 \ast f_2 (x) = \sum_i f_i(x_i^\prime) \cdot f_i(x_i^{\prime\prime})$ for $x \in C$, where $\Delta(x) = \sum_i x_i^\prime \otimes x_i^{\prime\prime} \in C \otimes C$.
  We have the unit $\eta_A \circ \epsilon_C \colon C \xrightarrow{\epsilon_C} k \xrightarrow{\eta_A} A$.
\end{lemma}
\begin{proof}~ 
  \begin{itemize}
  \item $\ast$ is a bilinear form, which is clear from the definition.
  \item $\ast$ is associative:
    \begin{equation*}
      \begin{tikzcd}
        &C\otimes C \arrow[r,"(f_1 \ast f_2) \otimes f_3"] \arrow[d,"\Delta \otimes \id"] & A \otimes A \arrow[dr,"m"] & \\
        C \arrow[ur,"\Delta"]\arrow[dr,"\Delta"] & C \otimes C \otimes C \arrow[r,"f_1 \otimes f_2 \otimes f_3"] & A \otimes A \otimes A \arrow[u,"m \otimes \id"] \arrow[d,"\id \otimes m"] & A \\
        & C \otimes C \arrow[u,"\id \otimes \Delta"] \arrow[r,"f_1 \ast (f_2 \ast f_3)"] & A \otimes A \arrow[ur,"m"] & .
      \end{tikzcd}
    \end{equation*}
    We here se that all ``small'' four-gons commute.
    We therefore see that $(f_1 \ast f_2) \ast f_3 = f_1 \ast ( f_2 \ast f_3)$, and we have associativity.
  \item
    We have $ ((\eta \circ \epsilon) \ast f)(x) = \sum_i (\eta \circ \epsilon)(x_i^\ast)f(x_i^{\prime\prime}) = f\left( \sum_i (\eta \circ \epsilon)(x_i^\prime)x_i^{\prime\prime}\right) = f(x)$.
    Similarly, we have $f \ast (\eta \circ \epsilon)(x) = f(x)$.
    We have sown that $\eta \circ \epsilon$ is $1$ for the multiplication.
  \end{itemize}
  This concludes the proof.
\end{proof}
\begin{corollary}\label{corviii.10}
  If a bialgebra has an antipode, then it is unique.
\end{corollary}
\begin{proof}
  Let $I,I^\prime$ be antipodes for the bialgebra $(A,m,\eta,\Delta,\epsilon)$.
  Then, using (H7), we have
  \begin{align*}
    I = I \ast (\eta \circ \epsilon) &= I \ast (\id \ast I^\prime) \\
                                     &= (I\ast \id)\ast I^\prime \\
                                     &= (\eta \circ \epsilon) \ast I^\prime = I^\prime \; .
  \end{align*}
\end{proof}
\begin{lemma}\label{lemviii.11}
  The antipode $I$ in a Hopf algebra $(A,m,\eta,\Delta,\epsilon, I)$ satisfies
  \begin{equation*}
    I(ab) = I(b)I(a)
  \end{equation*}
  for all $a,b \in A$, that is, $I$ is an algebra anti-homomorphism
\end{lemma}
\begin{proof}
  Define $\alpha,\beta \in \Hom_k(A\otimes A,A)$ as $ \alpha(a\otimes b) = I(b)I(a)$, $\beta(a\otimes b) = I(ab)$.
  It is enough to show that $\beta \ast m = m \ast \alpha = \eta \circ \epsilon$.

  Note: Let $\Delta_{A\otimes A}(a\otimes b) = \sum_i (a\otimes b)_i^\prime \otimes (a\otimes b)_i^{\prime\prime} \in (A\otimes B) \otimes (A\otimes B)$.
  We also have $\Delta_{A\otimes A} = (\id \otimes \tau \otimes \id) \circ \Delta_A \otimes\Delta_A (a\otimes b) = \sum_{i,j} a_i^\prime \otimes b_j^\prime \otimes a_i^{\prime\prime} \otimes b_j^{\prime\prime}$, if $\Delta(a) = \sum_i a_i^\prime \otimes a_i^{\prime\prime}$ and $\Delta(b) = \sum_j b_j^\prime \otimes b_j^{\prime\prime}$.
  Also $\Delta(ab) = \Delta(a)\Delta(b) = \sum_{i,j} a_i^\ast b_j^\prime \otimes a_i^{\prime\prime}b_j^{\prime\prime} = \sum_s (ab)_s^\prime \otimes (ab)_s^{\prime\prime}$ (\cref{lemviii.8}).
  Hence
  \begin{align*}
    \beta \ast m (a\otimes b) &= \sum_{i,j} I(a_i^\prime b_j^\prime)a_i^{\prime\prime}b_j^{\prime\prime} \\
                              &= \sum_s I((ab)_s^\prime)(ab)_s^{\prime\prime}  \\
                              &\overset{=}{\text{(H7)}} \eta \circ \epsilon (a \otimes b) \; .
  \end{align*}
  Similarly, we have $m \ast \alpha (a \otimes b) = \eta \circ \epsilon (a \otimes b)$, which concludes our claim
\end{proof}
\begin{theorem}\label{thmviii.12}~ 
  \begin{enumerate}
  \item
    If $(G,k[G])$ is an affine algebraic group, then $(k[G],m,\eta,\Delta,\epsilon, \inv^\ast = i)$ (as described in an earlier example) is a commutative finitely generated Hopf algebra without nilpotent elements.
  \item
    If $(A,m,\eta,\Delta,\epsilon,I)$ is a finitely generated Hopf algebra without nilpotent elements, then it is isomorphic to a Hopf algebra as described above for some $(G,k[G])$.
  \end{enumerate}
  (An isomorphism of Hopf algebras is an isomorphism of the underlying algebras sending $1$ to $1$, which is a morphism of coalgebras and sends antipode to antipode.)
\end{theorem}
\begin{proof}~ 
  \begin{enumerate}
  \item clear by construction
  \item
    $(A,m,\eta)$ is by assumption a finitely generated commutative algebra without nilpotent elements.
    Therefore $(A,m,\eta) \cong k[X]$ as algebras for some affine algebraic variety $(X,k[X])$ (\Cref{lemvii.2}).
    Now (H2) implies (G1) and (G2) and (H7) implies (G3), therefore $X=G$ is a group.

    By assumption and \Cref{lemviii.8}, $\Delta$ is an algebra homomorphism $$k[G] \to k[G]\otimes k[G] = k[G \times G] \;;$$ therefore $\mu \colon G \times G \to G$ is a morphism of affine algebraic varieties.
    By assumption $A$ is commutative, hence $I(ab) = I(a)I(b)$ (\Cref{lemviii.11}).
    Thus the antipode is an algebra homomorphism.
    Now $A \cong k[G]$ as bialgebras, hence $\inv^\ast$ is the antipode, and therefore an algebra homomorphism as well.
  \end{enumerate}
\end{proof}
\begin{remark}
  Behind this is an equivalence of categories
  \begin{align*}
    \set{\begin{matrix}\text{affine algebraic groups (over $k$)} \\  \text{and morphisms of} \\ \text{affine algebraic groups}\end{matrix}}&
                                                                                               \xrightarrow{\sim} \set{\begin{matrix}\text{finitely generated}  \\ \text{commutative Hopf algebras} \\ \text{without nilpotent elements and} \\ \text{Hopf algebra homomorphisms}\end{matrix}}  \\
    (G,k[G]) &\mapsto (k[G],m,\eta,\Delta,\epsilon=\ev_e,\iota = \inv^\ast)
  \end{align*}
\end{remark}
\newpage
\section{Linearization of Affine Algebraic Groups}
\begin{definition}
  Let $(G,k[G])$ be an affine algebraic group and $(X,k[X])$ be an affine algebraic variety.
  An \textbf{action of $G$ on $X$} (as affine varieties) is a morphism
  \begin{align*}
    \alpha \colon G \times X & \longrightarrow X \\
    (g,x) &\longmapsto g.x = \alpha((g,x))
  \end{align*}
  of affine algebraic varieties such that $e.x = x$ for all $x \in X$ and $g.(h.x) = (gh).x$ for all $x \in X$ and for all $g,h \in G$, that is $G$ acts on $X$, $\alpha$ is an action.

  This means that any $g \in G$ gives a morphism $X \to X$, $x \mapsto g.x$, meaning that we have a morphism $G \to \GL(k[X]) := \setdef{\varphi \in \End_k (k[X])}{\varphi \text{ invertible}}$
\end{definition}
\begin{remark}
  $G$ acts on itself by left multiplication and by right multiplication.

  \underline{Idea:}  Let $G$ act on itself by right multiplication there exists a finite dimensional subspace $E$ of $k[G]$ which is stable under the induced action.
  We get $G \to \GL(E) = \GL_n(k)$ for some $n \in \N$.

  \underline{Plan:}  We show that this inclusion is closed.
\end{remark}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "algebraII_main"
%%% End:
